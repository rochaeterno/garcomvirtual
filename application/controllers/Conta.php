<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conta extends CI_Controller{
	public function index(){
		//apenas para bloquear o index por enquanto
	}
	
	public function Trocar_Mesa(){
		/*Esta função deve capturar inicialmente o id da mesa atual a partir do id da conta para então trocar seu status para fora de uso, em seguida o status da nova mesa deve ser trocado para EM USO, para só então o id da mesa ser trocado na row da conta.*/
		//a função recebe os dados via post do balcao_view
		$id_conta = $this->input->post('id_conta');
		$id_mesa_nova = $this->input->post('slct_new_mesa');
		$this->load->model('conta_model');
		$id_mesa = $this->conta_model->Identificar_Mesa($id_conta);
		//die($id_mesa."/teste/".$id_mesa_nova);
		$this->load->model('mesa_model');
		$this->mesa_model->Update($id_mesa,0);
		
		$this->load->model('mesa_model');
		$this->mesa_model->Update($id_mesa_nova,1);
		
		$this->load->model('conta_model');
		$teste=$this->conta_model->Trocar_Mesa($id_conta,$id_mesa_nova);
		
		if($id_mesa_nova==$teste){
			$redirect = site_url("balcao");
			header("location:$redirect");
		}
				
	}


	
	public function Solicitar_Abertura()
	{
		$form['idMesa'] = $this->input->post('idMesa');
		$form['status_conta'] = "0";
		//$localGTM = '';
		$form['horaAbertura'] = date('Y-m-d H:i:s');
		$form['status_mesa'] = "1";
		
		$table = 'tbconta';
		$mesa = 'tbmesa';
		$dados = array('idMesa' => $form['idMesa'],'status_conta'=> $form['status_conta'], 
			'horaAbertura' => $form['horaAbertura']);
		$dados_mesa = array('idMesa' => $form['idMesa'], 'statusMesa' => '1');
		$this->load->model('conta_model');
		$mesa_status = $this->conta_model->Verificar_Status($mesa,$dados_mesa);
		//o status da mesa retornando falso significa que a mesa buscada está atualmente vazia
		if($mesa_status==FALSE){
			$this->load->model('mesa_model');
			$mesa_confirm = $this->mesa_model->Update($form['idMesa'],$form['status_mesa']);

			$this->load->model('conta_model');
			//no insert a seguir tambem e feita a criação do alerta responsovel por essa abertura de conta
			$confirmation = $this->conta_model->Insert($table, $dados); 
			if($confirmation){
				unset($_SESSION['lastIdConta']);			
				$newdata = array('idConta' => $confirmation,'status_conta' => 0);
				$this->session->set_userdata($newdata);
				
				return true;
			}
		}elseif($mesa_status==TRUE){
			if(isset($_SESSION['id_mesa'],$_SESSION['idConta'])){
				$redirect = site_url("cliente_apt/todos");
				header("location:$redirect");
			}
			elseif(isset($_SESSION['id_mesa'],$_SESSION['lastIdConta'])){
				$newdata = array('idConta' => $_SESSION['lastIdConta'],'status_conta'=> '0');
				$this->session->set_userdata($newdata);
				unset($_SESSION['lastIdConta']);
				$redirect = site_url("cliente_apt/todos");
				header("location:$redirect");
			}
		}
	}
	
	public function Solicitar_Fechamento()
	{
		$form['idConta'] =$this->input->post('idConta_fechada');
		$table = 'tbconta';
		$dados = array('idConta' => $form['idConta'], 'status_conta' => '3');
		
		$this->load->model('crud');
		$confirmation = null;
		$confirmation = $this->crud->Select_obj($table, $dados);
		
		if($confirmation){
			//echo "confirmation";
			$status = intval($confirmation->status_conta);
		}else{
			//echo "continua null";
			$status = null;
		}
		if($status >= 2){
			//echo "Conta Fechada ";
			unset($_SESSION['idConta'],$_SESSION['lastIdConta']);
			$redirect = site_url("cliente_apt/seted_mesa");
			header("location:$redirect");	
		}else{
			$form['status_conta'] = "2";
			//$localGTM = '';
			$form['horaFechamento'] = date('Y-m-d H:i:s');
			$table = 'tbconta';
			$dados = array('status_conta'=> $form['status_conta'], 
				'horaFechamento' => $form['horaFechamento']);

			//AQUI FICARA A CHAMADA DO MODEL DE AVALIAÇÃO!!!
			if(isset($_POST['nota_rest'])){//ultilizando isset a avaliação não é obrigatoria
				$form['nota_rest'] = $this->input->post('nota_rest');
				$this->load->model('model_avaliacao');
				$confirm=$this->model_avaliacao->Avaliar_rest($form['nota_rest'],$form['idConta']);

				if(!isset($confirm)){
					echo "erro na inserção de avaliação do restaurante";
				}
			}

			if(isset($_POST['nota_garcom'])){//ultilizando isset a avaliação não é obrigatoria
				$form['nota_garcom'] = $this->input->post('nota_garcom');
				$this->load->model('model_avaliacao');
				$confirm=$this->model_avaliacao->Avaliar_garcom($form['nota_garcom'],$form['idConta']);
				if(!isset($confirm)){
					echo "erro na inserção de avaliação do garcom";
				}
			}
			$gorjeta= $this->input->post('gorjetapost');
			if(isset($gorjeta)){
				$this->load->model('gorjeta');
				$this->gorjeta->Consagrado($form['idConta'],$gorjeta);
			}
			$this->load->model('conta_model');
			$confirm = $this->conta_model->Fechamento($table,$dados,$form['idConta']); 
			if($confirm){
				$newdata = array('status_conta' => 2);
				$this->session->set_userdata($newdata);
				$redirect = site_url('cliente_apt/loading_encerramento');
				header("location:$redirect");
			}
		}
	}
	public function Verificar_Status(){
		$status_desejado =  $this->input->post('status');
		$table = 'tbconta';
		if(isset($_SESSION['idConta'])){
			$dados = array('idConta' => $_SESSION['idConta'], 'status_conta' => $status_desejado);
			//procura uma tupla da tbconta que tenha o status desejado
		}elseif(isset($_SESSION['lastIdConta'])){
			$dados = array('idConta' => $_SESSION['lastIdConta'], 'status_conta' => $status_desejado);
			//procura uma tupla da tbconta que tenha o status desejado
		}
		
		$this->load->model('conta_model');
		$confirm = $this->conta_model->Verificar_Status($table, $dados);
		
		if($confirm){
			echo "1";
		}
		else{
			echo "0";
		}
		
	}
	
	public function Criar_Conta(){
		$loggedUser =  $this->session->user;
		
		$form['idGarcom'] = $this->input->post('idGarcom');
		$form['idUsuario_Abertura'] = $this->session->id;
		$form['status_conta'] = "1";
		$form['idMesa'] = $this->input->post('idMesaModal');
		
		$table = 'tbconta';
		$dados = array('idGarcom' => $form['idGarcom'],	'status_conta'=> $form['status_conta'],
			'idUsuario_Abertura' => $form['idUsuario_Abertura'], 'idMesa' => $form['idMesa']);
		
		$this->load->model('conta_model');
		$confirm = $this->conta_model->Update_Abertura($table, $dados);
		
		if($confirm){
			redirect('balcao/index/');
		}
	}

	public function Contar_contas(){
		//conta o numero de contas presentes no banco de dados
		$this->load->model('conta_model');
		$this->conta_model->Contar_contas();
	}

	public function Identificar_Mesa(){
		$id_conta = $this->input->post('id_conta');
		$this->load->model('conta_model');
		$result = $this->conta_model->Identificar_Mesa($id_conta);
		$dados_mesa['id_mesa'] = $result;

		$dados = array('idMesa' => $dados_mesa['id_mesa'], 'statusMesa' => '1');
		$this->load->model('conta_model');
		$result2 = $this->conta_model->Verificar_Status('tbmesa', $dados);
		if($result2){
			$dados_mesa["status"] = 1;
		}else{
			$dados_mesa["status"] = 0;
		}

		echo json_encode($dados_mesa);
	}

	public function Validar_Entrada(){
		$id_conta = $this->input->post('id_conta');
		$table = "tbconta";
		$dados = array('idConta' => $id_conta);

		$this->load->model('conta_model');
		//no futuro o parametro senha sera adicionado
		$result = $this->conta_model->Verificar_Status($table, $dados);

		if($result){
			redirect("cliente_apt/seted_account/".$id_conta);
		}else{
			return false;
		}
	}

	public function Fechar_Conta(){
		//ERROR!!!, A testar com troca de mesa
		$loggedUser =  $this->session->user;
		$form['idUsuario_Fechamento'] = $this->session->id;
		$form['status_conta'] = '3';
		$form['status_mesa'] = '0';
		$table = 'tbconta';
		$mesa='tbmesa';
		$id['id']=$this->input->post('iddaconta');
		//capturamos o id da mesa de acordo com o id da conta
		$this->load->model('conta_model');
		$id_mesa = $this->conta_model->Identificar_Mesa($id['id']);
		//em seguida usamos ele para trocar o status da mesa para que o mesmo seja 0 apos a conta ser fechada
		$this->load->model('mesa_model');
		$mesa_confirm = $this->mesa_model->Update($id_mesa,$form['status_mesa']);
		if(isset($_POST['flag'])){
			$form['flag']=$_POST['flag'];
			$dados = array('status_conta'=> $form['status_conta'],'idUsuario_Fechamento' => $form['idUsuario_Fechamento'],'flag'=>$form['flag']);
		}
		else{
			$dados = array('status_conta'=> $form['status_conta'],'idUsuario_Fechamento' => $form['idUsuario_Fechamento']);
		}
		$this->load->model('conta_model');
		$confirm = $this->conta_model->Fecha_Balcao($table, $dados,$id);
		
		$this->load->model('mesa_model');
		$mesa_confirm = $this->mesa_model->Update($confirm,$form['status_mesa']);
		
		if($mesa_confirm){
			redirect('balcao/index/');
		}
	}
	
}		
?>