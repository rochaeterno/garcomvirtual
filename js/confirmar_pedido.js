$(document).ready(function(){
	//função responssavel por confirmar os pedidos do carrinho do cliente
	$("#confirm").click(function () {
	var dados = new Array();
	$(".product").each(function() {
		var colunas=$(this).children();	
		var obs = $(this).next().find(".product_obs").val();
		var pedido = {
			'nome_prod' : $.trim($(colunas[1]).text()),//retira os espacos do nome 
			'quant' : $(colunas[0]).text(),
			'obs' : $(colunas[2]).find(".product_obs").val(),
		};
		dados.push(pedido);						
	});	
	var myJSON = JSON.stringify(dados);
	// O ajax a seguir direciona os dados para pedido/confirma_pedido
	$.ajax({
        	url: url_confirma,
            type: "POST",
            data: {'pedidos' : myJSON},
                
                success: function(dados) {
                	$("#conf_pedido tbody tr").each(function() {
                		$(this).fadeOut(400, function() {
    						$(this).remove();  	    
    					});	
                	});
                }             
            });
            return false;
});
});