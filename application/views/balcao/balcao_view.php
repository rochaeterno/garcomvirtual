<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<div id="page-wrapper">
	<div class="container-fluid">
		<!-- DIVISÃO DA TELA AO MEIO-->
		<!--LINHA DE MESAS: A linha das mesas segue um padrão de cores para demonstrar seu atual 
		status, a partir do momento em que um mesa for solicitada para abertura ela piscará em 
		VERDE, usando a classe "Mesa-verde", ao ser aberta a mesa se manterá em LARANJA, usando a 
		classe "Mesa-?", solicitações de garçom piscarão em AZUL, usando a classe "Mesa-azul", 
		enquanto o solicitar fechamento piscará em VERMELHO, usando a classe "Mesa-vermelha"-->
		<div id='table_line' class="row">
			<div id="table_bar" class="col-lg-6"></div>
				<!--FECHAMENTO DA METADE DA TELA-->
				<div class="col-lg-6">
					<div id="side_bar_content">
						<div class="tab-content" style="height:100vh;">
							<fieldset id="cabecalho" class="scheduler-border aparecer2">
								<legend class="scheduler-border cabecalho"></legend>
							
								<!--TABELA-->
							<div class="aparecer testes"">
								<div id="dados_ocultos" ></div>
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="display: none">Mesa</th>
											<th style="display: none">Conta</th>
											<th>Quantidade</th>
											<th>Solicitação</th>
											<th>Horário</th>
											<th>Ações</th>
										</tr>
									</thead>
										
									<tbody class="pedidos">
										<!--alimentada por table_selector-->
									</tbody>
								</table>
							</div>

							<script type="text/javascript">
								function abrirConta(idSelect) {
									//recebe o id da mesa clicada e envia para o modal de selecao de garcom
									document.getElementById('idMesaModal').value = idSelect; 
								}
							</script>
						</div>
					</div>
				</div>

				<?php // Adicionar Garçom Modal HTML ?>
				<div id="addgarcom" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Adicionar Garçom</h4>
								<button type="button" class="close" 
									data-dismiss="modal" aria-hidden="true">&times;
								</button>
							</div>

							<?php echo form_open('conta/criar_conta'); ?> 
								<div class="modal-body"> 
									<div class="form-group">
										<label>Garçom</label>
										<select class="form-control" id="idGarcom" name="idGarcom" required>
											<?php foreach($amigao as $camarada):?> 
												<option value="<?=$camarada['idUsuario']?>"><?=$camarada['nome']?></option>
											<?php endforeach; ?>
										</select>
									</div> 
								</div>

							<div class="modal-footer">
								<?php /*o input a seguir recebe e repassa o id da mesa que esta vinculada 
									a conta que sera aberta */?>
								<input type="hidden" id="idMesaModal" name="idMesaModal" value="0">
								<input type="submit" class="btn btn-laranja" value="Abrir Mesa" 
									style="min-width:100%;min-height: 60px;">
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>

				<?php // Modal Fechamento ?>
				<div id="fechar" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<?php /*o form a seguir seleciona uma nova mesa e captura 
								o id com a func .fechamento */?>
							<?php echo form_open('conta/fechar_conta'); ?>
							<div class="modal-header">                      
								<h4 class="modal-title">Fechamento 
									<span class="aviso">forçado</span> da conta 
									<strong class="conta_id"></strong>
								</h4>
								<button type="button" class="close" data-dismiss="modal" 
									aria-hidden="true">&times;
								</button>
							</div>
							<input type="hidden" id="indicadordeid" name="iddaconta" value="0">
							<div class="modal-body">
								<label class="aviso">Motivo</label>
								<select class="form-control aviso" id="motivo" name="flag" >
									<option value="-1">Cliente saiu insatisfeito</option>
									<option value="1">Cliente mudou de mesa</option>
									<option value="-2">Cliente saiu sem pagar</option>
									<option value="2">Cliente se mudou para outra mesa</option>
									<option value="-3">Tablet queimou</option>
									<option value="-4">Erro no sistema</option>
								</select>
								<div class="aviso">
									<div class="alert alert-danger" role="alert">
										Aviso! Você está forçando a conta sem consentimento do 
										<strong>Cliente</strong>.Essa ação faz a gorjeta do garçom 
										<strong>NÃO</strong> ser incluida no sistema.
									</div>
								</div>
								<table id="table_close" class="table">
									<thead>
										<tr>
											<th scope="col">Qtd.</th>
											<th scope="col">Produto</th>
											<th scope="col">Status</th>
											<th scope="col">Preço</th>
										</tr>
									</thead>
									<tbody>
										<div id="dados_fechamento"></div>
										<?php //TBODY alimentado por refresh_modals_balcao.js?>
										<div id="the_end"></div> 
									</tbody>
								</table>
							</div>
							<div class="modal-footer">
								<input id="botao_fechar" type="submit" class="btn btn-default" 
									value="Fechar Conta">
								<button id="close_verificacao" type="button" class="btn btn-default" 
									data-dismiss="modal" style="display:none;">OK
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
				</div>
				
				<?php // Modal Troca de Mesa?>
				<div id="trocar" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<?php // Bota aqui para onde isso vai ser direcionado?>
							<?php echo form_open('conta/Trocar_Mesa'); ?>
							<div class="modal-header">
								<h4 class="modal-title">Trocar Mesa</h4>
								<button type="button" class="close" data-dismiss="modal" 
									aria-hidden="true">&times;
								</button>
							</div>
							<input type="hidden" id="id_conta" name="id_conta">
							<div class="modal-body">
								<label class="">Mesa</label>
								<select class="form-control" name="slct_new_mesa" id="slct_new_mesa">
									<option>Selecione a mesa</option>
									<?php if ($mesas == FALSE): ?>
										<p>Nenhuma mesa cadastrada!</p>
									<?php else: ?>
										<?php foreach ($mesas as $row):
											if($row['statusMesa']==0):?>
												<option value="<?=$row['idMesa'] ?>">Mesa 
													<?=$row['numMesa']?>
												</option>
											<?php endif; 
										endforeach; 
									endif; ?>
								</select>
							</div>
							<div class="modal-footer">
								<input id="botao_fechar" type="submit" class="btn btn-default" 
									value="Trocar Mesa">
								<button id="" type="button" class="btn btn-default" data-dismiss="modal" 
									style="display:none;">OK
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>