<?php
if(isset ($_SESSION['id_mesa'])){
	$cod_mesa = $_SESSION['id_mesa'];
	$cod_conta = $_SESSION['idConta'];
}
?>
<div id="content" style="">
  <!--CONTEÚDO DOS MENUS-->
  <div class="tab-content" style="padding-top: 1%">
    <!--INICIO DA TAB PRATOS-->
    <div id="pratos"class="tab-pane fade in active" style="padding-bottom: 40%">
      <div class="container">
       <?php $count=1; ?>
       <?php if ($pratos == FALSE): ?>
        <p>Nenhum produto cadastrado!</p>
        <?php else: ?>
          <div class="row">
            <?php foreach ($pratos as $row):?>
             <div class="destaque col-sm-4" style="position: relative;float:left;padding-top:5px;padding-left: 0px;">
              <a data-toggle="modal" href="#m_<?=$row['idProduto'];?>">
               <img src="<?=$row['img']?>" class="img-responsive autola" alt="<?=$row['nome'];?>">
             </a>
             <div style="position: absolute;background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));
             display: inline-block;bottom:-1px;width:1000px;max-width: 96.4%;height: 50%;"></div>
             <!--NOME E PREÇO-->
             <div class="nomepreco" style="position: absolute;bottom: 8px;left: 23px;">
               <p style="color: white;"><?=$row['nome']?></p>
               <p style="color: white;"> R$  <?php echo number_format($row['preco'], 2, ',', '.');?></p>
             </div>
             <div class="eofim"  style="position: absolute;bottom: 8px;right: 10%;">
               <form id="form_qtt" name="form_qtt" class="quantidade">
                <div class="input-group input-group-prepend mb-3">
                 <select class="custom-select form-control form-control-lg qty" id="number" 
                 name="number" style="    height: 34px;">
                 <option class="comum" value="1" selected>1</option>
                 <?php if($row['porcao']!=0):?>
                   <option <?php if($row['porcao']==2):?>class="sera"<?php endif;?> value="0.5">½</option>
                 <?php endif;?>
                 <option class="comum" value="2">2</option>
                 <option class="comum" value="3">3</option>
                 <option class="comum" value="4">4</option>
                 <option class="comum" value="5">5</option>
                 <option class="comum" value="6">6</option>
                 <option class="comum" value="7">7</option>
                 <option class="comum" value="8">8</option>
                 <option class="comum" value="9">9</option>
                 <option class="comum" value="10">10</option>
               </select>
               <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

               <div class="input-group-append">
                <a class="btn btn-outline-secondary add-to-cart
                <?php if($row['porcao']==2): echo 'metade';endif;?>" href="#" type="button">Pedir</a>
              </div>
            </div>
          </form>
        </div>

        <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
        <p name="nome_produto" class="esconde"><?php echo $row['nome']; ?></p>
        <h3 name="preco_produto" class="esconde"><?php echo $row['preco'] ?></h3>
      </div>


      <!--Modal do Produto -->
      <div id="m_<?=$row['idProduto'];?>" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">                      
              <h4 class="modal-title"><span><?=$row['nome'];?></span> <span>R$ <?php echo number_format($row['preco'], 2, ',', '.');?></span></h4>
              <!--<button type="button" class="close atualizar" data-dismiss="modal" aria-hidden="true">&times;</button>-->
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Fechar" style="min-width: 0;float:right;margin-right: 10px;">
            </div>
            <div class="modal-body" style="height: 80vh;overflow: none;">
              <div>
                <img src="<?=$row['img']?>" style="height:auto;width:100%">
              </div>
              <div>
                <span class="centrado"><?=$row['descricao'];?></span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!--Testes de linha-->
      <?php if ($count%3==0): ?>
      </div>
      <div class="row">
      <?php endif;
      $count++;
      ?>
    <?php endforeach;
  endif; ?>
</div>
</div>
</div>
<!--FIM DA TAB PRATOS-->
<!--INICIO DA TAB ACOMPANHAMENTOS-->
<div id="acompanhamentos" class="tab-pane fade" style="padding-bottom: 40%">
  <div class="container">
    <?php $count=1; ?>
    <?php if ($acm == FALSE): ?>
      <p>Nenhum produto cadastrado!</p>
      <?php else: ?>
        <div class="row">
          <?php foreach ($acm as $row):?>
            <div class="destaque col-sm-4" style="position: relative;float:left;padding-top:5px;padding-left: 0px;">
              <a data-toggle="modal" href="#m_<?=$row['idProduto'];?>">
                <img src="<?=$row['img']?>" class="img-responsive autola" alt="<?=$row['nome'];?>">
              </a>
              <div style="position: absolute;background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));
              display: inline-block;bottom:-1%;width:1000px;max-width: 96.4%;height: 50%;"></div>
              <!--NOME E PREÇO-->
              <div class="nomepreco" style="position: absolute;bottom: 8px;left: 23px;">
               <p style="color: white;"><?=$row['nome']?></p>
               <p style="color: white;"> R$  <?php echo number_format($row['preco'], 2, ',', '.');?></p>
             </div>
             <div class="eofim"  style="position: absolute;bottom: 8px;right: 10%;">
              <form id="form_qtt" name="form_qtt" class="quantidade">
                <div class="input-group input-group-prepend mb-3">
                  <select class="custom-select form-control form-control-lg qty" id="number" 
                  name="number" style="    height: 34px;">
                  <option class="comum" value="1" selected>1</option>
                  <?php if($row['porcao']!=0):?>
                   <option <?php if($row['porcao']==2):?>class="sera"<?php endif;?> value="0.5">½</option>
                 <?php endif;?>
                 <option class="comum" value="2">2</option>
                 <option class="comum" value="3">3</option>
                 <option class="comum" value="4">4</option>
                 <option class="comum" value="5">5</option>
                 <option class="comum" value="6">6</option>
                 <option class="comum" value="7">7</option>
                 <option class="comum" value="8">8</option>
                 <option class="comum" value="9">9</option>
                 <option class="comum" value="10">10</option>
               </select>
               <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

               <div class="input-group-append">
                <a class="btn btn-outline-secondary add-to-cart
                <?php if($row['porcao']==2): echo 'metade';endif;?>" href="#" type="button">Pedir</a>
              </div>
            </div>
          </form>
        </div>

        <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
        <p name="nome_produto" class="esconde"><?php echo $row['nome']; ?></p>
        <h3 name="preco_produto" class="esconde"><?php echo $row['preco'] ?></h3>
      </div>


      <!--Modal do Produto -->
      <div id="m_<?=$row['idProduto'];?>" class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">                      
              <h4 class="modal-title"><span><?=$row['nome'];?></span> <span>R$ <?php echo number_format($row['preco'], 2, ',', '.');?></span></h4>
              <!--<button type="button" class="close atualizar" data-dismiss="modal" aria-hidden="true">&times;</button>-->
              <input type="button" class="btn btn-default" data-dismiss="modal" value="Fechar" style="min-width: 0;float:right;margin-right: 10px;">
            </div>
            <div class="modal-body" style="height: 80vh;overflow: none;">
              <div>
                <img src="<?=$row['img']?>" style="height:auto;width:100%">
              </div>
              <div>
                <span class="centrado"><?=$row['descricao'];?></span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!--Testes de linha-->
      <?php if ($count%3==0): ?>
      </div>
      <div class="row">
      <?php endif;
      $count++;
      ?>
    <?php endforeach;
  endif; ?>
</div>
</div>
</div>
<!--FIM DA TAB ACOMPANHAMENTOS-->
<!--INICIO DA TAB SOBREMESAS-->
<div id="sobremesas" class="tab-pane fade" style="padding-bottom: 40%">

  <div class="container">
    <?php $count=1; ?>
    <?php if ($sobremesas == FALSE): ?>
      <p>Nenhum produto cadastrado!</p>
      <?php else: ?>
        <div class="row">
          <?php foreach ($sobremesas as $row):?>
           <div class="destaque col-sm-4" style="position: relative;float:left;padding-top:5px;padding-left: 0px;">
            <a data-toggle="modal" href="#m_<?=$row['idProduto'];?>">
              <img src="<?=$row['img']?>" class="img-responsive autola" alt="<?=$row['nome'];?>">
            </a>
            <div style="position: absolute;background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));
            display: inline-block;bottom:-1%;width:1000px;max-width: 96.4%;height: 50%;"></div>

            <!--NOME E PREÇO-->
            <div class="nomepreco" style="position: absolute;bottom: 8px;left: 23px;">
             <p style="color: white;"><?=$row['nome']?></p>
             <p style="color: white;"> R$  <?php echo number_format($row['preco'], 2, ',', '.');?></p>
           </div>
           <div class="eofim"  style="position: absolute;bottom: 8px;right: 10%;">
            <form id="form_qtt" name="form_qtt" class="quantidade">
              <div class="input-group input-group-prepend mb-3">
                <select class="custom-select form-control form-control-lg qty" id="number" 
                name="number" style="    height: 34px;">
                <option class="comum" value="1" selected>1</option>
                <?php if($row['porcao']!=0):?>
                 <option <?php if($row['porcao']==2):?>class="sera"<?php endif;?> value="0.5">½</option>
               <?php endif;?>
               <option class="comum" value="2">2</option>
               <option class="comum" value="3">3</option>
               <option class="comum" value="4">4</option>
               <option class="comum" value="5">5</option>
               <option class="comum" value="6">6</option>
               <option class="comum" value="7">7</option>
               <option class="comum" value="8">8</option>
               <option class="comum" value="9">9</option>
               <option class="comum" value="10">10</option>
             </select>
             <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

             <div class="input-group-append">
              <a class="btn btn-outline-secondary add-to-cart
              <?php if($row['porcao']==2): echo 'metade';endif;?>" href="#" type="button">Pedir</a>
            </div>
          </div>
        </form>
      </div>

      <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
      <p name="nome_produto" class="esconde"><?php echo $row['nome']; ?></p>
      <h3 name="preco_produto" class="esconde"><?php echo $row['preco'] ?></h3>
    </div>



    <!--Modal do Produto -->
    <div id="m_<?=$row['idProduto'];?>" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">                      
            <h4 class="modal-title"><span><?=$row['nome'];?></span> <span>R$ <?php echo number_format($row['preco'], 2, ',', '.');?></span></h4>
            <!--<button type="button" class="close atualizar" data-dismiss="modal" aria-hidden="true">&times;</button>-->
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Fechar" style="min-width: 0;float:right;margin-right: 10px;">
          </div>
          <div class="modal-body" style="height: 80vh;overflow: none;">
            <div>
              <img src="<?=$row['img']?>" style="height:auto;width:100%">
            </div>
            <div>
              <span class="centrado"><?=$row['descricao'];?></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--Testes de linha-->
    <?php if ($count%3==0): ?>
    </div>
    <div class="row">
    <?php endif;
    $count++;
    ?>
  <?php endforeach;
endif; ?>
</div>
</div>
<br/><br/><br/>
</div>
<!--FIM DA TAB SOBREMESAS-->
<!--INICIO DA TAB BEBIDAS-->
<div id="bebidas" class="tab-pane fade" style="padding-bottom: 40%">
 <div class="container">
  <?php $count=1; ?>
  <?php if ($bebidas == FALSE): ?>
    <p>Nenhum produto cadastrado!</p>
    <?php else: ?>
      <div class="row">
        <?php foreach ($bebidas as $row):?>
         <div class="destaque col-sm-4" style="position: relative;float:left;padding-top:5px;padding-left: 0px;">
          <a data-toggle="modal" href="#m_<?=$row['idProduto'];?>">
            <img src="<?=$row['img']?>" class="img-responsive autola" alt="<?=$row['nome'];?>">
          </a>
          <div style="position: absolute;background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));display: inline-block;bottom:-1%;width:1000px;max-width: 96.4%;height: 50%;"></div>
          <!--NOME E PREÇO-->
          <div class="nomepreco" style="position: absolute;bottom: 8px;left: 23px;">
           <p style="color: white;"><?=$row['nome']?></p>
           <p style="color: white;"> R$  <?php echo number_format($row['preco'], 2, ',', '.');?></p>
         </div>
         <div class="eofim"  style="position: absolute;bottom: 8px;right: 10%;">
          <form id="form_qtt" name="form_qtt" class="quantidade">
            <div class="input-group input-group-prepend mb-3">
              <select class="custom-select form-control form-control-lg qty" id="number" name="number" style="    height: 34px;">
                <option value="1" selected>1</option>
                <?php if($row['porcao']!=0):?>
                 <option <?php if($row['porcao']==2):?>class="sera"<?php endif;?> value="0.5">½</option>
               <?php endif;?>
               <option value="2">2</option>
               <option value="3">3</option>
               <option value="4">4</option>
               <option value="5">5</option>
               <option value="6">6</option>
               <option value="7">7</option>
               <option value="8">8</option>
               <option value="9">9</option>
               <option value="10">10</option>
             </select>
             <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

             <div class="input-group-append">
              <a class="btn btn-outline-secondary add-to-cart" href="#" type="button">Pedir</a>
            </div>
          </div>
        </form>
      </div>

      <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
      <p name="nome_produto" class="esconde"><?php echo $row['nome']; ?></p>
      <h3 name="preco_produto" class="esconde"><?php echo $row['preco'] ?></h3>
    </div>

    <!--Modal do Produto -->
    <div id="m_<?=$row['idProduto'];?>" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">                      
            <h4 class="modal-title"><span><?=$row['nome'];?></span> <span>R$ <?php echo number_format($row['preco'], 2, ',', '.');?></span></h4>
            <!--<button type="button" class="close atualizar" data-dismiss="modal" aria-hidden="true">&times;</button>-->
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Fechar" style="min-width: 0;float:right;margin-right: 10px;">
          </div>
          <div class="modal-body" style="height: 80vh;overflow: none;">
            <div>
              <img src="<?=$row['img']?>" style="height:auto;width:100%">
            </div>
            <div>
              <span class="centrado"><?=$row['descricao'];?></span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--Testes de linha-->
    <?php if ($count%3==0): ?>
    </div>
    <div class="row">
    <?php endif;
    $count++;
    ?>
  <?php endforeach;
endif; ?>
</div>
</div>
</div>
<!--FIM DA TAB BEBIDAS-->
</div>
<!--Modal de Confirmar Pedido -->
<div id="confirmar" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="form1">
        <div class="modal-header">                      
          <h4 class="modal-title">Confirmar Pedido</h4>
          <!--<button type="button" class="close atualizar" data-dismiss="modal" aria-hidden="true">&times;</button>-->
          <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar" style="min-width: 0;float:right;margin-right: 10px;">
        </div>

        <div class="modal-body">
          <table id="conf_pedido" class="table">
            <thead>
              <tr>
                <th scope="col" style="display: none">Qtd.</th>
                <th scope="col">Produto</th>
                <th scope="col">Observações</th>
                <th scope="col">Preço<span style="font-size:8px;">(Total)</span></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <!--Essa é uma tabela hipotetica, todos os seus dados estão em adicionar_pedido.js-->
            </tbody>
          </table>
        </div>

        <div class="modal-footer">
         <input id="confirm" type="submit" class="btn btn-laranja confirmapedido atualizar" style="min-height: 60px;" value="Adicionar">
         <!--Este submit redireciona os dados presentes na tabela para o confirma_pedido.js que por sua vez envia os dados para o bd-->
       </div>
     </form>
   </div>
 </div>
</div>