<?php
class Cliente_apt extends CI_Controller
{	
	/*Apresentação do cliente*/
	public function index(){
		$this->load->model('mesa_model');
		$results=$this->mesa_model->Mesas();
		$dados['mesas']=$results;
		$this->load->view('clienteapp/apresentacao_mesa_view',$dados);
	}
	
	public function seted_account(){
		$id_conta = $this->uri->segment(3, 0);
		
		$dados = array('idConta' => $id_conta, 'status_conta' => 0);
		$this->load->model('conta_model');
		$confirm = $this->conta_model->Verificar_Status('tbconta', $dados);
		if($confirm){
			$this->load->view('clienteapp/aguardando_conta');
		}		
		else{
			/*ultilizando o id da conta passado pela URI localhost/rest/cliente_apt/seted_account/IDCONTA eu 
			recupero o id da mesa para só então criar a SESSION caso a conta esteja aberta*/		
			$dados = array('idConta' => $id_conta, 'status_conta' => 1);
			$this->load->model('conta_model');
			$confirm = $this->conta_model->Verificar_Status('tbconta', $dados);
			if($confirm){
				//caso a conta procurada esteja aberta o sistema identifica a mesa
				$this->load->model('conta_model');
				$id_mesa = $this->conta_model->Identificar_Mesa($id_conta);
				if($id_mesa){
					// e com os id da mesa ele pega o numero dela
					$this->load->model('mesa_model');
					$num_mesa = $this->mesa_model->Select_Mesa($id_mesa);
					$newdata = array('id_mesa' => $id_mesa,'num_mesa'=>$num_mesa,
					'idConta' => $id_conta, 'status_conta' => 1);
					$this->session->set_userdata($newdata);
					//com todos os dados em mãos nos montamos a SESSION que carregará os dados
					$redirect = site_url("cliente_apt/todos");
					header("location:$redirect");
					//e nos direcionamos a pagina do cardapio
				}		
			}else{
				$dados = array('idConta' => $id_conta, 'status_conta' => 3);
				$this->load->model('conta_model');
				$confirm = $this->conta_model->Verificar_Status('tbconta', $dados);
				if($confirm){
					unset( $_SESSION['idConta'], $_SESSION['id_mesa'], $_SESSION['num_mesa'], $_SESSION['status_conta']); 
					$redirect = site_url();
					header("location:$redirect");
				}
			}
		}
	}
	
	public function seted_mesa(){
			//caso os dados venham do POST, ou seja ja passaram pela apresentacao_mesa_view
		if(isset($_POST['slct'])){
			$id_mesa = $_POST['slct'];
				//monta a session
			$this->load->model('mesa_model');
			$num_mesa = $this->mesa_model->Select_Mesa($id_mesa);			
			$newdata = array('id_mesa' => $id_mesa,'num_mesa'=>$num_mesa);
			$this->session->set_userdata($newdata);
			return true;
		}
			//se não vamos a pagina onde ela será criada
		else{
			$this->load->model('mesa_model');
			$results=$this->mesa_model->Mesas();
			$dados['mesas']=$results;
			$this->load->view('clienteapp/apresentacao_mesa_view',$dados);
		}
	}
	
	public function loading_encerramento(){
		//guarda o id da conta até que outra seja aberta para validações futuras
		if(isset($_SESSION['idConta'])){
		$id=$_SESSION['idConta'];	
	}else{
		$id=$_SESSION['lastIdConta'];
	}
	//Captura o historico para que o cliente possa velo enquanto paga a conta e o repassa para a view de fechamento 
		$this->load->model('conta_model');
		$retorno=$this->conta_model->HistoricoPOS($id);
		$data['historico']=$retorno;
		$this->load->view('clienteapp/fechando_conta',$data);
	}		
	
	/*Sub-menus*/
	public function todos(){
			//Load Models
		$this->load->model('produtos_model');
		$this->load->model('conta_model');
			//Pega todos
		$result_t=$this->produtos_model->Todos('tbprodutos','0');
		$dados['todos']=$result_t;
			//Pega bebidas
		$result_b=$this->produtos_model->Todos('tbprodutos','"4"');
		$dados['bebidas']=$result_b;
			//Pega sobremesas
		$result_s=$this->produtos_model->Todos('tbprodutos','"3"');
		$dados['sobremesas']=$result_s;
			//Pega acompanhamento
		$result_a=$this->produtos_model->Todos('tbprodutos','"2"');
		$dados['acm']=$result_a;
			//Pega Pratos Principais
		$result_p=$this->produtos_model->Todos('tbprodutos','"1"');
		$dados['pratos']=$result_p;
		//$results3=$this->conta_model->Tela_fecha();
		//$data['fechar']=$results3;

			//Load Views
		$this->load->view('clienteapp/menu_menu_view');
		$this->load->view('clienteapp/todos_menu_view',$dados);
		$this->load->view('clienteapp/footer_menu_view');
	}

	public function get_refresh_modals(){
		//futuramente seria interessante unir esse metodo ao de mesmo nome presente no controller Balcao poissão praticamente o mesmo
		$id=$_POST['id_conta'];
		$this->load->model('conta_model');
		//Pega Histórico e Fechamento
		$results2=$this->conta_model->Historico($id);
		$data['historico']=$results2;
		$results3=$this->conta_model->Tela_fecha($id);
		$data['fechar']=$results3;
		// Retornando o data em formato JSON
		echo json_encode($data);
	}
}
?>