<div id="page-wrapper" style="height:100vh;overflow: auto">
	<div class="container-fluid">
		<div class="row" style="margin-top:10%">
			<div class="col-md-4">
				<div class="card-body">
					<h5 class="card-title"><i class="fa fa-address-book-o" aria-hidden="true"></i> Fábio Aguiar</h5>
					<p class="card-text"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail: <a style="cursor: pointer">fabioaguiar360@gmail.com</a></p>
					<p class="card-text"><i class="fa fa-phone-square" aria-hidden="true"></i> Contato: (85) 9 9621-6603</p>
				</div>
			</div>
			<div class="col-md-4">
				<!--<img class="card-img-top" src="..." alt="Card image cap">-->
				<div class="card-body">
					<h5 class="card-title"><i class="fa fa-address-book-o" aria-hidden="true"></i> Gustavo Carneiro</h5>
					<p class="card-text"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail: <a style="cursor: pointer">gustavocarneiro.a@gmail.com</a></p>
					<p class="card-text"><i class="fa fa-phone-square" aria-hidden="true"></i> Contato: (85) 9 8653-0828</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card-body">
					<h5 class="card-title"><i class="fa fa-address-book-o" aria-hidden="true"></i> Roberto Dutra</h5>
					<p class="card-text"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail: <a style="cursor: pointer">robertodutraemp@gmail.com</a></p>
					<p class="card-text"><i class="fa fa-phone-square" aria-hidden="true"></i> Contato: (85) 9 9634-9243</p>
				</div>
			</div>
		</div>
	</div>
</div>