<?php
//Existem 4 tipos de alert, 0 para abertura, 1 para pedido, 2 para garçom e 3 para fechamento
class alerts_ajax extends CI_Model  {
	function Return_ajax($id_conta){
		$where = 'tbalerts.status = 0 and tbalerts.idConta='.$id_conta;
		$contagem = $this->db->where($where)->count_all_results('tbalerts');

		$where = 'tbalerts.status = 0 and tbalerts.tipoAlert=2 and tbalerts.idConta='.$id_conta;
		$garcom = $this->db->where($where)->count_all_results('tbalerts');
		$id_call = $this->db->select('tbalerts.idAlert')
		->get_where('tbalerts', $where)->result_array();
    	// Retornando a contagem de alertas em formato JSON
		$cont  = array('quant' => $contagem, 'quant_garcom' => $garcom, 'id_call' => $id_call);
    	// Retornando a quantidade em formato JSON
		echo json_encode($cont);
	}
	
	function GarçomSolicitado(){
		$where = "tbalerts.tipoAlert='2' and tbalerts.status='0' and tbconta.status_conta='1'";
		return $results2=$this->db->select("tbalerts.*,tbconta.*")
		->join("tbconta", "tbalerts.idConta=tbconta.idConta")
		->where($where)
		->get("tbalerts")->result_array();
	}

	function Reset_alerts($id_conta){
		$replacement_data = array('status'=> 1);
		$where = 'tbalerts.status = 0 and tbalerts.tipoAlert!=2 and tbalerts.idConta='.$id_conta;
		$this->db->where($where);
		$this->db->update('tbalerts', $replacement_data); 
		$this->load->model('alerts_ajax');
		$this->alerts_ajax->Return_ajax($id_conta);
	}
}
?>