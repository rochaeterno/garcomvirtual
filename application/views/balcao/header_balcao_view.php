<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Fortaleza');
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Tela de Balcão </title>
	<!--JavaScripts-->
	<script src="<?php echo base_url();?>js/jquery.js"></script>
	<script src="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/js/bootstrap.min.js"></script>
	<!--Ícones e Bootstrap-->
	<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/css/bootstrap.min.css">
	<!--Scripts-->
	<script type="text/javascript">count_contas = '<?php echo site_url("conta/contar_contas");?>'</script>
  <script type="text/javascript">count_mesas = '<?php echo site_url("mesa/contar_mesas");?>'</script>
  <script type="text/javascript">url_account='<?php echo site_url("conta/identificar_mesa");?>'</script>
	<script type="text/javascript">url_status='<?php echo site_url("mesa/verificar_status"); ?>'</script>
  <script type="text/javascript">call_history='<?php echo site_url("balcao/pull_history"); ?>'</script>
  <script type="text/javascript">count_alerts='<?php echo site_url("balcao/refresh_alerts"); ?>'</script>
  <script type="text/javascript">reset_alerts='<?php echo site_url("balcao/reset_alerts"); ?>'</script>
  <script type="text/javascript">url_refresh_modals = '<?php echo base_url(); ?>' + 'index.php/balcao/get_refresh_modals';</script>
  <script type="text/javascript">status_update = '<?php echo site_url("balcao/status_update"); ?>'</script>
	<script src="<?php echo base_url();?>js/balcao_view.js"></script>
  <script src="<?php echo base_url();?>js/table_selector.js"></script>
  <script src="<?php echo base_url();?>js/refresh_modals_balcao.js"></script>
  <script src="<?php echo base_url();?>js/atualizar_status.js"></script>
	<script type="text/javascript">
   	
	$(document).ready(function(){ 
		// Ativa o tooltip as 2 funçoes fazem o mesmo
		$('[data-toggle="tooltip"]').tooltip();
		//a função a seguir insere o id da conta nos modais caso necessario
		$(document).on('click','.fechamento',function(){
			if($(".conta_id").empty()){
				$continha=$(this).attr("idConta");
				$(".conta_id").append($continha);
				$('#indicadordeid').val($(this).attr('idConta'));
				$('#id_conta').val($(this).attr('idConta'));
			}
		});
		
	// Selecionar todas as checkbox
	var checkbox = $('table tbody input[type="checkbox"]');
	$("#selectAll").click(function(){
		if(this.checked){
			checkbox.each(function(){
       this.checked = true;
     });
		}else{
			checkbox.each(function(){
				this.checked = false;                        
			});
		}
	});
	checkbox.click(function(){
		if(!this.checked){
			$("#selectAll").prop("checked", false);
		}
	});
	//Abre e fecha a side-bar
	$('#sidebarCollapse').on('click', function () {
		$('#sidebar').toggleClass('active');
	});
  $('#dismiss, .overlay').on('click', function () {
    $('#sidebar').removeClass('active');
    $('.overlay').fadeOut();
  });
  $(".Mesa-icon").click(function(){
    $(".Mesa-corpo").show();
  });
    //Select que mostra divs
    $("#select_mesas").on('change', function(){
      $('.aparecer2').hide();
      $('#' + this.value).show(); 
    });
    //Forçar Fechamento
    $(".forcar").click(function(){
      $(".aviso").show();
    });
    $(".comum").click(function(){
      $(".aviso").hide();
    })

    //Remover pisca - Adicionar Background antigo
    $(".new1").on( "click", function( event ) {
      $( event.target ).closest( ".Mesa" ).addClass("Mesa-laranja" );
      $( event.target ).closest( ".Mesa" ).removeClass("Mesa-verde" );
      $( event.target).closest(".msg_mesa").text("Em uso.");
      $( event.target ).closest( ".Mesa" ).removeClass("new1" );
    });
    $(".new2").on( "click", function( event ) {
      $( event.target).closest(".msg_mesa").text("Em uso.");
      $( event.target ).closest( ".Mesa" ).addClass("Mesa-laranja" );
      $( event.target ).closest( ".Mesa" ).removeClass("Mesa-azul" );
      $( event.target ).closest( ".Mesa" ).removeClass("new2");
    });

  });


</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/crud.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/sb-admin.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/style5.css">
<style type="text/css">
.tdbotao-verde, .tdbotao-azul, .tdbotao-laranja{cursor: pointer;}
td[colspan]:not([colspan="1"]) {text-align: center;}
#nMesa{display: none;}
fieldset.scheduler-border{display:none}
legend.scheduler-border{border-bottom:none;}
.forcar{cursor: pointer;}
.preto{background-color: black;padding: 6px;}
.aviso{display: none;}
span.aviso{color:red;}
.modal-content .modal-body{height:250px;overflow:auto;}
.testes{overflow: auto;height:85vh;}
.blink {
  animation: blink-animation 3s  infinite;
  -webkit-animation: blink-animation 3s infinite;
}
@keyframes blink-animation {
  30%{
    background-color: grey;
  }
  }
}
@-webkit-keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
</style>

</head>
<body>
 <!--Navegador de cima, pode ser incluido mais coisas-->
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#C55D03;border-color:#C55D03;">
  <div class="navbar-header">
    <a class="navbar-brand" href="<?php echo site_url('balcao/index');?>" style="color:white;"><strong>Balcão - Mesas</strong></a>
  </div>
  <!--Menu do canto direito, podem ser incluidos mais botões-->
    <ul class="nav navbar-right top-nav">
      <li class="dropdown" >
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:white;"><i class="fa fa-user"></i> <?=$_SESSION['nome']?> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li>
            <a href="<?php echo site_url('Login/logout'); ?>">
              <i class="fa fa-sign-out" aria-hidden="true"></i>
              Logout
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </nav>