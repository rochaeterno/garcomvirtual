<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$cod_conta = null;
$cod_mesa = null;
if(isset ($_SESSION['id_mesa'],$_SESSION['idConta'])){
	$cod_mesa = $_SESSION['id_mesa'];
	$cod_conta = $_SESSION['idConta'];
	if(isset ($_SESSION['status_conta'])){
		$status_teste = $this->session->status_conta;
		if($status_teste==1){
			//ABRE A TELA DE INICIO DO CLIENTE
			$redirect = site_url("cliente_apt/todos");
			header("location:$redirect");
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Bem-vindo</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--JavaScripts-->
  <script src="<?php echo base_url();?>js/jquery.js"></script>
  <script src="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/js/bootstrap.min.js"></script>
  <!--Ícones e Bootstrap-->
  <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/css/bootstrap.min.css">

  <!--NOVO-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style3.css">

  <style>
  .fa {
    margin-left: -12px;
    margin-right: 8px;
  }
</style>
<script type="text/javascript">url_status='<?php echo site_url("conta/verificar_status"); ?>';</script>
<script type="text/javascript">url_cardapio='<?php echo site_url("cliente_apt/todos"); ?>';</script>
<script type="text/javascript">$(document).ready(function(){setTimeout(function(){ $("#demora").show(1000);}, 15000);});</script>
<script src="<?php echo base_url();?>js/aguardando_conta.js"></script>
</head>
<body style="background-color: black;">
  <form id="form1"> 
   <input type="hidden" id="idMesa" name="idMesa" value="<?php echo $cod_mesa;?>"/>
   <input type="hidden" id="status" name="status" value="1"/>
 </form>
 <div class="bgimg w3-display-container w3-animate-opacity w3-text-white">
  <div class="w3-display-middle">
  	<!--NOVO: Loading.
  		Existem 3 opções de loading-->
      <h1 class="w3-jumbo w3-animate-top"><i class="fa fa-refresh fa-spin"></i></h1>
     <div id="demora" style="display:none">
        <h1 style="background:none;margin-bottom: 20px;">Demorando muito?</h1>
        <button type="button" id="atualizar" class="w3-button"  onClick="window.location.reload()" style="background:none;">Clique aqui</button>

      </div>
    </div>
  </div>
<!--<script type="text/javascript">
	//ainda pensando em como desenvolver essa parte
	$(".solicitarAbertura").click(function(){
		$.getJSON("<?php echo base_url();?>/json/cacheMesa.txt", function(result)
    {
			$.each(result, function(i, field)
            {
				$("div").append(field + " ");
			}
		}
	});
</script>-->

</body>
</html>
<div id="conteudo"></div>
</BODY>
</HTML>