CREATE VIEW lastalert AS
SELECT tbmesa.idMesa,
tbconta.status_conta,tbalerts.idConta,tbalerts.status,tbalerts.tipoAlert,tbalerts.idAlert
FROM tbalerts
JOIN tbconta ON tbconta.idConta=tbalerts.idConta

JOIN tbmesa ON tbconta.idMesa=tbmesa.idMesa
	
WHERE tbconta.status_conta=1 AND tbalerts.idAlert IN

(SELECT MAX(idAlert) 
FROM tbalerts
GROUP BY idConta DESC)