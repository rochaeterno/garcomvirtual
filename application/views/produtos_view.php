<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="page-wrapper" style="height:100vh;overflow: auto">
    <div class="container-fluid">
    <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Produtos
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-6">
						<h2>Gerenciar <b>Produtos</b></h2>
					</div>
					<div class="col-sm-6">
						<!--Nav da tabela-->
						<a href="#adicionar" class="btn btn-laranja" data-toggle="modal">
							<i class="fa fa-plus-circle" aria-hidden="true"></i><span>Novo produto</span>
						</a>
					</div>
				</div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<!--Colunas-->
						<th>ID</th>
						<th>Nome</th>
						<th>Tipo</th>
						<th>Foto</th>
						<th>Descrição</th>
						<th>Preço</th>
						<th>Dispon.</th>
						<th style="display:none">Destaque</th>
						<th>Porção</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($produtos == FALSE): ?>
						<p>Nenhum produto cadastrado!</p>
						<?php else: ?>
							<?php foreach ($produtos as $row):?>
								<tr>
									<!--Linhas-->
									<td><?=$row['idProduto']?></td>
									<td><?=$row['nome']?></td>
									<td><?=$row['tipo']?></td>
									<td>
										<a href="<?=$row['img']?>" target="_blank">Link</a>
										<a href="#updatefoto<?=$row['idProduto']?>" data-toggle="modal">
											<i class="fa fa-file-image-o" aria-hidden="true"></i>
										</a>
									</td>
									<td><?=$row['descricao']?></td>
									<td><small style="font-size: 10px">R$</small><?php echo number_format($row['preco'], 2, ',', '.');?></td>
									<td>
									<?php echo form_open_multipart('Crud/Atualizar_Disponibilidade', 'id="disp"');?> 
									<input type="hidden" name="id_produto" value="<?=$row['idProduto']?>">
									<?php
									if($row['disponibilidade']==0):?>
									<input type="hidden" name="change" value="1">
									<button class="btn fa fa-check"></button>
									<?php else:?>
									<input type="hidden" name="change" value="0">
									<button class="btn fa fa-times"></button>
									<?php endif;?>
									</form></td>
										<td style="display:none"><?php
										switch($row['nivelDestaque']){
											case 1:echo "Destaque 1";break;
											case 2:echo "Destaque 2";break;
											case 3:echo "Destaque 3";break;
											case 4:echo "Destaque 4";break;
											case 5:echo "Destaque 5";break;
											case 6:echo "Destaque 6";break;
											default:echo "Produto comum";break;
										}

										?></td>
										<td><?=$row['porcao'];?></td>
										<td>
											<a href="#editar<?=$row['idProduto']?>" class="edit" data-toggle="modal">
												<i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											<!--Editar Foto-->
											<div id="updatefoto<?=$row['idProduto']?>" class="modal fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<?php echo form_open_multipart('Crud/EditarFotoProduto', 'id="novafoto"');?> 

														<div class="modal-header">                      
															<h4 class="modal-title">Atualizar foto de <?=$row['nome']?></h4>
															<button type="button" class="close" data-dismiss="modal" 
															aria-hidden="true">&times;</button>
														</div>

														<div class="modal-body">                    
															<div class="form-group">
																<input type="hidden" name="id" value="<?=$row['idProduto']?>">
																<input type="hidden" name="nome" value="<?=$row['nome']?>">
																<label for="imgProd">Inserir nova foto</label>
																<input type="file" class="form-control-file" id="imgProd" 
																name="imgProd" aria-describedby="fileHelp">
																<small id="fileHelp" class="form-text text-muted">
																Insira aqui uma foto com até 2MB.</small>
															</div>
														</div>

														<div class="modal-footer">
															<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
															<input type="submit" class="btn btn-success" value="Enviar">
														</div>
													</form>
												</div>
											</div>
										</div>

										<!-- Editar Modal HTML -->
										<div id="editar<?=$row['idProduto']?>" class="modal fade">
											<div class="modal-dialog">
												<div class="modal-content">

													<?php echo form_open('Crud/EditarProdutos');?>
													<input type="hidden" name="id" value="<?=$row['idProduto']?>">
													<input type="hidden" name="caminho" value="">
													<div class="modal-header">                      
														<h4 class="modal-title">Editar Produto <?=$row['nome']?></h4>
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													</div>

													<div class="modal-body">                    
														<div class="form-group">
															<label>Nome</label>
															<input type="text" class="form-control" value="<?=$row['nome']?>" name="nomeProd" required>
														</div>

														<div class="form-group">
															<label>Tipo</label>
															<select class="form-control" id="TipoProd" name="tipoProd" required>
																<?php 
																$sl1='';$sl2='';$sl3='';$sl4='';
																switch ($row['tipo']) {
																	case '1':
																	$sl2='selected';
																	break;
																	case '2':
																	$sl1='selected';
																	break;
																	case '3':
																	$sl4='selected';
																	break;
																	case '4':
																	$sl3='selected';
																	break;
																	default:break;
																}?>
																<option value="1" <?php echo $sl1;?>>Prato Principal</option>
																<option value="2" <?php echo $sl2;?>>Acompanhamento</option>
																<option value="4" <?php echo $sl3;?>>Bebidas</option>
																<option value="3" <?php echo $sl4;?>>Sobremesas</option>
															</select>
														</div> 

														<div class="form-group">
															<label>Descrição</label>
															<textarea class="form-control" name="descProd" required><?=$row['descricao']?></textarea>
														</div>

														<div class="form-group">
															<label>Preço</label>
															<div class="input-group">
															<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
															<input type="number" class="form-control" name="precoProd" value="<?=$row['preco']?>" step=".01" required></input>
															</div>
														</div>

														<div class="form-group">
															<label>Disponibilidade:</label>
															<?php $dsp='';
															if($row['disponibilidade']==0):
																$dsp='checked';
															endif;?>
															<input type="checkbox" name="dispProd" class="form control form-check-input" <?php echo $dsp;?>>
														</div>
														<div class="form-group">
															<label>Porção:</label>
															<div class="form-check">
																<?php 
																$sl1='';$sl2='';$sl3='';$sl4='';
																switch ($row['porcao']) {
																	case '0':
																	$sl1='checked';
																	break;
																	case '1':
																	$sl2='checked';
																	break;
																	case '2':
																	$sl3='checked';
																	break;
																	default:break;
																}?>
																<input class="form-check-input" type="radio" name="porcao" id="p1" value="0" <?php echo $sl1?>>
																<label class="form-check-label" for="p1">
																	Não é vendido em porção
																</label>
															</div>
															<div class="form-check">
																<input class="form-check-input" type="radio" name="porcao" id="p2" value="1" <?php echo $sl2?>>
																<label class="form-check-label" for="p2">
																	Pode ser vendido em porção separada
																</label>
															</div>
															<div class="form-check">
																<input class="form-check-input" type="radio" name="porcao" id="p3" value="2" <?php echo $sl3?>>
																<label class="form-check-label" for="p3">
																	É vendido em porções, mas precisa completar um pedido inteiro
																</label>
															</div>
														</div>
														<div class="form-group" style="display:none">
															<label>Destaque:<br>0 para Não Destaque 
																<br>1 a 6 para posição de destaque</label>
																<input type="number" class="form-control" min="0" max="6" value="<?=$row['nivelDestaque'];?>" name="nivelDestaque">
															</div>

														</div>

														<div class="modal-footer">
															<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
															<input type="submit" class="btn btn-success" value="Adicionar">
														</div>
														<?php echo form_close();?>
													</div>
												</div>
											</div>

											<a class="delete" href="#excluirprod<?=$row['idProduto']?>" data-toggle="modal">
												<i class="fa fa-trash-o" aria-hidden="true"></i>
											</a>

											<!-- Excluir Modal HTML -->
											<div id="excluirprod<?=$row['idProduto']?>" class="modal fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">                      
															<h4 class="modal-title">
																Excluir Produto <strong><?=$row['nome']?></strong>
															</h4>
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														</div>
														<div class="modal-body">                    
															<p>Tem certeza que deseja apagar esse registro?</p>
															<p class="text-warning"><small>Essa ação não pode ser desfeita.</small></p>
														</div>
														<div class="modal-footer">
															<?php echo form_open('Crud/excluirProduto');?>
															<input type="hidden" name="id" value="<?=$row['idProduto']?>">

															<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
															<input type="submit" class="btn btn-danger" value="Excluir">
															<?php echo form_close();?>
														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
								<?php endforeach;
							endif; ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- Adicionar Modal HTML -->
			<div id="adicionar" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<?php //echo $error;?>

						<?php echo form_open_multipart('produtos/Insere_produtos', 
						'id="cadastraProdutos"');//chama o form de de inserir Produtos?> 

						<div class="modal-header">                      
							<h4 class="modal-title">Novo Produto</h4>
							<button type="button" class="close" data-dismiss="modal" 
							aria-hidden="true">&times;</button>
						</div>

						<div class="modal-body">                    
							<div class="form-group">
								<label>Nome</label>
								<input type="text" class="form-control" name="nomeProd" required>
							</div>

							<div class="form-group">
								<label>Tipo</label>
								<select class="form-control" id="tipoProd" name="tipoProd" required>
									<option value="1" selected>Prato Principal</option>
									<option value="2">Acompanhamento</option>
									<option value="3">Sobremesas</option>
									<option value="4">Bebidas</option>
								</select>
							</div> 

							<div class="form-group">
								<label for="imgProd">Foto do Produto</label>
								<input type="file" class="form-control-file" id="imgProd" 
								name="imgProd" aria-describedby="fileHelp">
								<small id="fileHelp" class="form-text text-muted">
								Insira aqui uma foto com até 2MB.</small>
							</div>

							<div class="form-group">
								<label>Descrição</label>
								<textarea class="form-control" name="descProd" required></textarea>
							</div>

							<div class="form-group">
								<label>Preço</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
									<input type="number" class="form-control" name="precoProd" step=".01" required></input>
								</div>
							</div>

							<div class="form-group">
								<label>Disponibilidade:</label>
								<input type="checkbox" class="form control form-check-input" name="dispProd" checked>
							</div>

							<div class="form-group">
								<label>Porção:</label>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="porcao" id="p1" value="0" checked>
									<label class="form-check-label" for="p1">
										Não é vendido em porção
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="porcao" id="p2" value="1">
									<label class="form-check-label" for="p2">
										Pode ser vendido em porção separada
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="porcao" id="p3" value="2">
									<label class="form-check-label" for="p3">
										É vendido em porções, mas precisa completar um pedido inteiro
									</label>
								</div>
							</div>

							<div class="form-group" style="display:none">
								<label>Destaque:<br><small>0 para Não Destaque 
									<br>1 a 6 para posição de destaque</small></label>
									<input type="number" name="nivelDestaque" class="form-control" min="0" max="6" value="0">
								</div>

							</div>

							<div class="modal-footer">
								<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
								<input type="submit" class="btn btn-success" value="Adicionar">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>