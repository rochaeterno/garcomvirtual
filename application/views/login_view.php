<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/login.css');?>">
	<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/css/bootstrap.min.css">
	<script src="<?php echo base_url();?>js/jquery.js"></script>
	<script src="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		window.setTimeout(function() {
			$(".alert").fadeTo(500, 0).slideUp(500, function(){
				$(this).remove(); 
			});
		}, 4000);
	</script>
</head>
<body>
	<?php if($mensagem!='99'):?>
		
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Erro!</strong> <?php echo $mensagem;?>
		</div>

	<?php endif;?>
	<div class="login-page">
		<div class="form" >
			<?php echo validation_errors(); ?>			
			<?php echo form_open('login/validacao', 'class="login" id="form_login"');?>
			<input type="text" id="login" name="loginUsu" value="" placeholder="digite seu nome de usuário" required>
			<input type="password" id="senha" name="senhaUsu" value="" placeholder="digite sua senha" required>
			<button type="submit" id="entrar" name="entrar" value="Submit">login</button>
		</form>
	</div>
</div>
</body>
</html>