// Função responsável por atualizar os alerts
/*Ao ser chamada esta função utiliza a função Count_alerts do conta_model para contar todos os alerts presentes na tabela tbalerts, com este numero ela testa a diferença deste com aquele trazido pela variavel contagem 
inicial que por sua vez e capturado do mesmo destinatario quando a pagina abre pela primeira dessa forma o sistema sabe quantos novos alertas existem no banco e adiciona isso a DOM a cada 10 sec*/
var contador = 0;
function atualizar()
{
	var contagem_inicial=$('#count_var').val()
	$.post(lin, function (cont) { 
		contador = contador + cont.quant - Number(contagem_inicial);
		//alert(contador);
		if(contador>0){
			$('#contador').html(contador);
			$('#count_var').val(cont.quant);
			$('#numeroalertas').addClass("new");
			$('audio').get(0).play();
		}	
		}, 'JSON');   
}
// Definindo intervalo que a função será chamada
setInterval("atualizar()", 10000);

// Quando carregar a página
$(function() {
    // Faz a primeira atualização
    atualizar();
});