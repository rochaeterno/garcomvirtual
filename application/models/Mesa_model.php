<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mesa_model extends CI_Model  {
	function index(){
		
	}
	function Mesas(){
		$this->db->order_by("numMesa", "asc");
		$query = $this->db->get('tbmesa');
		$results=$query->result_array();
		return $results;
	}
	
	function Select_Mesa($idMesa){
		$where = array('idMesa' => $idMesa);
		$results=$this->db  ->select('tbmesa.numMesa')
									->where($where)
									->get("tbmesa")->row_array();
		$result2 =  $results["numMesa"];
		return $result2;
	}
	
	function Update($idMesa,$statusNovo){
		$dados = array('statusMesa'=>$statusNovo);
		$where = array('idMesa' => $idMesa);
		
		$this->db->where($where);
		$this->db->update('tbmesa', $dados);
		
		return true;
	}

	function Contar_mesas(){
		$where='tbmesa.statusMesa>0';
		$indice=$this->db->order_by("numMesa",'asc')->select('tbmesa.idMesa, tbmesa.numMesa')
		->get_where("tbmesa",$where)->result_array();
		// Retornando a contagem de mesas, juntamente com a sequencia de id delas em formato JSON
		$cont  = array("indice" => $indice);
		// Retornando a quantidade em formato JSON
		echo json_encode($cont);
	}
}
?>