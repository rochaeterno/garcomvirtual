    $('#gridCheck').click(function() {
    	//modifica o modal do fechamento para que o cliente decida sobre a gorjeta do garcom
      if ($(this).is(':checked')) {
        $('span.numero').html('<?php echo $total+$gorjeta; ?>');
        $('p.pc10').html('<small>Os 10% de gorjeta adicionados.</small>');
    } else {
        $('span.numero').html('<?php echo $total; ?>');
        $('p.pc10').html('<small>Os 10% de gorjeta foram removidos.</small>');
    }
});