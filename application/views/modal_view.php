<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- Mesa Modal HTML -->
<div id="addmesa" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open('mesa/Insere_mesa', 'id="cadastraMesa"'); //chama o form de de inserir mesas  ?> 
            <div class="modal-header">                      
                <h4 class="modal-title">Nova Mesa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" name="addmesa">&times;</button>
            </div>
            
            <div class="modal-body">                    
                <div class="form-group">
                    <?php if($_SESSION['maxmesas']>count($mesas)):?>
                        <label>Número da Mesa</label>
                        <select  class="form-control" name="numeroMesa" id="numeroMesa">

                            <?php 
                            $premiados=array('');
                            foreach ($mesas as $row) {
                                array_push($premiados,$row['numMesa']);
                            }
                            var_dump($premiados);?>

                            <?php for($i=0;$i<$_SESSION['maxmesas'];$i++):?>

                             <?php if (!in_array($i+1, $premiados)):?>
                                <option value="<?=$i+1?>"> Mesa <?=$i+1?></option>
                            <?php endif;?>


                        <?php endfor;?>
                    </select>
                    <?php else:?>
                        <p>Limite de mesas cadastradas atingido</p>
                    <?php endif;?>
                </div>
                <div class="form-group" style="display: none">
                    <label>Conectar ao tablet:</label>
                    <p class="text-warning"><small>Total de tablets cadastrados: <b>3</b>.</small></p>
                    <select class="form-control" id="tabletMesa" name="tabletMesa" required>
                        <option>Tablet #001</option>
                        <option>Tablet #002</option>
                        <option>Tablet #003</option>
                    </select>
                </div>                  
            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                <input type="submit" class="btn btn-success" value="Adicionar">
            </div>
        </form>
    </div>
</div>
</div>

<!-- Tablet Modal HTML -->
<div id="addtablet" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
         <?php echo form_open('tablets/Insere_tablet', 'id="cadastraTablet"'); //chama o form de de inserir tablets  ?> 
         <div class="modal-header">                      
            <h4 class="modal-title">Adicionar tablet</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">                    
            <div class="form-group">
                <label>Número do Tablet</label>
                <input type="text" class="form-control" name="mactablet">
            </div>
            <div class="form-group">
                <p>Tem certeza que deseja adicionar novo Tablet?</p>
                <p class="text-warning"><small>Total de tablets cadastrados: <b>3</b>.</small></p>
            </div>                  
        </div>
        
        <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-success" value="Adicionar">
        </div>
    </form>
</div>
</div>
</div>
<!-- Excluir Tablet Modal HTML -->
<div id="excluirtablet" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <div class="modal-header">                      
                    <h4 class="modal-title">Excluir Tablet</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">                    
                    <p>Tem certeza que deseja apagar esse registro?</p>
                    <p class="text-warning"><small>Essa ação não pode ser desfeita.</small></p>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                    <input type="submit" class="btn btn-danger" value="Excluir">
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</body>
</html>