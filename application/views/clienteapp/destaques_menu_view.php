<?php
if(isset ($_SESSION['id_mesa'])){
 $cod_mesa = $_SESSION['id_mesa'];
}
if(isset ($_SESSION['idConta'])){
 $cod_conta = $_SESSION['idConta'];
}	
?>
<div id="content"> 

  <div id="Destaques123" class="carousel slide" data-ride="carousel" data-interval="false">
    <!-- Indicadores mostram quantas páginas do Carrosel tem. A gente habilitar ou não, não faz diferença
    <ol class="carousel-indicators">
      <li data-target="#Destaques123" data-slide-to="0" class="active"></li>
      <li data-target="#Destaques123" data-slide-to="1"></li>
      <li data-target="#Destaques123" data-slide-to="2"></li>
    </ol>-->

    <!-- Slides -->

    <div class="carousel-inner" role="listbox">
      <!--Imagem 1-->
      <div class="item destaque active">
        <img src="<?php echo $destaques[0]['img']; ?>" alt="<?php echo $destaques[0]['nome']; ?>" style="width:100%;height: auto;">
        <div class="carousel-caption">
          <h3 id="nome"><?php echo $destaques[0]['nome']; ?></h3>
          <h3 id="descricao"><?php echo $destaques[0]['descricao']; ?></h3>
          <h3 id="preco" style="display: none;"><?php echo $destaques[0]['preco'] ?></h3>
          <!-- BOTÃO DE PEDIR -->
          <div class="eofim"  style="/*position: absolute;bottom: 8px;right: 10%;*/">
            <form id="form_qtt" name="form_qtt" class="quantidade">
              <div class="input-group input-group-prepend mb-3">
                <select class="custom-select form-control form-control-lg qty" id="number" name="number" style="    height: 34px;">
                  <?php if( $destaques[0]['porcao']!=0):?>
                    <option value="0.5">½</option>
                  <?php endif;?>
                  <option value="1" selected>1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

                <div class="input-group-append">
                  <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
                <p name="nome_produto" class="esconde"><?php echo $destaques[0]['nome']; ?></p>
                <h3 name="preco_produto" class="esconde"><?php echo $destaques[0]['preco'] ?></h3>
                  <a class="btn btn-outline-secondary add-to-cart" href="#" type="button" style="background-color: black">Pedir</a>
                </div>
              </div>
            </form>
          </div>
          <!--FIM DO PEDIDO-->
        </div>      
      </div>

      <!--Imagem 2-->
      <div class="item destaque">
        <img src="<?php echo $destaques[1]['img']; ?>" alt="<?php echo $destaques[1]['nome']; ?>" style="width:100%;height: auto; ">
        <div class="carousel-caption">
          <h3 id="nome"><?php echo $destaques[1]['nome']; ?></h3>
          <h3 id="descricao"><?php echo $destaques[1]['descricao']; ?></h3>
          <h3 id="preco" style="display: none;"><?php echo $destaques[1]['preco'] ?></h3>
          <!-- BOTÃO DE PEDIR -->
          <div class="eofim"  style="/*position: absolute;bottom: 8px;right: 10%;*/">
            <form id="form_qtt" name="form_qtt" class="quantidade">
              <div class="input-group input-group-prepend mb-3">
                <select class="custom-select form-control form-control-lg qty" id="number" name="number" style="    height: 34px;">
                  <?php if( $destaques[1]['porcao']!=0):?>
                    <option value="0.5">½</option>
                  <?php endif;?>
                  <option value="1" selected>1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

                <div class="input-group-append">
                  <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
                <p name="nome_produto" class="esconde"><?php echo $destaques[1]['nome']; ?></p>
                <h3 name="preco_produto" class="esconde"><?php echo $destaques[1]['preco'] ?></h3>
                  <a class="btn btn-outline-secondary add-to-cart" href="#" type="button" style="background-color: black">Pedir</a>
                </div>
              </div>
            </form>
          </div>
          <!--FIM DO PEDIDO-->
        </div>      
      </div>

      <!--Imagem 3-->
      <div class="item destaque">
        <img src="<?php echo $destaques[2]['img']; ?>" alt="<?php echo $destaques[2]['nome']; ?>" style="min-width:100%;min-height: auto;">
        <div class="carousel-caption">
          <h3 id="nome"><?php echo $destaques[2]['nome']; ?></h3>
          <h3 id="descricao"><?php echo $destaques[2]['descricao']; ?></h3>
          <h3 id="preco" style="display: none;"><?php echo $destaques[1]['preco'] ?></h3>
          <!-- BOTÃO DE PEDIR -->
          <div class="eofim"  style="/*position: absolute;bottom: 8px;right: 10%;*/">
            <form id="form_qtt" name="form_qtt" class="quantidade">
              <div class="input-group input-group-prepend mb-3">
                <select class="custom-select form-control form-control-lg qty" id="number" name="number" style="height: 34px;">
                  <?php if( $destaques[2]['porcao']!=0):?>
                    <option value="0.5">½</option>
                  <?php endif;?>
                  <option value="1" selected>1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

                <div class="input-group-append">
                  <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
                <p name="nome_produto" class="esconde"><?php echo $destaques[2]['nome']; ?></p>
                <h3 name="preco_produto" class="esconde"><?php echo $destaques[2]['preco'] ?></h3>
                  <a class="btn btn-outline-secondary add-to-cart" href="#" type="button" style="background-color: black">Pedir</a>
                </div>
              </div>
            </form>
          </div>
          <!--FIM DO PEDIDO-->
        </div>      
      </div>
    </div>

    <!-- Botão direita e esquerda -->
    <a class="left carousel-control" href="#Destaques123" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>

    <a class="right carousel-control" href="#Destaques123" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Próximo</span>
    </a>
  </div>
  <!--Divs de baixo-->
  <div class="container text-center">    
    <h3 class="branco">Confira também</h3><br>
    <div class="row ">
      <div class="destaque col-sm-4" style="position: relative;float:left;padding-top:5px;padding-left: 0px;">
        <img src="<?php echo $destaques[3]['img']; ?>" class="img-responsive autola" alt="<?php echo $destaques[3]['nome']; ?>">
        <div style="position: absolute;background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));bottom:0%;width: 96.4%;height: 50%;"></div>
        <div class="nomepreco" style="position: absolute;bottom: 8px;left: 23px;">
          <p id="nomepreco" class="centrado" style="color: white;"><?=$destaques[3]['nome']?></p>
        </div>
        <p id="nome" style="display: none;"><?php echo $destaques[3]['nome']; ?></p>
        <h3 id="preco" style="display: none;"><?php echo $destaques[3]['preco'] ?></h3>
        <!-- BOTÃO DE PEDIR -->
        <div class="eofim"  style="position: absolute;bottom: 8px;right: 10%;">
          <form id="form_qtt" name="form_qtt" class="quantidade">
            <div class="input-group input-group-prepend mb-3">
              <select class="custom-select form-control form-control-lg qty" id="number" name="number" style="height: 34px;">
                <?php if($$destaques[3]['porcao']!=0):?>
                  <option value="0.5">½</option>
                <?php endif;?>
                <option value="1" selected>1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
              </select>
              <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

              <div class="input-group-append">
                <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
                <p name="nome_produto" class="esconde"><?php echo $destaques[3]['nome']; ?></p>
                <h3 name="preco_produto" class="esconde"><?php echo $destaques[3]['preco'] ?></h3>
                <a class="btn btn-outline-secondary add-to-cart" href="#" type="button" style="background-color: black">Pedir</a>
              </div>
            </div>
          </form>
        </div>
        <!--FIM DO PEDIDO-->
      </div>

      <div class="destaque col-sm-4" style="position: relative;float:left;padding-top:5px;padding-left: 0px;">
        <img src="<?php echo $destaques[4]['img']; ?>" class="img-responsive autola" alt="<?php echo $destaques[4]['nome']; ?>">
        <div style="position: absolute;background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));bottom:0%;width: 96.4%;height: 50%;"></div>
        <div class="nomepreco" style="position: absolute;bottom: 8px;left: 23px;">
          <p id="nomepreco" class="centrado" style="color: white;"><?=$destaques[4]['nome']?></p>
        </div>
        <p id="nome" style="display: none;"><?php echo $destaques[4]['nome']; ?></p>
        <h3 id="preco" style="display: none;"><?php echo $destaques[4]['preco'] ?></h3>
        <!-- BOTÃO DE PEDIR -->
        <div class="eofim"  style="position: absolute;bottom: 8px;right: 10%;">
          <form id="form_qtt" name="form_qtt" class="quantidade">
            <div class="input-group input-group-prepend mb-3">
              <select class="custom-select form-control form-control-lg qty" id="number" name="number" style="height: 34px;">
                <?php if($destaques[4]['porcao']!=0):?>
                  <option value="0.5">½</option>
                <?php endif;?>
                <option value="1" selected>1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
              </select>
              <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

              <div class="input-group-append">
                <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
                <p name="nome_produto" class="esconde"><?php echo $destaques[4]['nome']; ?></p>
                <h3 name="preco_produto" class="esconde"><?php echo $destaques[4]['preco'] ?></h3>
                <a class="btn btn-outline-secondary add-to-cart" href="#" type="button" style="background-color: black">Pedir</a>
              </div>
            </div>
          </form>
        </div>
        <!--FIM DO PEDIDO-->
      </div>


      <div class="destaque col-sm-4" style="position: relative;float:left;padding-top:5px;padding-left: 0px;">
        <img src="<?php echo $destaques[5]['img']; ?>" class="img-responsive autola" alt="<?php echo $destaques[5]['nome']; ?>">
        <div style="position: absolute;background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));bottom:0%;width: 96.4%;height: 50%;"></div>
        <div class="nomepreco" style="position: absolute;bottom: 8px;left: 23px;">
          <p id="nomepreco" class="centrado" style="color: white;"><?=$destaques[5]['nome']?></p>
        </div>
        <p id="nome" style="display: none;"><?php echo $destaques[5]['nome']; ?></p>
        <h3 id="preco" style="display: none;"><?php echo $destaques[5]['preco'] ?></h3>
        <!-- BOTÃO DE PEDIR -->
        <div class="eofim"  style="position: absolute;bottom: 8px;right: 10%;">
          <form id="form_qtt" name="form_qtt" class="quantidade">
            <div class="input-group input-group-prepend mb-3">
              <select class="custom-select form-control form-control-lg qty" id="number" name="number" style="height: 34px;">
                <?php if($destaques[5]!=0):?>
                  <option value="0.5">½</option>
                <?php endif;?>
                <option value="1" selected>1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
              </select>
              <!--<input type="number" name="number" id="number" class="form-control qty" value="1"/>-->

              <div class="input-group-append">
                <!--AQUI É ONDE PEGA O NOME E PREÇO, AGORA-->
                <p name="nome_produto" class="esconde"><?php echo $destaques[5]['nome']; ?></p>
                <h3 name="preco_produto" class="esconde"><?php echo $destaques[5]['preco'] ?></h3>
                <a class="btn btn-outline-secondary add-to-cart" href="#" type="button" style="background-color: black">Pedir</a>
              </div>
            </div>
          </form>
        </div>
        <!--FIM DO PEDIDO-->
      </div>
    </div>
  </div>

</div>
<!--Modal de Confirmar Pedido -->
<div id="confirmar" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="form1">
        <div class="modal-header">                      
          <h4 class="modal-title">Confirmar Pedido</h4>
          <!--<button type="button" class="close atualizar" data-dismiss="modal" aria-hidden="true">&times;</button>-->
          <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar" style="min-width: 0;float:right;margin-right: 10px;">
        </div>

        <div class="modal-body">
          <table id="conf_pedido" class="table">
            <thead>
              <tr>
                <th scope="col">Qtd.</th>
                <th scope="col">Produto</th>
                <th scope="col">Preço<span style="font-size:8px;">(Total)</span></th>
            </thead>
            <tbody>
              <!--Essa é uma tabela hipotetica, todos os seus dados estão em adicionar_pedido.js-->
            </tbody>
          </table>
        </div>

        <div class="modal-footer">
         <input type="hidden" id="id_mesa" value="<?php echo $cod_mesa;?>">
         <input id="confirm" type="submit" class="btn btn-laranja confirmapedido atualizar" style="min-height: 60px;" value="Adicionar">
       </div>
     </form>
   </div>
 </div>
</div>