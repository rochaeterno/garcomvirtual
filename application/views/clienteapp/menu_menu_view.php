<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<?php
$cod_mesa =  $this->session->id_mesa;
$cod_conta =  $this->session->idConta;
$num_mesa = $this->session->num_mesa;
if(!isset ($cod_mesa,$cod_conta)){
	$redirect = site_url("cliente_apt/seted_mesa");
	header("location:$redirect");	
}

if(isset ($_SESSION['status_conta'])){
	$status_teste = $this->session->status_conta;
	if($status_teste==2){
		$redirect = site_url('cliente_apt/loading_encerramento');
		header("location:$redirect");
	}
	elseif($status_teste==0){
		if(isset($cod_mesa,$cod_conta)){
			$newdata = array('status_conta' => 1);
			$this->session->set_userdata($newdata);	
		}else{
			$redirect = site_url("cliente_apt/seted_mesa");
			header("location:$redirect");
		}	
	}
}
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Menu</title>
    <!--JavaScripts-->
    <script src="<?php echo base_url();?>js/jquery.js"></script>
    <script src="<?php echo base_url();?>js/gorjeta.js"></script>
    <script src="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">url_confirma='<?php echo site_url("pedido/confirma_pedido"); ?>';</script>
    <script type="text/javascript">id_conta_modals='<?php echo $cod_conta; ?>';</script>
    <!--Ícones e Bootstrap-->
    <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/css/bootstrap.min.css">
    <!-- CSS costumizado -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cliente/cliente_style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cliente/extrabts.css">
    <style type="text/css">
    /*Não está funcionando fora da página, ainda a descobrir o porque*/
    .btn-outline-secondary {
        width: 60px;color: #ffffff;
        background-color: transparent;
        background-image: none;
        border-color: #6c757d;
    }
    #contamenu a:hover{
        color: #FA7500;
        background-color: white;
    }

 .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #FA7500;
}
li>a:hover{
   color: #FA7500;
}
i #garcon{
    font-size: 3em;
}
img{
    max-width: 100%;
    width: 395px;
    height: 200px;
    object-fit: cover;
}
.carousel-inner>.item>a>img, .carousel-inner>.item>img{
    max-width: 100%;
    max-height: 300px;
    object-fit: cover;
}
.modal-content .modal-body{
  height:250px;
  overflow:auto;

}
.pisca {
  animation: piscapisca 1s linear;
  background-color: #C55D03;
}

@keyframes piscapisca {
    50%{
       opacity: .4;

   }
}
.slideDown{
    animation-name: slideDown;
    -webkit-animation-name: slideDown;  

    animation-duration: 1s; 
    -webkit-animation-duration: 1s;

    animation-timing-function: ease;    
    -webkit-animation-timing-function: ease;    

    visibility: visible !important;                     
}

@keyframes slideDown {
    0% {
        transform: translateY(-100%);
    }         
    100% {
        transform: translateY(0%);
    }       
}

@-webkit-keyframes slideDown {
    0% {
        -webkit-transform: translateY(-100%);
    }      
    100% {
        -webkit-transform: translateY(0%);
    }   
}
.slideUp{
    animation-name: slideUp;
    -webkit-animation-name: slideUp;  

    animation-duration: 1s; 
    -webkit-animation-duration: 1s;

    animation-timing-function: ease;    
    -webkit-animation-timing-function: ease;    

    visibility: visible !important;                     
}

@keyframes slideUp {
    0% {
        transform: translateY(0%);
    }        
    100% {
        transform: translateY(-100%);
    }       
}

@-webkit-keyframes slideUp {
    0% {
        -webkit-transform: translateY(0%);
    }      
    100% {
        -webkit-transform: translateY(-100%);
    }   
}

li:active, li:highlight:hover, li:link, li:visited, li:focus{
    color: #FA7500;
}
}

.dropup {margin-top:120px;}

.terco, .quarto{
    padding: 10px 15px;
    text-decoration: none;
    transition: all 0.3s;
    justify-content:center;align-items:center; text-align: center;
}
.terco{
    width: 30%;
    max-width: 33%;
}
.quarto{width: 24.5%;max-width: 25%}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/cliente/detalhesfinais.css">
<!--JS costumizado-->
<script type="text/javascript">
    /*Não está funcionando fora da página, ainda a descobrir o porque*/
    var id_mesa = '<?php echo $cod_mesa;?>';
    var base_garcom='<?php echo base_url(); ?>' + 'index.php/pedido/chamar_garcom';
    var url_refresh_modals = '<?php echo base_url(); ?>' + 'index.php/cliente_apt/get_refresh_modals';
    var metades=0;
    $(document).ready(function(){
        $('.metade').click(function(){
            if($('.sera').is(':selected')){
                ++metades;
                if(metades%2==0)
                {
                    $('.cart').show();
                    $('.avisometade').hide();

                }
                else
                {
                    $('.cart').hide();
                    $('.avisometade').show();
                }
            }
        });
    });  
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/carrinho.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/maisoumenos.js"></script>
<script src="<?php echo base_url();?>js/confirmar_pedido.js"></script>
<script src="<?php echo base_url();?>js/adicionar_pedido.js"></script>
<script src="<?php echo base_url();?>js/remover_pedido.js"></script>
<script src="<?php echo base_url();?>js/avaliacao.js"></script>
<script src="<?php echo base_url();?>js/chamar_garcom.js"></script>
<script src="<?php echo base_url();?>js/refresh_modals.js"></script>
<script src="<?php echo base_url();?>js/todos.js"></script>
</head>
<body onLoad="history.go(+1)">

    <!--ATUALIZAR PATHS DO MENU CLIENTES-->
    <div class="sticky" style="background-color:#212121;">
        <ul class="nav nav-pills row fat">
            <!--<li ><a id="todos" href="#">Todos</a></li>-->
            <li class="pratos col-md-3 quarto active">
              <a data-toggle="tab" href="#pratos">
                <i class="fa fa-cutlery"></i> 
                <span class="hidden-xs">Pratos Principais</span>
            </a>
        </li>
        <li class="acompanhamentos col-md-3 quarto">
          <a data-toggle="tab" href="#acompanhamentos">
            <i class="fa fa-lemon-o"></i> 
            <span class="hidden-xs">Acompanhamentos</span>
        </a>
    </li>
    <li class="sobremesas col-md-3 quarto">
      <a data-toggle="tab" href="#sobremesas">
        <i class="fa fa-birthday-cake"></i> 
        <span class="hidden-xs">Sobremesas</span>
    </a>
</li>
<li class="bebidas col-md-3 quarto">
  <a data-toggle="tab" href="#bebidas">
    <i class="fa fa-beer"></i> 
    <span class="hidden-xs">Bebidas</span>
</a>
</li>
<li class="avisometade" style="display:none;background-color:orange;"><a>Complete a outra metade do seu pedido para prosseguir</a></li>
</ul>
</div>  
<div class="container">
    <div id="alertg" class="alert alert-info" role="alert" style="left:30%;position:absolute;z-index:3000;display: none">
      <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>O garçom foi chamado!</strong> Aguarde alguns instantes! 
  </div>
