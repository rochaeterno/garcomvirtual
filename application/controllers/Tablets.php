<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tablets extends CI_Controller{
	public function index(){
		$dados['titulo']="tablets";
		$this->load->model('crud');
		$result=$this->crud->Select('tbtablet');
		$tablets['tablets']=$result;
		$this->load->view('header_adm_view',$dados);//nome fantasia	
		$this->load->view('tablet_view',$tablets);
		$this->load->view('modal_view');
	}
	
	public function Insere_tablet(){
		$form = $_POST['mactablet'];
		
		$table = 'tbtablet';
		$dados = array('macTablet' => $form);	
		
		$this->load->model('crud');
		$confirm = $this->crud->Insert($table, $dados);
		
		if($confirm){
		redirect('Mesa');
		}
	}
}