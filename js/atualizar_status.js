//javascript responsavel pelo preenchimento do checkbox de entrega do pedido
/*Classe responssavel por capturar os dados que vem da caixa de checagem presentes no pedido do balcao assim como pelo regitro do atendimento da solicitação do balcão, recebe os dados de balcão view e os manda para o controller balcão por ajax para que que sejatratado e inserido no DOM em tempo real*/
$(document).ready(function(){
	$(document).on('click','.atualizar_status',function(){
		var id=$(this).attr("id_update");
		var table=$(this).attr("tabela");
		var status=$(this).attr("status_pedido");
		
		$.ajax({
			//status_update=base_url + 'index.php/balcao/status_update';
    		url: status_update,
    		type: "post",
    		data: {tabela: table, id_update: id},
   		 	//dataType: 'json',
    		success: function(dados) {
    			//jquery responsavel pela modificao do DOM ainda nao esta funcionando
    			if(dados){
    				if(table=="tbalerts"){
  	      				$("a[id_update|='"+id+"'").parent().fadeOut(400, function() {
   							$(this).parent().remove();  	    
   						});
					}else if(table=="produtospedidos"){  
						if(status=="0"){
							$("span[id_update|='"+id+"'").removeClass( "fa fa-square").addClass( "fa fa-check-square");
							$("span[id_update|='"+id+"'").attr("status_pedido","1");
						}else if(status=="1"){
							$("span[id_update|='"+id+"'").removeClass( "fa fa-check-square").addClass( "fa fa-square");
							$("span[id_update|='"+id+"'").attr("status_pedido","0");
						}	
					}
    			}else{
    				alert("A conexão com o servidor foi mal sucedida, tente novamente mais tarde");
    			}
    		}
  		}); 
  	}); 
});