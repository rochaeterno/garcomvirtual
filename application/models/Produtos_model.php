<?php
class produtos_model extends CI_Model  {
	function index(){
		
	}
	function Destaques($table){
		
		$where = "nivelDestaque > 0";
		$this->db->where($where);
		$this->db->order_by("nivelDestaque", "asc");
		$query = $this->db->get($table);
		$results=$query->result_array();
		return $results;
	}
	function Todos($table,$where){
		if($where!='0'){
			$w='tipo='.$where;
			$this->db->where($w);
		}
		$this->db->order_by('nome','ASC');
		$query=$this->db->get($table);
		$results=$query->result_array();
		return $results;
	}
	
	function Set_img($table,$img,$id){
		$this->db->set('img', $img);
		$this->db->where('idProduto', $id);
		$this->db->update($table);
		
		return true; 
	}
	function UnlinkaImg($id){
		$this->db->where('idProduto', $id);
		$this->db->select('img');
		$query= $this->db->get('tbprodutos');
		return $query->result_array();
	}
	function Update_Status($id,$status){
		$this->db->where('idProduto',$id);
		$this->db->set('disponibilidade',$status);
		$this->db->update('tbprodutos');
	}
	
}
?>