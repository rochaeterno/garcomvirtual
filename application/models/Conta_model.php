<?php
class Conta_model extends CI_Model  {
	function index(){
		
	}
	function Identificar_Mesa($id_conta){
		$where='idConta='.$id_conta.' AND status_conta<3';
		$query = $id_mesa=$this->db->select('tbconta.idMesa')
			->where($where)
			->get("tbconta"); 	
			
			foreach ($query->result() as $row)
			{
				return $row->idMesa;
			}		
	}
	
	
	function Insert($table, $dados){
		$query = $this->db->get_where($table, $dados);
		$num_rows = $query->num_rows();//conta o numero de linhas do select
		if($num_rows>0){
			return false;
		}
		else{
			$this->db->insert($table, $dados);
			$last_id = $this->db->insert_id();

			$data = array(
				'idConta' => $last_id,
				'tipoAlert' => '0',
				'status' => '0'
			);
			$this->db->insert('tbalerts',$data);  

			return $last_id;
		}				
	}
	
	function Verificar_Status($table, $dados){
		$this->db->select('idMesa');
		$this->db->from($table);
		$this->db->where($dados);
		$result = $this->db->count_all_results();
		if($result>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function Trocar_Mesa($id_conta,$id_mesa_nova){
		$dados = array('idMesa'=>$id_mesa_nova);
		$where = array('idConta' => $id_conta);
		
		$this->db->where($where);
		$this->db->update('tbconta', $dados);
		
		$retorno = $this->Identificar_Mesa($id_conta);
			
		return $retorno;
	}
	
	function Update_Abertura($table, $dados){
		$where = array('idMesa' => $dados['idMesa'],'status_conta' => '0');

		$this->db->where($where);
		$this->db->update($table, $dados);

		$query = $this->db->get_where($table, $dados);
			$num_rows = $query->num_rows();//conta o numero de linhas do select

			if($num_rows==1){
				return TRUE;
			}
			else{
				return FALSE;
			}				
		}

		function Fecha_Balcao($table, $dados,$id){
			$where='idConta='.$id['id'];
			$check=$this->db->update($table, $dados,$where);

			if($check){
				$query = $this->db->get_where($table,$where);
				foreach ($query->result() as $row)
				{
					return $row->idMesa;
				}
			}
			else{
				//return FALSE;
				echo $table.' ';
				foreach ($dados as $p1 => $p2) {echo ' '.$p1."=".$p2.' ';}
				echo $where;
			}				
		}

		function Fechamento($table, $dados, $conta){
			$where = array('idConta' => $conta , 'status_conta' => '1');
			
			$this->db->where($where);
			$this->db->update($table, $dados);
			
			$query = $this->db->get_where($table,$dados);
			$num_rows = $query->num_rows();//conta o numero de linhas do select
			
			if($num_rows>0){
				$data = array(
					'idConta' => $conta,
					'tipoAlert' => '3',
					'status' => '0'
				);
				$this->db->insert('tbalerts',$data);  
				
				return true;
			}
			else{
				die("fail");
			}

		}

		function Historico($idconta){
	   //$where = "tbconta.status_conta='1' AND produtospedidos.status='1'";
			$where = "tbconta.idConta='".$idconta."'AND tbconta.status_conta='1'";
			return $results2=$this->db  ->select("tbprodutos.*, produtospedidos.*,tbconta.*")
			->join("tbprodutos", "produtospedidos.id_produtos=tbprodutos.idProduto")
			->join("tbconta", "produtospedidos.idConta=tbconta.idConta")
			->where($where)
			->order_by("produtospedidos.hora_pedido")
			->get("produtospedidos")->result_array();
		}

		function HistoricoPOS($idconta){
	   //$where = "tbconta.status_conta='1' ";
			$where = "tbconta.idConta='".$idconta."'AND produtospedidos.status='1'";

			return $retorno=$this->db  ->select("tbprodutos.*, produtospedidos.*,tbconta.*")
			->join("tbprodutos", "produtospedidos.id_produtos=tbprodutos.idProduto")
			->join("tbconta", "produtospedidos.idConta=tbconta.idConta")
			->where($where)
			->order_by("produtospedidos.hora_pedido")
			->get("produtospedidos")->result_array();
		}


		function Tela_fecha($idconta){
			//futuramente seria interessante unir esse metodo ao de mesmo nome presente no balcao_model pois são praticamente o mesmo
			$where = "tbconta.idConta='".$idconta."'AND tbconta.status_conta='1' AND produtospedidos.status='1'";
			return $results3=$this->db  ->select("tbprodutos.*, produtospedidos.*,tbconta.*")
			->join("tbprodutos", "produtospedidos.id_produtos=tbprodutos.idProduto")
			->join("tbconta", "produtospedidos.idConta=tbconta.idConta")
			->where($where)
			->order_by("produtospedidos.hora_pedido")
			->get("produtospedidos")->result_array();
		}

		function Last_status(){
			$where="tbconta.status_conta!=3";
			return $ultimo=$this->db->select('tbconta.*')
			->join("tbmesa", "tbmesa.idMesa=tbconta.idMesa")
			->where($where)
			->get("tbconta")->result_array(); 
		}

		function Last_alert(){
			return $ultimoalerta=$this->db->get('lastalert')->result_array();

			/*$this->db->select_max('idAlert');
			$this->db->group_by('idConta', 'desc');
			$result = $this->db->get('tbalerts');

			$ultimoalerta=$this->db->select("
			tbmesa.idMesa,
			tbconta.status_conta,
			tbalerts.idConta,
			tbalerts.status,
			tbalerts.tipoAlert,
			tbalerts.idAlert")
			->join("tbconta", "tbconta.idConta=tbalerts.idConta")
			->join("tbmesa", "tbconta.idMesa=tbmesa.idMesa")
			->where("tbconta.status_conta=1")
			->where("tbalerts.idAlert",$result)
			->order_by("produtospedidos.hora_pedido")
			->get("tbalerts")->result_array();*/
			return $ultimoalerta;
		}

		function Count_alerts(){
			return $this->db->count_all('tbalerts');
		}

		function Contar_contas(){
			$where='tbconta.status_conta<3';
			$indice=$this->db->order_by("idMesa",'asc')->select('tbconta.idMesa, tbconta.idConta, tbconta.status_conta')
			->get_where('tbconta', $where)->result_array();
			// Retornando a contagem de mesas, juntamente com a sequencia de id delas em formato JSON
			$cont  = array("indice" => $indice);
			// Retornando a quantidade em formato JSON
			echo json_encode($cont);
		}
		
		function Procurar_mesa_aberta($idMesa){
			//$dados = array('idMesa' => $form['idMesa'],'status_conta'=> $form['status_conta'], 'horaAbertura' => $form['horaAbertura']);

			$where='tbconta.status_conta<=1 and tbconta.idMesa = '.$idMesa;
			$this->db->get_where('tbconta', $where);
			$result = $this->db->count_all_results();
			if($result>0){
				return false;
			}else{
				return true;
			}
		}
	}
	?>