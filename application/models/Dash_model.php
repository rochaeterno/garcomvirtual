<?php
	class Dash_model extends CI_Model  {
		function Mostrar_vendas_periodo($diaInicial,$diaFinal){
			$where = "ganhos_mesa.data BETWEEN '$diaInicial' AND '$diaFinal'";
			return $results2=$this->db  ->select("ganhos_mesa.mesa_key as mesa_key, COUNT(distinct  ganhos_mesa.id_conta) as quant_contas,SUM(ganhos_mesa.quant_prod) as quant_prod,SUM(ganhos_mesa.soma_total) as soma_total")
			->where($where)
			->group_by("mesa_key")
			->get("ganhos_mesa")->result_array();
		}
		
		function gjt_garcom($diaInicial,$diaFinal){
			$where = "gjt_rel.data BETWEEN '$diaInicial' AND '$diaFinal'";
			return $results2=$this->db  
			->select("gjt_rel.nome AS nome,count(gjt_rel.quant_gjt) as quant_contas,SUM(gjt_rel.quant_gjt) as quant_gjt,SUM(gjt_rel.soma_total) as soma_total")
			->where($where)
			->group_by("nome")
			->get("gjt_rel")->result_array();
		}
		
		function ranking_pratos($diaInicial,$diaFinal){
			$where = "hora_pedido BETWEEN '$diaInicial' AND '$diaFinal'";
			return $results2=$this->db->select("ranking_pratos.id, ranking_pratos.nome, ranking_pratos.preco, SUM(ranking_pratos.quantidade_vendida) as quantidade_vendida")->where($where)->group_by("nome")->get("ranking_pratos")->result_array();
		}
	}
?>