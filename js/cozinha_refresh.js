var contador = 0;
function atualizar()
{
	$("#table_cozinha tbody").html("");
	$.ajax({
		url:url_refresh_cozinha,
		type:"POST",
		data: {id_conta: id_conta_modals},
			
		success: function(dados){
			arr = $.parseJSON(dados);
			call_history();
		}
	});
}
// Definindo intervalo que a função será chamada
setInterval("atualizar()", 7000);

// Quando carregar a página
$(function() {
	// Faz a primeira atualização
	atualizar();
});