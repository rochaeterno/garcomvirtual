<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balcao extends CI_Controller{
	public function index(){
		$this->load->model('balcao_model');
		$this->load->model('conta_model');
		$this->load->model('alerts_ajax');
		$ultimo2=$this->conta_model->Last_alert();
		$ultimo=$this->conta_model->Last_status();
		$amigao=$this->balcao_model->Garcon();
		$results=$this->balcao_model->Mesas();
		$results7=$this->balcao_model->Cont_alert_mesa();
		$results4=$this->balcao_model->Open_close();
		$results5=$this->conta_model->Count_alerts();
		$results6=$this->alerts_ajax->GarçomSolicitado();
		$dados['amigao']=$amigao;
		$dados['status']=$ultimo;
		$dados['mesas']=$results;
		$dados['solicitacoes']=$results4;
		$dados['alertas']=$results5;
		$dados['garcom_sol']=$results6;
		$dados['alertas_fil']=$results7;
		$dados['ultimo3']=$ultimo2;
		$this->load->view('balcao/header_balcao_view',$dados);
		$this->load->view('balcao/balcao_view',$dados);
	}

	public function Pull_history(){
		$idMesa = $_POST['idMesa'];
		$this->load->model('balcao_model');
		$this->balcao_model->Historico($idMesa);
	}
	
	public function Refresh_alerts(){
		$id_conta=$_POST['id_conta'];
		$this->load->model('alerts_ajax');
		$this->alerts_ajax->Return_ajax($id_conta);
	}

	 public function Reset_alerts(){
		$id_conta=$_POST['id_conta'];
		$this->load->model('alerts_ajax');
		$this->alerts_ajax->Reset_alerts($id_conta);
	}
	
	public function Status_update(){
		//aqui com os dados trazidos de atualizar status o ajax se comunica com o model de balcão para capturar os dados
		$table=$_POST['tabela'];
		$id=$_POST['id_update'];
		$this->load->model('balcao_model');
		$confirm=$this->balcao_model->Status_update($table,$id);	
		if($confirm){
			echo $id;
		}		
		else{
			echo false;
		}
	}
	
	public function get_refresh_modals(){
		$id=$_POST['id_conta'];
		$this->load->model('balcao_model');
			//Pega os dados de Fechamento
		$results=$this->balcao_model->Tela_fecha($id);
		$data['fechar']=$results;
			// Retornando o data em formato JSON
		echo json_encode($data);
	}
}
?>