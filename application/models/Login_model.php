<?php
class Login_model extends CI_Model  {
	
	function Validar_login($login,$senha){
		$table = 'tbusuario';
    	// a array dados recebe o login e senha digitados na view Login
		$dados = array('login' => $login, 'senha' => $senha);
		
		$query = $this->db->get_where($table, $dados);
		$num_rows = $query->num_rows();//conta o numero de linhas do select
		if($num_rows == 1){//se certifica que a apenas um usuario com esse login
			return $query->row_object();
		}
		else{
			return false;
		}
	}
}
?>