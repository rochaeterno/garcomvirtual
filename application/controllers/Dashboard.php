<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{
	public function index(){
		$dataInicial = date('Y-m-d');
		$dataFinal = date('Y-m-d', strtotime('+1 day'));
		//$dataInicial = "2018/01/01";
		//$dataFinal = "2018/12/01";
		$this->load->model('dash_model');
		$results=$this->dash_model->Mostrar_vendas_periodo($dataInicial,$dataFinal);
		$results2=$this->dash_model->gjt_garcom($dataInicial,$dataFinal);
		$results3=$this->dash_model->ranking_pratos($dataInicial,$dataFinal);
		
		$dados['titulo']="Relatórios";
		$dados['vendas_periodo']=$results;
		$dados['gjt_garcom']=$results2;
		$dados['ranking_pratos']=$results3;
		
		$this->load->view('header_adm_view',$dados);
		$this->load->view('dash_view',$dados);
		//$this->load->view('modal_view');
	}
	
	public function preencher_mesa_por_periodo(){
		$de = $this->input->post('de');
		$para = $this->input->post('para');
		
		$dataInicial = date('Y-m-d', strtotime($de));
		$dataFinal = date('Y-m-d', strtotime($para));
		
		if($dataInicial!=$dataFinal){
			$this->load->model('dash_model');
			$results=$this->dash_model->Mostrar_vendas_periodo($dataInicial,$dataFinal);
			echo json_encode($results);
		}
		else
		{
			$dataFinal = date('Y-m-d', strtotime($dataInicial . '+1 day'));	
			$this->load->model('dash_model');
			$this->load->model('dash_model');
			$results=$this->dash_model->Mostrar_vendas_periodo($dataInicial,$dataFinal);
			echo json_encode($results);
		}
	}
	
		public function gjt_garcom(){
		$de = $this->input->post('de');
		$para = $this->input->post('para');
		
		$dataInicial = date('Y-m-d', strtotime($de));
		$dataFinal = date('Y-m-d', strtotime($para));
		
		if($dataInicial!=$dataFinal){
			$this->load->model('dash_model');
			$results=$this->dash_model->gjt_garcom($dataInicial,$dataFinal);
			echo json_encode($results);
		}
		else
		{
			$dataFinal = date('Y-m-d', strtotime($dataInicial . '+1 day'));	
			$this->load->model('dash_model');
			$results=$this->dash_model->gjt_garcom($dataInicial,$dataFinal);
			echo json_encode($results);
		}
	}
	
	public function ranking_pratos(){
		$de = $this->input->post('de');
		$para = $this->input->post('para');
		
		$dataInicial = date('Y-m-d', strtotime($de));
		$dataFinal = date('Y-m-d', strtotime($para));
		
		if($dataInicial!=$dataFinal){
			$this->load->model('dash_model');
			$results=$this->dash_model->ranking_pratos($dataInicial,$dataFinal);
			echo json_encode($results);
		}
		else
		{
			$dataFinal = date('Y-m-d', strtotime($dataInicial . '+1 day'));		
			$this->load->model('dash_model');
			$results=$this->dash_model->ranking_pratos($dataInicial,$dataFinal);
			echo json_encode($results);
		}
	}
}