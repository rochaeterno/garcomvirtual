<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedido extends CI_Controller{
	public function Confirma_Pedido(){
		//echo "até aqui";
		header('Content-Type: text/html; charset=utf-8');// para formatar corretamente os acentos
		if(isset($_POST['pedidos'])){
			$dados_pedido =json_decode($_POST['pedidos'], true);
		}
		
		//echo count($dados_pedido);
		//pega os dados do modal de confirmação de pedidos por meio de uma array, inserindo seuresultado no banco linha por linha ultilizando uma estrutura de loop
		for($i=0; $i<count($dados_pedido); $i++){
			$form['nome'] = $dados_pedido[$i]['nome_prod'];
			$form['quant'] = $dados_pedido[$i]['quant'];
			$form['obs'] = $dados_pedido[$i]['obs'];
			$table = 'tbprodutos';
			$dados = array('nome' => $form['nome']);
			//$testados ="".$form['obs']." ".$form['nome']." ";
			$this->load->model('crud');
			//aqui o produto e buscado por nome que como definido anteriormente deve ser unico
			$confirm = $this->crud->Select_obj($table, $dados);
			if($confirm != null){
				//echo "produto pego";
				$cod_conta = $_SESSION['idConta'];
				//aqui o id do produto e pego a partir do seu nome
				$pedido['idProd'] = intval($confirm->idProduto);
				
				$table = 'tbconta';
				$dados = array('idConta' => $cod_conta, 'status_conta' => '3');
				//verifica o status da conta pelo id
				$this->load->model('crud');			
				$confirmation = null;
				$confirmation = $this->crud->Select_obj($table, $dados);
				if($confirmation!=null){
					//echo "confirmation";
					$status = intval($confirmation->status_conta);
				}else{
					//echo "continua null";
					$status = null;
				}
				if($status >= 2){
					//echo "Conta Fechada ";
					unset($_SESSION['idConta'],$_SESSION['lastIdConta']);
					$redirect = site_url("cliente_apt/seted_mesa");
					header("location:$redirect");	
				}
				else{
					//echo "Testa Conta ";
					$table = 'tbconta';
					$dados = array('idConta' => $cod_conta,'status_conta' == '1');
				
					$this->load->model('crud');			
					$confirm = null;
					$confirm = $this->crud->Select_obj($table, $dados);
					$status = intval($confirm->status_conta);
					if($status == 1){
						//echo ", Conta Passou, ";
						$pedido['idConta'] = intval($confirm->idConta);
						$pedido['quant'] = floatval($form['quant']);
						$pedido['obs'] = $form['obs'];
					
						$localGTM = '';
						$pedido['horaAbertura'] = date('Y-m-d H:i:s');
						$pedido['status'] = "0";
					
						$table = 'produtospedidos';
						$dados = array('idConta' => $pedido['idConta'],'id_produtos' => $pedido['idProd'],'quantidade' => $pedido['quant']
						,'hora_pedido' => $pedido['horaAbertura'],'status' => $pedido['status'], 'obs' => $pedido['obs']);
					
						$this->load->model('crud');	
						$confirm = null;
						//insere os dados previamente tratados da linha do modal de confirmação de pedidos
						$confirm = $this->crud->Insert($table, $dados);
						if($confirm){
							// caso o pedido seja inserido o sistema irá gerar o alert responsavel por avisar o balcão de um novo pedido
							$table = 'tbalerts';
							$dados = array(
							'idConta' => $pedido['idConta'],
							'tipoAlert' => '1',
							'status' => '0'
							);
							$this->load->model('crud');	
							$confirm = null;
							$confirm = $this->crud->Insert($table, $dados);
							if(!isset($confirm)) {
								echo "Erro na geração do alerta";
							}
						}
						else{
							echo "Erro na inserção";
						}
					}
				}
			}
			else{
				echo "Erro no recebimento do produto";
			}
		}
		echo "Pedido Recebido, Por Favor Aguarde";
	}
	
	public function Chamar_garcom(){
		$table = 'tbalerts';
		$dados = array(
			'idConta' => $_SESSION['idConta'],
			'tipoAlert' => '2',
			'status' => '0'
		);
		$this->load->model('crud');
		$confirm=$this->crud->Insert_return_id($table,$dados);
		$cont  = array('id' => $confirm);
    		// Retornando o id em formato JSON
		echo json_encode($cont);
	}
}
?>