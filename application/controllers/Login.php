<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{
	public function index(){
		//carrega a tela inicial de login
		//put some vars back into $_GET.
		parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
		$data['mensagem']='99';
		if(isset($_GET['msg'])){
			$a=$_GET['msg'];

			switch($a)
			{
				case 1: $data['mensagem'] = 'Usuário ou senha inválidos. Verifique os dados e tente novamente.';break;
				case 2: $data['mensagem'] = 'Tipo de usuário inválido. Por favor, contrate um administador.';break;
				default:$data['mensagem'] = 'Ocorreu um erro desconhecido.';break;
			}
		}
		$this->load->view('login_view',$data);
	}
	
	public function logout(){
		$this->session->sess_destroy();
				$data['mensagem']='99';
		$this->load->view('login_view',$data);
	}

	public function validacao(){
		//validaçao de formularios se necessario adicionar clausulas
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('loginUsu','', 'required');
		$this->form_validation->set_rules('senhaUsu','', 'required');
		//fim da validação
		
		$login =  $_POST['loginUsu'];
		$senha =  $_POST['senhaUsu'];

		
		if($this->form_validation->run() == TRUE){
			//chama o model para testar se o login e senha estao corretos
			$this->load->model('login_model');

			$dados = $this->login_model->Validar_login($login,$senha);
			if($dados==FALSE){
				redirect('Login?msg=1'); //invalido
			}
			else{
				$id = $dados->idUsuario;
				$nome =$dados->nome;
				$userType = $dados->tipoUsuario;
				$user = $dados->login;
				$pass = $dados->senha;
				
				//monta a session			
				$newdata = array('id' => $id,'user' => $user,'pass' => $pass,'nome'=>$nome,'maxmesas'=>'10');
				$this->session->set_userdata($newdata);

				//monta a view de acordo com o tipo de usuario retornado pelo model
				if($userType == "Balcão"){	
					redirect('balcao/index/');
				}
				elseif($userType == "Administrador"){
					//Array de Dados da view
					redirect('Admin');
				}
				else{
					redirect('Login?msg=2'); // n permitido
				}
			}
		}
	}
}
?>