<?php
class model_avaliacao extends CI_Model  {
	function Avaliar_rest($nota,$idConta){
		$table = 'tbavaliacao';	
		$data['idConta'] = $idConta;
		$data['nota'] = $nota;
		$data['tipo_avaliacao'] = "Restaurante";//Seta o tipo da avaliação
		
		$this->load->model('crud');
		$id=$this->crud->Insert_return_id($table,$data);
		return $id;	
	}
	
	function Avaliar_garcom($nota,$idConta){
		$table = 'tbavaliacao';	
		$data['idConta'] = $idConta;
		$data['nota'] = $nota;
		$data['tipo_avaliacao'] = "Garcom";//Seta o tipo da avaliação
		
		$this->load->model('crud');
		$id=$this->crud->Insert_return_id($table,$data);
		return $id;	
	}
}
?>