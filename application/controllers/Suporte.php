<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suporte extends CI_Controller{
	public function index(){
		$dados['titulo']="Contatos";
		$this->load->view('header_adm_view',$dados);
		$this->load->view('sup_view');
		$this->load->view('modal_view');
	}
}