<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="page-wrapper" style="overflow: auto">
	<div class="container-fluid">
		<!--Cabeçalho-->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Relatórios</h1>
		</div>
		<!--CONTEUDO-->
		<!--LINHA-->
		<div class="row">
			<div class="col-lg-6">
				<h2>Gorjetas por Garçom</h2>
				<form class="form_gjt_garcom">
					De<input type="date" name="de">
					até <input type="date" name="para">
					<button type="button" class="btn btn-default gjt_garcom">
						<span class="fa fa-search"></span> Pesquisar
					</button>
					<br><br>
				</form>
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped table_gjt_garcom">
					<thead>
						<tr>
							<th>Garçom</th>
							<th>Mesas atendidas</th>
							<th>Gorjetas recebidas</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<?php if(!$gjt_garcom):?>
							<tr>
								<td colspan="4">Não há registros a serem mostrados, faça uma nova pesquisa.</td>
							</tr>
						<?php else:?>
						<?php foreach($gjt_garcom as $gg):?>
							<tr class="active">
								<td><?=$gg['nome']?></td>
								<td><?=$gg['quant_contas']?></td>
								<td><?=$gg['quant_gjt']?></td>
								<td>R$ <?=number_format($gg['soma_total'], 2, ',', '');?></td>
							</tr>
						<?php endforeach;endif;?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-lg-6">
			<h2>Ganhos por mesa</h2>
			<form class="form_ganhos_mesa_periodo">
				De<input type="date" name="de">
				até <input type="date" name="para">
				<button type="button" class="btn btn-default ganhos_mesa_periodo">
					<span class="fa fa-search"></span> Pesquisar
				</button>
				<br><br>
			</form>
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped table_ganhos_mesa">
					<thead>
						<tr>
							<th>Mesa</th>
							<th>Contas abertas</th>
							<th>Total de pedidos</th>
							<th>Total faturado</th>
						</tr>
					</thead>
					<tbody>
						<?php if(!$gjt_garcom):?>
							<tr>
								<td colspan="4">Não há registros a serem mostrados, faça uma nova pesquisa.</td>
							</tr>
						<?php else:?>
						<?php foreach($vendas_periodo as $vp):?>
							<tr class="active">
								<td><?=$vp['mesa_key']?></td>
								<td><?=$vp['quant_contas']?></td>
								<td><?=$vp['quant_prod']?></td>
								<td>R$ <?=number_format($vp['soma_total'], 2, ',', '');?></td>
							</tr>
						<?php endforeach;endif;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- /LINHA -->
	<div class="row">
		<div class="col-lg-6">
			<h2>Pratos mais vendidos</h2>
			<form class="form_ranking_pratos">
				De<input type="date" name="de">
				até <input type="date" name="para">
				<button type="button" class="btn btn-default ranking_pratos">
					<span class="fa fa-search"></span> Pesquisar
				</button>
				<br><br>
			</form>
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped table_ranking_pratos">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Preço</th>
							<th>Quantidade Vendida</th>
						</tr>
					</thead>
					<tbody>
						<!--por default vem de dashboard caso pesquisado sera inserido pelo preencher_dashboard.js-->
						<?php if(!$gjt_garcom):?>
							<tr>
								<td colspan="3">Não há registros a serem mostrados, faça uma nova pesquisa.</td>
							</tr>
						<?php else:?>
						<?php foreach($ranking_pratos as $rp):?>
							<tr class="active">
								<td><?=$rp['nome']?></td>
								<td><?=$rp['preco']?></td>
								<td><?=$rp['quantidade_vendida']?></td>
							</tr>
						<?php endforeach;endif;?>
					</tbody>
				</table>
			</div>
		</div>
</div>
<!-- /.row -->

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
</body>