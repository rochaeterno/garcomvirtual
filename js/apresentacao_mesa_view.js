$(document).ready(function(){
	$('#slct').change(function(){
        $('#enviar').show();
        $('#enviarcode').hide();
    });

    $('.close').click(function(){
    	$('#alertg').hide();  
    });

	$('#texto').click(function(){
        $('#enviar').hide();
        $('#enviarcode').show();
        $("#texto").val(''); 
        $("#texto").attr('type', 'text'); 
        $("#texto").attr("placeholder", "Digite aqui a senha da conta");
	});


	$("#slct").change(function() {
		$("#option_default").remove();
		$("#enviar").addClass( "enviar" );
	});

	$('#enviarcode').click(function(){
		var id_conta = $('.seted_account').val();
		//url_validar_entrada="<?php echo site_url('conta/validar_entrada')
		$.post(url_validar_entrada,{id_conta:id_conta},function(data){
			if(!data){
      			$('#alertg').show();
      			$('#alertg').fadeOut(5000,function(){
        			$('#alertg').hide(); 
      			});
			}
		});

	});
	
	$('#enviar').click(function(){
		if($( "#enviar" ).hasClass( "enviar" )){
			//oculta de forma bruta os botões da tela
			$("#ora").toggle();
			$("#roda").toggle();
			setTimeout(function(){ $("#demora").show(1000);}, 15000);
			$("#buttons").hide();
			$('.select').hide();
			//url_validacao='<?php echo site_url("cliente_apt/seted_mesa"); ?>';
			selectedMesa = $("#slct").val();
			//faz o processo de abertura de mesa e cria sua session
			$.ajax({
				url:url_validacao,
				type:"POST",
				data:{slct:selectedMesa},
		
				success: function(dados){
					//url_conta="<?php echo site_url('conta/solicitar_abertura'); ?>";
					// apos a mesa ter sido criada, criamos a conta e tambem abrimos sua session
					$.ajax({
						url:url_conta,
						type:"POST",
						data:{idMesa:selectedMesa},
				
						success: function(data){
							if(data){
								/*com a conta criada, aguadamos a liberação pelo balcão para redirecionar o 
								cliente para o cardapio*/
								aguardando_conta();
							}
						}
					});
				}
			});
		}
	});
});