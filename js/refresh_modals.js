var total=0;
var gjt=10/100;
var gorjeta=0;
var total_final=0;
///url_refresh_modals = cliente_apt/get_refresh_modals
$(document).ready(function(){
	$('.refresh_history').click(function() {
		total=0;gjt=10/100;gorjeta=0;total_final=0;
		$("#historico tbody").html("");
		$.ajax({
			url:url_refresh_modals,
			type:"POST",
			data: {id_conta: id_conta_modals},
			
			success: function(dados){
				arr = $.parseJSON(dados);
				call_history();
			}
		});
	});
	
	$('.refresh_close').click(function() {
		total=0;gjt=10/100;gorjeta=0;total_final=0; //Mudar valor da Gorjeta
		$("#table_close tbody").html("");
		$.ajax({
			url:url_refresh_modals,
			type:"POST",
			data: {id_conta: id_conta_modals},
			
			success: function(dados){
				arr = $.parseJSON(dados);
				call_close();
				$("#table_close tbody").append(str3+str4);
				liberar_gorjeta();	
			}
		});
	});
	
});
function liberar_gorjeta(){
		$("#gorjeta").show();
		total_final =total+gorjeta;
		total_final = total_final.toFixed(2);
		$('#precofinal').html(total_final);
   		$('#last_gorjeta').val(gjt*100);
   		$('#negativo').show();
   		$('#positivo').hide();
   		$('#gorjeta').css("background-color","#4dff002e");
   		$('.price').show();
   		//teste_gorjeta = parseFloat($('#last_gorjeta').val());
   		//alert(teste_gorjeta);
	};

	function negar_gorjeta(){
		total_final =total;
		total_final = total_final.toFixed(2);
		$('#precofinal').html(total_final);
   		$('#last_gorjeta').val(0);
   		$('#positivo').show();
   		$('#negativo').hide();
   		$('#gorjeta').css("background-color","#ff11002e");
   		$('#price').hide();
	};

	
	function call_close(){
		if(arr == ""){
			str1 = '<p>Nenhum produto pedido!</p>';
		}
		else{
			for(i = 0; i < arr['fechar'].length; i++){
				if(arr['fechar'][i]['idConta'] == id_conta_modals){
					total=total+arr['fechar'][i]['preco']*arr['fechar'][i]['quantidade'];
					str2 = "<tr><td style='display: none'>"
					+arr['fechar'][i]['quantidade']
					+"</td><td>"+
					arr['fechar'][i]['quantidade']+'x '+arr['fechar'][i]['nome']
					+"</td><td>R$ "+
					(arr['fechar'][i]['preco']*1).toFixed(2)
					+"</td><td>R$ <span class='price'>"+
					(arr['fechar'][i]['preco']*arr['fechar'][i]['quantidade']).toFixed(2)
					+"</span></td>";
				}
				$("#table_close tbody").append(str2);
			}
			if(total>0){
			gorjeta=total*gjt;
			}
			str3 = '<tr id="gorjeta" style="background-color: #4dff002e;">'+
        	'<td style="display: none"><span id="gjt"></span>'+String(gjt*100)+'%</td><td>Gorjeta '+String(gjt*100)+'%</td><td>'+
        	'<button id="positivo" onclick="liberar_gorjeta()" type="button" class="btn btn-default btn-sm"'+
        	' style="border:none;font-size: 16px;border-radius:none;min-width:0px;display:none;background:none;">'+
        	'<span class="fa fa-check" aria-hidden="true" style="color:rgba(118, 171, 96);"></span>'+
        	'</button>'+
        	'<button id="negativo" onclick="negar_gorjeta()" type="button" class="btn btn-default btn-sm"'+ 
        	'style="border:none;font-size: 16px;;border-radius:none;min-width:0px;background:none;">'+
        	'<span style="color:rgba(255, 113, 102);" class="fa fa-times"></span>'+
        	'</button>'+
        	'</td><td id="adgorjeta">R$<span id="price" class="price"> '+gorjeta.toFixed(2)+'</span></td></tr>';
		
		str4 = '<tr><td scope="col" colspan="2"><b>Total</b></td>'+
		'<td>R$ <span id="precofinal">'+total.toFixed(2)+'</span></td></tr>';
		}		
	}
	
	function call_history(){
		var str2="";
		var str3="";
		var str4="";
		if (arr == ""){
			str2 = "<p>Nenhum produto pedido!</p>";
		}
		else{
			for(i = 0; i < arr['historico'].length; i++){
				if(arr['historico'][i]['idConta'] == id_conta_modals){
					preco = arr['historico'][i]['preco']*arr['historico'][i]['quantidade'];
					str2 = '<tr><td style="display: none">'+String(arr['historico'][i]['quantidade'])+'</td><td>'+String(arr['historico'][i]['quantidade'])+'x '
					+String(arr['historico'][i]['nome'])+'</td><td style="display: none">R$ '+(preco/arr['historico'][i]['quantidade']).toFixed(2)
					+'</td><td>R$ '+preco.toFixed(2)+'</td><td>';
								
					a=arr['historico'][i]['status'];
					if(a=="1"){
						str3 = '<i class="fa fa-check" aria-hidden="true"></i>';
					}else{
						str3 = '<i class="fa fa-refresh fa-spin" ></i>';
					}
					str4 = '</td></td>';
				}
				var modal = str2+str3+str4;
				$("#historico tbody").append(modal);
			}
		}
	}	