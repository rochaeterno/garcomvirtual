<?php
/* este codigo e responsavel por criar um lopp que verifica se a conta foi fechada no balcão por meio do aguardano_conta.js, caso a mesma já tenha sido fechada os cache de idConta são limpos e o usuario retorna a pagina de solicitação de abertura*/
defined('BASEPATH') OR exit('No direct script access allowed');
if(isset($_SESSION['idConta'])){
	$newdata = array('lastIdConta' => $_SESSION['idConta']);
	$this->session->set_userdata($newdata);
	unset($_SESSION['idConta']);
}
if(isset ($_SESSION['id_mesa'])){
	$cod_mesa = $_SESSION['id_mesa'];
}
//testa se já a uma mesa setada e então ja passa para a tela de solicitação de abertura
if(!isset ($cod_mesa) || $_SESSION['status_conta'] != 2){
	$redirect = site_url("cliente_apt/seted_mesa");
	header("location:$redirect");	
}
?><!DOCTYPE html>
<HTML>
<HEAD>
	<title>Obrigado pela preferência!</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--JavaScripts-->
	<script src="<?php echo base_url();?>js/jquery.js"></script>
	<script src="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/js/bootstrap.min.js"></script>
	<!--Ícones e Bootstrap-->
	<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/css/bootstrap.min.css">

	<!--NOVO-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style3.css">

	<style>
	.fa {
		margin-left: -12px;
		margin-right: 8px;
	}
	.w3-orange,.w3-hover-orange:hover{
		color:white!important;background-color:#C55D03!important}
	</style>

	<meta charset="UTF-8">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript">url_status='<?php echo site_url("conta/verificar_status"); ?>';</script>
	<script type="text/javascript">url_cardapio='<?php echo site_url("cliente_apt/seted_mesa"); ?>';</script>
	<script src="<?php echo base_url();?>js/aguardando_conta.js"></script>
</HEAD>

<body style="background-color: black;">
	<form id="form1"> 
		<input type="hidden" id="status" name="status" value="3"/>
	</form>
	<div class="bgimg w3-display-container w3-animate-opacity w3-text-white">
		<div class="w3-display-topleft w3-padding-large w3-xlarge">
			Logo
		</div>
		<div class="w3-display-middle">
  	<!--NOVO: Loading.
  		Existem 3 opções de loading-->
  		<h1 class="w3-jumbo w3-animate-top">Obrigado por sua presença<br><i class="fa fa-refresh fa-spin"></i></h1>
  		<br>
  		<hr class="w3-border-grey" style="margin:auto;width:40%">
  		<p class="w3-large w3-center">Sua conta está sendo encerrada.</p>
  		<button href="#lista" data-toggle="modal" class="w3-button w3-display-middle" style="top:105%;border: 1px solid;border-radius: 3px;padding: 10px;background: none;">
  		Lista da Conta</button>
  	</div>
  </div>
  <!--Lista-->
  <div id="lista" class="modal fade">
  	<div class="modal-dialog">
  		<div class="modal-content"">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal">&times;</button>
  				<h4 class="modal-title">Lista de Consumo</h4>
  			</div>
  			<div class="modal-body">
  				<table class="table table-striped">
  					<thead>
  						<tr>
  							<th scope="col">Produto</th>
  							<th scope="col" style="display: none">Quantidade</th>
  							<th scope="col">Preço</th>
  							<th scope="col">Total</th>
  						</tr>
  					</thead>
  					<tbody>
  						<?php $total=0;$cont=0;?>
  						<?php foreach($historico as $row):?>
  							<tr>
  								<?php
  								$a=$row['quantidade'];
  								$b=$row['preco'];
  								$ptotal=$a*$b;
  								$total=$total+$ptotal;
  								?>
  								<td><b><?=$row['quantidade']?>x </b><?=$row['nome']?></td>
  								<td style="display: none"><?=$row['quantidade']?></td>
  								<td>R$ <?php echo number_format($row['preco'], 2, ',', '.');?></td>
  								<td>R$ <?php echo number_format($ptotal, 2, ',', '.');?></td>
  							</tr>
  							<?php if($row['gorjeta']=='10'):?>
  								<?php $cont=1;?>
  							<?php endif;?>
  						<?php endforeach;?>

  						<?php if($cont==1):?>
  							<?php $gorjeta=$total*0.1;?>
  							<tr>
  								<td>Gorjeta</td>
  								<td>10%</td>
  								<td>R$ <?php echo number_format($gorjeta, 2, ',', '.');?></td>
  							</tr>
  							<?php $total=$total+$gorjeta;?>
  						<?php endif;?>
  						<tr>
  							<td colspan="2"><strong>Total</strong></td>
  							<td>R$ <?php echo number_format($total, 2, ',', '.');?></td></td>
  						</tr>
  					</tbody>
  				</table>
  			</div>
  			<div class="modal-footer">
  				<p>Aguarde o garçom para finalizar o atendimento</p>
  			</div>
  		</div>
  	</div>
  </div>
<!--<script type="text/javascript">
	//ainda pensando em como desenvolver essa parte
	$(".solicitarAbertura").click(function(){
		$.getJSON("<?php echo base_url();?>/json/cacheMesa.txt", function(result)
    {
			$.each(result, function(i, field)
            {
				$("div").append(field + " ");
			}
		}
	});
</script>-->

</body>
</HTML>