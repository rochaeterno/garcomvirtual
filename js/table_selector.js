$(document).ready(function(){
	// a seguinte função remove o piscar
	$("div").on( "click",".blink",function(){
		$(this).removeClass( "blink" );
	});
	//a seguinte funcao filtra os pedidos feitos por mesa
	$("div").on( "click",".ref",function(){
		$('.aparecer2').show();
		$(".cabecalho").html("");
		$(".pedidos").html("");
		$("#dados_ocultos").html("");

		var id_mesa = $(this).find('#id_tb').val();
		var num_mesa = $(this).find('#cod_tb').val();
		//call_history='<?php echo site_url("balcao/pull_history"); ?>'
		$.ajaxSetup({async:false});
		$.post(call_history,{idMesa:id_mesa},function(data_history){
			tot_pedidos = data_history.indiceAberto.length;
			
			tot_s_pedido = data_history.indiceFechado.length;
			for(i = 0; i<tot_s_pedido; i++){
				status_conta = data_history.indiceFechado[i].status_conta;
				id_conta_atual = data_history.indiceFechado[i].idConta;
				str1 = "Mesa "+num_mesa+" - Conta <span class='id-conta'>"+id_conta_atual+"</span> - Garçom:<span class='id-garcom'></span>";

				$(".cabecalho").html(str1);
				if(status_conta>=0){
					str4 = "<div class='row'><div class='col-lg-6 preto'><a href='#fechar' class='fechamento refresh_close last_fechamento forcar' idConta='"+id_conta_atual+"'"+
							"data-toggle='modal' style='background-color: black;color:white;padding: 6px;'>Fechamento Forçado <i class='fa fa-close'></i></a></div>";
					str5 = "<div class='col-lg-6 preto'><a href='#trocar' class='fechamento' idConta='"+id_conta_atual+"' data-toggle='modal'"+
							"style='background-color: black;color:white;padding: 6px;'>Trocar Mesa<i class='fa fa-refresh'></i></a></div></div>";
				
					$("#dados_ocultos").prepend(str4 + str5);

					$.post(reset_alerts,{id_conta:id_conta_atual},function(data_count){
						$('#table_line').find('#'+id_mesa+'').find('.pedidos_feitos').html("");
						$('#table_line').find('#'+id_mesa+'').find('.pedidos_feitos').prepend(data_count.quant);

						if(data_count.quant_garcom>0){
							tot_call = data_count.id_call.length;
							for(k = 0; k<tot_call; k++){
								str6 = '<tr><td id="nMesa">'+id_mesa+'</td><td class="tdbotao-azul" colspan="4"><a id_update="'+data_count.id_call[k].idAlert+'"'+
								'class="atualizar_status" tabela="tbalerts" idConta="'+id_conta_atual+'" style="display: block;">'+
								'<span>Garçom Solicitado</span></a></td></tr>';

								$(".pedidos").prepend(str6);
							}
						}
					},'json');
				}
				if(status_conta==0){
 					str2 = "<tr><td class='tdbotao-verde' colspan='4'>"+
						"<a onclick='return abrirConta("+id_mesa+");'href='#addgarcom' data-toggle='modal'"+ 
						"style='display: block;'><span>Abrir Conta</span></a></td></tr>";
			
					$(".pedidos").prepend(str2);
				}else if(status_conta==2){
 					str3 = "<tr><td class='tdbotao-vermelho' colspan='4'><a href='#fechar' class='fechamento comum refresh_close last_fechamento'"+
 					"idConta='"+id_conta_atual+"' data-toggle='modal' style='display: block;'><span class='tdbotao-vermelho'>Fechar Conta</span></a></td></tr>";
 			
 					$(".pedidos").prepend(str3);
				}
			}
			 if(tot_pedidos>=1){
				for(j = 0; j<tot_pedidos; j++){
 					status_conta = data_history.indiceAberto[j].status_conta;
					id_conta_atual = data_history.indiceAberto[j].idConta;
					quantidade_atual = data_history.indiceAberto[j].quantidade;
					garcom_atual = data_history.indiceAberto[j].idGarcom;
					nome_atual = data_history.indiceAberto[j].nome;
					hora_atual = data_history.indiceAberto[j].hora_pedido;
					id_pedido_atual = data_history.indiceAberto[j].id_produtos_pedidos;
					status_pedido_atual = data_history.indiceAberto[j].status;

					str4 = "<tr><td id='"+id_mesa+"' style='display: none'>'"+id_conta_atual+"'</td><td style='text-align: center;display: none'>"+id_mesa+"</td>"+
							"<td id='quantidade'>"+quantidade_atual+"</td><td>"+nome_atual+"</td><td>"+hora_atual+"</td>";

					if(status_pedido_atual == 0){
						str5 = "<td><button style='border:none;background:none;'><span status_pedido='0' id_update='"+
						id_pedido_atual+"' tabela='produtospedidos' class='fa fa-square atualizar_status' style='font-size:1.5em;'></span></button>";
					}else if(status_pedido_atual == 1){
						str5 = "<td><button style='border:none;background:none;'><span status_pedido='1' id_update='"+
						id_pedido_atual+"' tabela='produtospedidos' class='fa  fa-check-square atualizar_status' style='font-size:1.5em;'></span></button>";
					}
 					$(".pedidos").append(str4 + str5);
 				}
			}
		},'json');
	});
});