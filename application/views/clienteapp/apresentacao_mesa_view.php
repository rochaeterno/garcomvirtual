<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<?php
$cod_conta =  $this->session->idConta;

if(isset($cod_conta)){
  $redirect = site_url("cliente_apt/seted_account/".$cod_conta);
  header("location:$redirect");
}
?>
<!--Adição do HEAD-->
<HEAD> 
  <title>POCKET WAITER</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--JavaScripts-->
  <script src="<?php echo base_url();?>js/jquery.js"></script>
  <script src="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/js/bootstrap.min.js"></script>
  <!--Ícones e Bootstrap-->
  <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/css/bootstrap.min.css">
  <!--VARIAVEIS JAVASCRIPT-->
  <script type="text/javascript">url_status='<?php echo site_url("conta/verificar_status"); ?>';</script>
  <script type="text/javascript">url_validacao='<?php echo site_url("cliente_apt/seted_mesa"); ?>';</script>
  <script type="text/javascript">url_cardapio='<?php echo site_url("cliente_apt/todos"); ?>';</script>
  <script type="text/javascript">url_conta="<?php echo site_url('conta/solicitar_abertura'); ?>";</script>
  <script type="text/javascript">url_validar_entrada="<?php echo site_url('conta/validar_entrada'); ?>";</script>
  <!--CSS CUSTOMIZADO-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style3.css">
  <style type="text/css">
  .envio{display: none;}
  ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: white;
    opacity: .7; /* Firefox */
  }

</style>
<!--JAVASCRIPT IMPORTS-->
<script src="<?php echo base_url();?>js/aguardando_conta.js"></script>
<script src="<?php echo base_url();?>js/apresentacao_mesa_view.js"></script>
</head>
<body onLoad="history.go(+1)">
  <div id="alertg" class="alert alert-danger" role="alert" style="left:30%;position:absolute;z-index:3000;display: none">
      <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>Erro.</strong> Número da conta inválida ou conta já fechada! 
  </div>
  <div class="bgimg w3-display-container w3-animate-opacity w3-text-white">
    <div class="w3-display-topleft w3-padding-large w3-xlarge">
    </div>
    <div class="w3-display-middle">
      <div id="mudamuda">
        <h1 id="ora" class="w3-jumbo w3-animate-top">POCKET<br>WAITER</h1>
        <h1 id="roda" class="w3-jumbo w3-animate-top" style="display: none;"><i class="fa fa-refresh fa-spin"></i></h1>
      </div>
      <BR>
      <div class="select">
        <form id="form1"> 
          <input type="hidden" id="status" name="status" value="1"/>
          <select name="slct" id="slct">
            <option id="option_default">Selecione sua mesa</option>
            <?php if ($mesas == FALSE): //Se não houver nenhuma mesa?>
              <option>Nenhuma mesa cadastrado!</option>
              <?php else: ?>
              <?php foreach ($mesas as $row): // para cada mesa
              if($row['statusMesa']==0): //Que esteja vazia ?> 
               <option value="<?=$row['idMesa'] ?>">Mesa <?=$row['numMesa']?></option>                
             <?php endif; endforeach; endif; ?>
           </select>
         </form>
       </div>
       <br>
       <div id="buttons">
         <button type="button" id="enviar" class="w3-button envio" style="background:none;margin-bottom: 20px;">
          Começar <i class="fa fa-sign-in" aria-hidden="true"></i>
        </button>
        <?php //Código para esconder tudo está em apresentacao_mesa_view.js ?>
        <h1 style="background:none;margin-bottom: 20px;">ou</h1>
        <input id="texto" type="button" class="w3-button seted_account" style="background:none;margin-bottom: 20px;" value="Digite o número da conta">

        <button type="button" id="enviarcode" class="w3-button envio" style="background:none;">
          Começar <i class="fa fa-sign-in" aria-hidden="true"></i>
        </button>
      </div>
      <div id="demora" style="display:none">
        <h1 style="background:none;margin-bottom: 20px;">Demorando muito?</h1>
        <button type="button" id="atualizar" class="w3-button" onClick="window.location.reload()" style="background:none;">Clique aqui</button>

      </div>
    </div>
</body>

</html>