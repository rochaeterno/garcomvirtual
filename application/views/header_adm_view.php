<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<?php 
if((!isset ($_SESSION['user']) == true) and (!isset ($_SESSION['pass']) == true))
{
  unset($_SESSION['user'],$_SESSION['pass']);  
}
$loggedUser =  $this->session->user;
$loggedPass =  $this->session->pass;
if(!isset ($loggedUser) or !isset($loggedPass)){
  header("Location:".site_url('login'));
}
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $titulo;?></title>
    <!--JavaScripts-->
    <script src="<?php echo base_url();?>js/jquery.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script type="text/javascript">url_ganhos_mesa='<?php echo site_url("dashboard/preencher_mesa_por_periodo"); ?>';</script>
    <script type="text/javascript">url_ranking_pratos='<?php echo site_url("dashboard/ranking_pratos"); ?>';</script>
    <script type="text/javascript">url_gjt_garcom='<?php echo site_url("dashboard/gjt_garcom"); ?>';</script>
    <!--Fontes e Bootstrap-->
    <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/css/bootstrap.min.css">
    <!-- CSS costumizado -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/crud.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/sb-admin.css">
    <script src="<?php echo base_url();?>js/preencher_dashboard.js"></script>

</head>
<body>



    <div id="wrapper">
        <!--Navegador de cima, pode ser incluido mais coisas-->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#C55D03;border-color:#C55D03;">
            <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="background-color: black">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('Admin');?>" style="color:white;"><strong>Administrador</strong></a>
            </div>
            <!--Menu do canto direito, podem ser incluidos mais botões-->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown" >
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:white;"><i class="fa fa-user"></i> <?=$_SESSION['nome']?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <!--<li class="divider"></li>-->
                        <li>
                            <a href="<?php echo site_url('Login/logout'); ?>">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Finalmente um menu 100% responsivo-->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="<?php echo site_url('Dashboard'); ?>">
                           <i class="fa fa-tachometer" aria-hidden="true"></i>
                       Relatórios</a>
                   </li>
                   <li>
                    <a href="<?php echo site_url('Mesa'); ?>">
                     <i class="fa fa-th" aria-hidden="true"></i>
                     Mesas
                 </a>
                              <!--<li>
                    <a href="<?php //echo site_url('tablets/index'); ?>">
                       <i class="glyphicon glyphicon-phone"></i>
                       Tablets
                   </a>
               </li>-->
           </li>
           <li>
            <a href="<?php echo site_url('Usuarios'); ?>">
                <i class="fa fa-users" aria-hidden="true"></i>
                Usuarios
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Produtos'); ?>">
                <i class="fa fa-cutlery" aria-hidden="true"></i>    
                Produtos
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('Suporte'); ?>">
                <i class="fa fa-address-card-o" aria-hidden="true"></i>
                Suporte
            </a>
        </li>
    </ul>
</div>
</nav>


