<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mesa extends CI_Controller{
	public function index(){
		$dados['titulo']="Mesas";
					//carrega a tela de inicial de Administrador
		$this->load->model('crud');
		$results=$this->crud->Select('tbmesa');
		$mesas['mesas']=$results;
		$this->load->model('crud');
		$this->load->view('header_adm_view',$dados);
		$this->load->view('mesas_view',$mesas);
		$this->load->view('modal_view',$mesas);
	}
	
	public function Insere_mesa(){
		$form['numMesa'] =  $_POST['numeroMesa'];
		$form['idTablet'] =  $_POST['tabletMesa'];
		
		$table = 'tbmesa';
		$dados = array('numMesa' => $form['numMesa'], 'idTablet' => $form['idTablet']);	
		
		$this->load->model('crud');
		$confirm = $this->crud->Insert($table, $dados);
		
		if($confirm){
		redirect('Mesa');	
		}
	}
	
	public function Verificar_Status(){
		//verifica o status de uma mesa identificada via post
		$id_mesa = $this->input->post('id_mesa');
		$table = 'tbmesa';
		
		$dados = array('idMesa' => $id_mesa, 'statusMesa' => '1');
		
		$this->load->model('conta_model');
		$confirm = $this->conta_model->Verificar_Status($table, $dados);
		
		if($confirm){
			$retorno = array("retorno"=>1,"id_mesa"=>$id_mesa);
		}
		else{
			$retorno = array("retorno"=>0,"id_mesa"=>$id_mesa);
		}
		echo json_encode($retorno);
	}
	
	public function Contar_mesas(){
		//conta o numero de mesas presentes no banco de dados
		$this->load->model('mesa_model');
		$this->mesa_model->Contar_mesas();
	}
}