<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Conteudo da pagina -->
<!--Navegador-->
<div id="page-wrapper" style="height:100vh;overflow: auto">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Mesas
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Cadastro de  <b>Mesas</b></h2>
                    </div>
                    <div class="col-sm-6">
                        <!--Nav da tabela-->
                        <a href="#addmesa" class="btn btn-laranja" data-toggle="modal">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i><span>Nova mesa</span>
                        </a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <!--Colunas-->
                        <th width="50%">ID</th>
                        <th width="50%">Número da mesa</th>
                        <th width="50%">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($mesas == FALSE): ?>
                        <p>Nenhum produto cadastrado!</p>
                        <?php else: ?>
                            <?php foreach ($mesas as $row):?>
                                <tr>
                                    <!--Linhas-->
                                    <td width="50%"><?=$row['idMesa']?></td>
                                    <td width="50%"><?=$row['numMesa']?></td>
                                    
                                    <td>
                                        <a class="delete" href="#excluirmesa<?=$row['idMesa']?>" data-toggle="modal">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                    </td>

                                </tr>
                                <!-- Excluir Mesa Modal HTML -->
                                <div id="excluirmesa<?=$row['idMesa']?>" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">                      
                                                <h4 class="modal-title">Excluir Mesa <?=$row['numMesa']?></h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">                    
                                                <p>Tem certeza que deseja excluir esse registro?</p>
                                                <p class="text-warning"><small>Essa ação não pode ser desfeita.</small></p>
                                            </div>
                                            <div class="modal-footer">
                                                <?php echo form_open('Crud/excluirMesa');?>
                                                <input type="hidden" name="id" value="<?=$row['idMesa']?>">
                                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                                                <input type="submit" class="btn btn-danger" value="Excluir">
                                                <?php echo form_close();?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;
                        endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        

        
