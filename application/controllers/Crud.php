<?php
class Crud extends CI_Controller {
	public function index(){}
	//EDITAR

	/*-------
	PRODUTOS
	--------*/
	public function EditarProdutos(){
		$id=$_POST['id'];
		$form['nome'] = $_POST['nomeProd'];
		$form['tipo'] = $_POST['tipoProd'];
		$form['desc'] = $_POST['descProd'];
		$form['preco'] = $_POST['precoProd'];
		if(isset($_POST['dispProd'])){
			$form['disp']='0';
		}
		else{
			$form['disp']='1';
		}
		$form['porcao'] = $_POST['porcao'];
		$form['destaque'] = $_POST['nivelDestaque'];
		$dados = array('nome' => $form['nome'],'descricao' => $form['desc'],
			'preco' => $form['preco'], 'disponibilidade' => $form['disp'], 'nivelDestaque' => $form['destaque'], 'porcao' => $form['porcao'],
			'tipo' => $form['tipo']);
		
		$this->load->model('editar');
		$conn = $this->editar->EditarProduto($id, $dados);
		redirect('Produtos');		
	}

	public function Atualizar_Disponibilidade(){
	$id=$_POST['id_produto'];
	$status=$_POST['change'];
	$this->load->model('produtos_model');
	$this->produtos_model->Update_Status($id,$status);
	redirect('Produtos');
	}
	/*-------------
	FOTO DO PRODUTO
	---------------*/
	public function EditarFotoProduto(){
		$id=$_POST['id'];
		$config['upload_path']  = './products_img/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '20048';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['file_name'] = $_POST['nome'];

		$this->load->model('produtos_model');
		$exclui=$this->produtos_model->UnlinkaImg($id);
		unlink('..'.$exclui[0]['img']);
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('imgProd'))
		{
			$error = array('error' => $this->upload->display_errors());
            die($error['error']);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$imagem = "/rest/products_img/";
			$imagem .= $this->upload->data('file_name');
			$this->load->model('produtos_model');
			$this->produtos_model->Set_img('tbprodutos',$imagem,$id);
			redirect('Produtos');
		}
	}
	/*------
	USUARIO
	-------*/
	public function EditarUser(){
		$id=$_POST['id'];
		$form['nome'] = $_POST['nomeUsu'];
		$form['login'] = $_POST['loginUsu'];
		$form['senha'] = $_POST['senhaUsu'];
		$form['tipo'] = $_POST['TipoUser'];
		$dados = array('nome' => $form['nome'],'login' => $form['login'],
			'senha' => $form['senha'], 'tipoUsuario' => $form['tipo']);
		var_dump($dados);
		$this->load->model('editar');
		$this->editar->EditarUser($id, $dados);
		redirect('usuarios');		
	}
	
	//EXCLUIR
	/*---
	MESA
	-----*/
	public function excluirMesa(){
		$id=$_POST['id'];
		$this->load->model('excluir');
		$this->excluir->DeleteMesa($id);
		redirect('Mesa');
	}
	/*-----
	TABLET
	------*/
	public function excluirTablet(){
		$id=$_POST['id'];
		$this->load->model('excluir');
		$this->excluir->DeleteTablet($id);
		redirect('Tablets');
	}
	/*------
	USUARIO
	-------*/
	public function excluirUsuario(){
		$id=$_POST['id'];
		$this->load->model('excluir');
		$this->excluir->DeleteUser($id);
		redirect('Usuarios');
	}
	/*-------
	PRODUTOS
	--------*/
	public function excluirProduto(){
		$id=$_POST['id'];
		$this->load->model('produtos_model');
		$exclui=$this->produtos_model->UnlinkaImg($id);
		unlink('..'.$exclui[0]['img']);
		$this->load->model('excluir');
		$this->excluir->DeleteProduto($id);
		redirect('Produtos');
	}

}
