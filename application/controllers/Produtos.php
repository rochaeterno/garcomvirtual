<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller{
	public function index(){
		$dados['titulo']="Produtos";
		//carrega a tela de inicial de Administrador
		$this->load->model('crud');
		$result=$this->crud->Select('tbprodutos');
		$produtos['produtos']=$result;
		$this->load->view('header_adm_view',$dados);
		$this->load->view('produtos_view',$produtos);
	}	
	public function Insere_produtos(){
		$table = 'tbprodutos';
		
		$form['nome'] = $_POST['nomeProd'];
		$form['tipo'] = $_POST['tipoProd'];
		$form['desc'] = $_POST['descProd'];
		$form['preco'] = $_POST['precoProd'];
		if(isset($_POST['dispProd'])){
			$form['disp']='0';
		}
		else{
			$form['disp']='1';
		}
		$form['porcao'] = $_POST['porcao'];
		$form['destaque'] = $_POST['nivelDestaque'];
		
		$dados = array('nome' => $form['nome'],'descricao' => $form['desc'],
			'preco' => $form['preco'], 'disponibilidade' => $form['disp'], 'nivelDestaque' => $form['destaque'], 'porcao' => $form['porcao'],
			'tipo' => $form['tipo']);
		
		$this->load->model('crud');
		$conn = $this->crud->Insert_return_id($table, $dados);
		//print_r($conn);
		
		$config['upload_path']  = '././products_img/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '20048';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['file_name'] = $form['nome'];
        //seria interessante inserir aqui um tamanho max em kbts e tamanho max em pixels
        //um nome padronizado provavelmente sera necessario para facilitar a comunicação com o banco
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('imgProd'))
        {
            $error = array('error' => $this->upload->display_errors());
            die($error['error']);

            //$this->load->view('upload_form', $error);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
           	$form['img'] = "/rest/products_img/";
			$form['img'] .= $this->upload->data('file_name');
			 	
            //$this->load->view('upload_success', $data);
        }
		
		$this->load->model('produtos_model');
		$confirm = $this->produtos_model->Set_img($table,$form['img'],$conn);
		
		if($confirm){
			redirect('Produtos/index');
		}
	}
}
?>