<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!-- Conteudo da pagina -->
<div id="content" style="padding: 30px; padding-top: 3px;overflow: auto;height:100vh;">
    <h2>Tablets</h2>
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Cadastro de  <b>Tablets</b></h2>
                </div>
                <div class="col-sm-6">
                    <!--Nav da tabela-->
                    <a href="#addtablet" class="btn btn-laranja" data-toggle="modal">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i><span>Novo tablet</span>
                    </a>
                </div>
            </div>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>
                        <span class="custom-checkbox">
                            <input type="checkbox" id="selectAll">
                            <label for="selectAll"></label>
                        </span>
                    </th>
                    <!--Colunas-->
                    <th width="50%">ID</th>
                    <th width="50%">Numero do Tablet</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($tablets == FALSE): ?>
                    <p>Nenhum produto cadastrado!</p>
                    <?php else: ?>
                        <?php foreach ($tablets as $linha):?>
                            <tr>
                                <td>
                                    <span class="custom-checkbox">
                                        <input type="checkbox" id="checkbox1" name="options[]" value="1">
                                        <label for="checkbox1"></label>
                                    </span>
                                </td>
                                <!--Linhas-->
                                <td><?=$linha['idtablet']?></td>
                                <td><?=$linha['macTablet']?></td>
                                <?php //echo form_open('Crud/excluirTablet');?>
                                <input type="hidden" name="id" value="<?=$linha['idtablet']?>">
                                <td>
                                    <a class="delete" href="#excluirmesa<?=$linha['idtablet']?>" data-toggle="modal">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <?php //echo form_close();?>
                            </tr>
                        <?php endforeach;
                    endif; ?>
                </tbody>
            </table>
            <div class="clearfix">
                <!--Footer, pode ser exclu�-->
                <div class="hint-text">Mostrando <b>1</b> de <b>1</b> linha</div>
                <ul class="pagination">
                    <li class="page-item disabled"><a href="#">Anterior</a></li>
                    <li class="page-item active"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item disabled"><a href="#" class="page-link">Pr�xima</a></li>
                </ul>
            </div>
        </div>
    </div>