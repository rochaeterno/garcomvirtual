$(document).ready(function() {
	//responsavel por atualizar a quantidade de pedidos no carrinho do cliente
$('.menos,.mais').on('click', function() {
    var $qty = $(this).closest('.quantidade').find('.qty'),
      currentVal = parseInt($qty.val()),
      isAdd = $(this).hasClass('mais');
    !isNaN(currentVal) && $qty.val(
      isAdd ? ++currentVal : (currentVal > 0 ? --currentVal : currentVal)
    );
  }); 
});