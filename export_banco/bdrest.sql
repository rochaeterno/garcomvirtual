-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 13/09/2019 às 00:30
-- Versão do servidor: 10.3.16-MariaDB
-- Versão do PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bdrest`
--

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `cozinha`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `cozinha` (
`numMesa` int(10)
,`nome` varchar(30)
,`quantidade` float
,`obs` varchar(255)
,`hora_pedido` datetime
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `ganhos_mesa`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `ganhos_mesa` (
`mesa_key` int(10)
,`id_conta` int(5)
,`quant_prod` float
,`soma_total` double
,`data` datetime
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `gjt_rel`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `gjt_rel` (
`id` int(11)
,`id_conta` int(5)
,`nome` varchar(50)
,`quant_gjt` decimal(20,0)
,`soma_total` double(19,2)
,`data` datetime
);

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `lastalert`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `lastalert` (
`idMesa` int(5)
,`status_conta` varchar(20)
,`idConta` int(5)
,`status` int(5)
,`tipoAlert` int(1)
,`idAlert` int(11)
);

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtospedidos`
--

CREATE TABLE `produtospedidos` (
  `id_produtos_pedidos` int(11) NOT NULL,
  `id_produtos` int(11) NOT NULL,
  `idConta` int(11) NOT NULL,
  `quantidade` float NOT NULL,
  `hora_pedido` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `obs` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `produtospedidos`
--

INSERT INTO `produtospedidos` (`id_produtos_pedidos`, `id_produtos`, `idConta`, `quantidade`, `hora_pedido`, `status`, `obs`) VALUES
(262, 9, 251, 1, '2018-07-28 16:35:07', '1', ''),
(263, 14, 251, 1, '2018-07-28 16:35:08', '1', ''),
(264, 15, 251, 1, '2018-07-28 16:35:08', '1', ''),
(265, 10, 251, 1, '2018-07-28 16:35:08', '1', ''),
(266, 9, 253, 1, '2018-07-28 16:37:50', '1', ''),
(267, 14, 253, 1, '2018-07-28 16:37:51', '1', ''),
(268, 15, 253, 1, '2018-07-28 16:37:51', '1', ''),
(269, 10, 253, 1, '2018-07-28 16:37:51', '1', ''),
(270, 9, 253, 1, '2018-07-28 16:37:51', '1', ''),
(271, 9, 254, 1, '2018-07-28 16:40:13', '1', ''),
(272, 14, 254, 1, '2018-07-28 16:40:14', '1', ''),
(273, 15, 254, 1, '2018-07-28 16:40:14', '1', ''),
(274, 10, 254, 1, '2018-07-28 16:40:14', '1', ''),
(275, 9, 254, 1, '2018-07-28 16:40:15', '1', ''),
(276, 9, 254, 1, '2018-07-28 16:40:15', '1', ''),
(277, 9, 255, 10, '2018-07-28 17:35:28', '1', ''),
(278, 32, 256, 1, '2018-08-04 11:00:04', '1', ''),
(279, 11, 256, 1, '2018-08-04 11:00:04', '1', ''),
(280, 37, 256, 1, '2018-08-04 11:00:04', '1', ''),
(281, 15, 256, 1, '2018-08-04 11:00:05', '1', ''),
(282, 32, 256, 2, '2018-08-04 11:00:05', '1', ''),
(283, 32, 257, 3, '2018-08-04 11:47:00', '1', ''),
(284, 11, 257, 1, '2018-08-04 11:47:01', '1', ''),
(285, 37, 257, 1, '2018-08-04 11:47:02', '1', ''),
(286, 15, 257, 1, '2018-08-04 11:47:02', '1', ''),
(287, 32, 259, 1, '2018-08-09 16:16:31', '1', ''),
(288, 11, 259, 1, '2018-08-09 16:16:31', '1', ''),
(289, 37, 259, 1, '2018-08-09 16:16:31', '1', ''),
(290, 15, 259, 1, '2018-08-09 16:16:32', '1', ''),
(291, 14, 260, 1, '2018-08-09 16:18:52', '1', ''),
(292, 10, 260, 1, '2018-08-09 16:18:52', '1', ''),
(293, 13, 261, 1, '2018-08-09 16:20:02', '1', ''),
(294, 9, 261, 1, '2018-08-09 16:20:02', '1', ''),
(295, 13, 261, 4, '2018-08-09 16:20:02', '1', ''),
(296, 32, 262, 3, '2018-08-21 19:14:00', '1', ''),
(297, 11, 262, 1, '2018-08-21 19:14:00', '1', ''),
(298, 37, 262, 1, '2018-08-21 19:14:00', '1', ''),
(299, 11, 264, 5, '2018-08-27 19:27:14', '1', ''),
(300, 37, 264, 1, '2018-08-27 19:27:14', '1', ''),
(301, 32, 265, 1, '2018-08-28 16:23:06', '1', ''),
(302, 32, 265, 6, '2018-08-28 16:23:07', '1', ''),
(303, 11, 265, 1, '2018-08-28 16:23:07', '1', ''),
(304, 37, 265, 1, '2018-08-28 16:23:07', '1', ''),
(305, 15, 265, 1, '2018-08-28 16:23:07', '1', ''),
(306, 37, 265, 1, '2018-08-28 16:23:07', '1', ''),
(307, 15, 265, 1, '2018-08-28 16:23:07', '1', ''),
(308, 15, 265, 1, '2018-08-28 16:23:07', '1', ''),
(309, 32, 266, 1, '2018-09-03 23:10:03', '1', ''),
(310, 11, 267, 1, '2018-09-08 02:33:46', '1', ''),
(311, 11, 268, 3, '2018-09-16 19:49:43', '1', ''),
(312, 11, 270, 1, '2018-12-28 11:24:09', '1', ''),
(313, 11, 283, 10, '2019-01-20 03:34:56', '1', ''),
(314, 14, 285, 1, '2019-01-20 05:44:16', '1', ''),
(315, 11, 289, 1, '2019-01-23 14:55:56', '1', ''),
(316, 11, 289, 1, '2019-01-23 15:27:01', '0', ''),
(317, 37, 290, 1, '2019-01-23 18:29:56', '0', ''),
(318, 11, 290, 1, '2019-01-23 18:39:52', '0', ''),
(319, 11, 290, 1, '2019-01-23 19:38:05', '0', ''),
(320, 37, 290, 1, '2019-01-23 19:38:21', '0', ''),
(321, 11, 295, 1, '2019-01-25 20:05:17', '0', ''),
(322, 37, 299, 1, '2019-09-12 16:57:29', '1', ''),
(323, 10, 299, 1, '2019-09-12 16:58:45', '1', ''),
(324, 32, 299, 1, '2019-09-12 16:58:45', '1', '');

-- --------------------------------------------------------

--
-- Estrutura stand-in para view `ranking_pratos`
-- (Veja abaixo para a visão atual)
--
CREATE TABLE `ranking_pratos` (
`id` int(5)
,`nome` varchar(30)
,`preco` float
,`contador` int(11)
,`quantidade_vendida` float
,`hora_pedido` datetime
);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbalerts`
--

CREATE TABLE `tbalerts` (
  `idAlert` int(11) NOT NULL,
  `idConta` int(5) NOT NULL,
  `tipoAlert` int(1) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbalerts`
--

INSERT INTO `tbalerts` (`idAlert`, `idConta`, `tipoAlert`, `status`) VALUES
(589, 251, 0, 0),
(590, 251, 1, 0),
(591, 251, 1, 0),
(592, 251, 1, 0),
(593, 251, 1, 1),
(594, 251, 3, 0),
(595, 252, 0, 0),
(596, 253, 0, 0),
(597, 253, 1, 0),
(598, 253, 1, 0),
(599, 253, 1, 0),
(600, 253, 1, 0),
(601, 253, 1, 1),
(602, 253, 3, 0),
(603, 254, 0, 0),
(604, 254, 1, 0),
(605, 254, 1, 0),
(606, 254, 1, 0),
(607, 254, 1, 0),
(608, 254, 1, 1),
(609, 254, 3, 0),
(610, 255, 0, 0),
(611, 255, 1, 1),
(612, 255, 3, 0),
(613, 256, 0, 0),
(614, 256, 1, 0),
(615, 256, 1, 0),
(616, 256, 1, 0),
(617, 256, 1, 0),
(618, 256, 1, 1),
(619, 256, 3, 0),
(620, 257, 0, 0),
(621, 257, 1, 0),
(622, 257, 1, 0),
(623, 257, 1, 0),
(624, 257, 1, 1),
(625, 257, 3, 0),
(626, 258, 0, 0),
(627, 258, 3, 0),
(628, 259, 0, 0),
(629, 259, 1, 0),
(630, 259, 1, 0),
(631, 259, 1, 0),
(632, 259, 1, 1),
(633, 259, 3, 0),
(634, 260, 0, 0),
(635, 260, 1, 0),
(636, 260, 1, 1),
(637, 260, 3, 0),
(638, 261, 0, 0),
(639, 261, 1, 0),
(640, 261, 1, 0),
(641, 261, 1, 1),
(642, 261, 3, 0),
(643, 262, 0, 0),
(644, 262, 1, 0),
(645, 262, 1, 0),
(646, 262, 1, 1),
(647, 262, 3, 0),
(648, 263, 0, 0),
(649, 264, 0, 0),
(650, 264, 1, 0),
(651, 264, 1, 1),
(652, 264, 3, 0),
(653, 265, 0, 0),
(654, 265, 1, 0),
(655, 265, 1, 0),
(656, 265, 1, 0),
(657, 265, 1, 0),
(658, 265, 1, 1),
(659, 265, 3, 0),
(660, 0, 0, 0),
(661, 266, 0, 0),
(662, 266, 1, 1),
(663, 266, 3, 0),
(664, 267, 0, 0),
(665, 267, 2, 1),
(666, 267, 1, 1),
(667, 267, 3, 0),
(668, 268, 0, 0),
(669, 268, 1, 1),
(670, 269, 0, 0),
(671, 270, 0, 0),
(672, 270, 1, 0),
(673, 270, 2, 1),
(674, 270, 3, 0),
(675, 271, 0, 0),
(676, 272, 0, 0),
(677, 272, 3, 0),
(678, 273, 0, 0),
(679, 274, 0, 0),
(680, 274, 3, 0),
(681, 275, 0, 0),
(682, 276, 0, 0),
(683, 276, 3, 0),
(684, 276, 3, 0),
(685, 277, 0, 0),
(686, 277, 3, 0),
(687, 278, 0, 0),
(688, 279, 0, 0),
(689, 280, 0, 0),
(690, 278, 3, 0),
(691, 281, 0, 0),
(692, 281, 2, 0),
(693, 281, 2, 0),
(694, 281, 2, 0),
(695, 281, 2, 0),
(696, 281, 2, 0),
(697, 281, 2, 0),
(698, 281, 2, 0),
(699, 281, 2, 0),
(700, 281, 2, 0),
(701, 281, 2, 0),
(702, 281, 2, 0),
(703, 281, 2, 0),
(704, 281, 2, 0),
(705, 281, 2, 0),
(706, 281, 2, 0),
(707, 281, 2, 0),
(708, 281, 2, 0),
(709, 281, 2, 0),
(710, 281, 2, 0),
(711, 281, 2, 0),
(712, 281, 2, 0),
(713, 281, 2, 0),
(714, 281, 2, 0),
(715, 281, 2, 0),
(716, 281, 2, 0),
(717, 281, 2, 0),
(718, 281, 2, 0),
(719, 281, 2, 0),
(720, 281, 2, 0),
(721, 281, 2, 0),
(722, 281, 2, 0),
(723, 281, 2, 0),
(724, 281, 2, 0),
(725, 281, 2, 0),
(726, 281, 2, 0),
(727, 281, 2, 0),
(728, 281, 2, 0),
(729, 281, 2, 0),
(730, 281, 2, 0),
(731, 281, 2, 0),
(732, 281, 2, 0),
(733, 281, 2, 0),
(734, 281, 2, 0),
(735, 281, 2, 0),
(736, 281, 2, 0),
(737, 281, 2, 0),
(738, 281, 2, 0),
(739, 281, 2, 0),
(740, 281, 2, 0),
(741, 281, 2, 0),
(742, 281, 2, 0),
(743, 281, 2, 0),
(744, 281, 2, 0),
(745, 281, 2, 0),
(746, 281, 2, 0),
(747, 281, 2, 0),
(748, 281, 2, 0),
(749, 281, 2, 0),
(750, 281, 2, 0),
(751, 281, 2, 0),
(752, 281, 2, 0),
(753, 281, 2, 0),
(754, 281, 2, 0),
(755, 281, 2, 0),
(756, 281, 2, 0),
(757, 281, 2, 0),
(758, 281, 2, 0),
(759, 281, 2, 0),
(760, 281, 2, 0),
(761, 281, 2, 0),
(762, 281, 2, 0),
(763, 281, 2, 0),
(764, 281, 2, 0),
(765, 281, 2, 0),
(766, 281, 2, 0),
(767, 281, 2, 0),
(768, 281, 2, 0),
(769, 281, 2, 0),
(770, 281, 2, 0),
(771, 281, 2, 0),
(772, 281, 2, 0),
(773, 281, 2, 0),
(774, 281, 3, 0),
(775, 282, 0, 0),
(776, 282, 3, 0),
(777, 283, 0, 0),
(778, 283, 1, 0),
(779, 283, 3, 0),
(780, 284, 0, 0),
(781, 285, 0, 0),
(782, 285, 2, 0),
(783, 285, 1, 0),
(784, 285, 2, 0),
(785, 285, 2, 0),
(786, 285, 2, 0),
(787, 286, 0, 0),
(788, 286, 3, 0),
(789, 287, 0, 0),
(790, 287, 2, 0),
(791, 287, 2, 0),
(792, 288, 0, 0),
(793, 288, 2, 0),
(794, 288, 2, 0),
(795, 288, 2, 0),
(796, 288, 2, 0),
(797, 288, 2, 0),
(798, 288, 2, 0),
(799, 289, 0, 0),
(800, 289, 2, 0),
(801, 289, 1, 0),
(802, 289, 1, 0),
(803, 289, 3, 0),
(804, 290, 0, 0),
(805, 290, 1, 0),
(806, 290, 2, 0),
(807, 290, 2, 0),
(808, 290, 1, 0),
(809, 291, 0, 0),
(810, 291, 2, 0),
(811, 291, 2, 0),
(812, 290, 2, 0),
(813, 290, 2, 0),
(814, 291, 2, 0),
(815, 291, 2, 0),
(816, 291, 2, 0),
(817, 291, 2, 0),
(818, 291, 3, 0),
(819, 290, 1, 0),
(820, 290, 1, 0),
(821, 292, 0, 0),
(822, 293, 0, 0),
(823, 293, 3, 0),
(824, 294, 0, 0),
(825, 294, 2, 0),
(826, 295, 0, 0),
(827, 294, 2, 0),
(828, 295, 1, 0),
(829, 295, 2, 0),
(830, 295, 3, 0),
(831, 296, 0, 0),
(832, 297, 0, 1),
(833, 298, 0, 1),
(834, 298, 2, 0),
(835, 298, 2, 0),
(836, 298, 2, 0),
(837, 298, 2, 0),
(838, 299, 0, 1),
(839, 299, 1, 1),
(840, 299, 1, 1),
(841, 299, 1, 1),
(842, 299, 3, 1),
(843, 300, 0, 1),
(844, 300, 3, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbavaliacao`
--

CREATE TABLE `tbavaliacao` (
  `id_avaliacao` int(5) NOT NULL,
  `tipo_avaliacao` varchar(20) NOT NULL,
  `nota` int(2) NOT NULL,
  `idConta` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbavaliacao`
--

INSERT INTO `tbavaliacao` (`id_avaliacao`, `tipo_avaliacao`, `nota`, `idConta`) VALUES
(9, 'Restaurante', 5, 29),
(10, 'Garcom', 5, 29),
(13, 'Restaurante', 0, 42),
(14, 'Garcom', 0, 42),
(15, 'Restaurante', 5, 43),
(16, 'Garcom', 2, 43),
(17, 'Restaurante', 4, 46),
(18, 'Garcom', 1, 46),
(19, 'Restaurante', 0, 51),
(20, 'Garcom', 0, 51),
(21, 'Restaurante', 0, 52),
(22, 'Garcom', 0, 52),
(23, 'Restaurante', 0, 54),
(24, 'Garcom', 0, 54),
(25, 'Restaurante', 0, 56),
(26, 'Garcom', 0, 56),
(27, 'Restaurante', 0, 55),
(28, 'Garcom', 0, 55),
(29, 'Restaurante', 0, 56),
(30, 'Garcom', 0, 56),
(31, 'Restaurante', 0, 57),
(32, 'Garcom', 0, 57),
(33, 'Restaurante', 0, 58),
(34, 'Garcom', 0, 58),
(35, 'Restaurante', 0, 63),
(36, 'Garcom', 0, 63),
(37, 'Restaurante', 0, 64),
(38, 'Garcom', 0, 64),
(39, 'Restaurante', 0, 65),
(40, 'Garcom', 0, 65),
(41, 'Restaurante', 0, 66),
(42, 'Garcom', 0, 66),
(43, 'Restaurante', 0, 69),
(44, 'Garcom', 0, 69),
(45, 'Restaurante', 0, 73),
(46, 'Garcom', 0, 73),
(47, 'Restaurante', 0, 74),
(48, 'Garcom', 0, 74),
(49, 'Restaurante', 0, 75),
(50, 'Garcom', 0, 75),
(51, 'Restaurante', 0, 76),
(52, 'Garcom', 0, 76),
(53, 'Restaurante', 0, 77),
(54, 'Garcom', 0, 77),
(55, 'Restaurante', 0, 78),
(56, 'Garcom', 0, 78),
(57, 'Restaurante', 0, 79),
(58, 'Garcom', 0, 79),
(59, 'Restaurante', 0, 80),
(60, 'Garcom', 0, 80),
(61, 'Restaurante', 0, 81),
(62, 'Garcom', 0, 81),
(63, 'Restaurante', 0, 82),
(64, 'Garcom', 0, 82),
(65, 'Restaurante', 0, 0),
(66, 'Garcom', 0, 0),
(67, 'Restaurante', 0, 84),
(68, 'Garcom', 0, 84),
(69, 'Restaurante', 0, 85),
(70, 'Garcom', 0, 85),
(71, 'Restaurante', 0, 86),
(72, 'Garcom', 0, 86),
(73, 'Restaurante', 0, 87),
(74, 'Garcom', 0, 87),
(75, 'Restaurante', 0, 90),
(76, 'Garcom', 0, 90),
(77, 'Restaurante', 0, 91),
(78, 'Garcom', 0, 91),
(79, 'Restaurante', 0, 93),
(80, 'Garcom', 0, 93),
(81, 'Restaurante', 0, 94),
(82, 'Garcom', 0, 94),
(83, 'Restaurante', 0, 99),
(84, 'Garcom', 0, 99),
(85, 'Restaurante', 0, 100),
(86, 'Garcom', 0, 100),
(87, 'Restaurante', 0, 102),
(88, 'Garcom', 0, 102),
(89, 'Restaurante', 0, 109),
(90, 'Garcom', 0, 109),
(91, 'Restaurante', 0, 110),
(92, 'Garcom', 0, 110),
(93, 'Restaurante', 0, 111),
(94, 'Garcom', 0, 111),
(95, 'Restaurante', 0, 113),
(96, 'Garcom', 0, 113),
(97, 'Restaurante', 0, 114),
(98, 'Garcom', 0, 114),
(99, 'Restaurante', 0, 115),
(100, 'Garcom', 0, 115),
(101, 'Restaurante', 0, 117),
(102, 'Garcom', 0, 117),
(103, 'Restaurante', 0, 127),
(104, 'Garcom', 0, 127),
(105, 'Restaurante', 0, 128),
(106, 'Garcom', 0, 128),
(107, 'Restaurante', 0, 129),
(108, 'Garcom', 0, 129),
(109, 'Restaurante', 0, 132),
(110, 'Garcom', 0, 132),
(111, 'Restaurante', 0, 133),
(112, 'Garcom', 0, 133),
(113, 'Restaurante', 0, 134),
(114, 'Garcom', 0, 134),
(115, 'Restaurante', 0, 135),
(116, 'Garcom', 0, 135),
(117, 'Restaurante', 0, 136),
(118, 'Garcom', 0, 136),
(119, 'Restaurante', 5, 137),
(120, 'Garcom', 5, 137),
(121, 'Restaurante', 0, 138),
(122, 'Garcom', 0, 138),
(123, 'Restaurante', 0, 139),
(124, 'Garcom', 0, 139),
(125, 'Restaurante', 0, 140),
(126, 'Garcom', 0, 140),
(127, 'Restaurante', 0, 141),
(128, 'Garcom', 0, 141),
(129, 'Restaurante', 0, 142),
(130, 'Garcom', 0, 142),
(131, 'Restaurante', 0, 143),
(132, 'Garcom', 0, 143),
(133, 'Restaurante', 0, 144),
(134, 'Garcom', 0, 144),
(135, 'Restaurante', 0, 145),
(136, 'Garcom', 0, 145),
(137, 'Restaurante', 0, 148),
(138, 'Garcom', 0, 148),
(139, 'Restaurante', 0, 154),
(140, 'Garcom', 0, 154),
(141, 'Restaurante', 0, 155),
(142, 'Garcom', 0, 155),
(143, 'Restaurante', 0, 156),
(144, 'Garcom', 0, 156),
(145, 'Restaurante', 0, 157),
(146, 'Garcom', 0, 157),
(147, 'Restaurante', 0, 158),
(148, 'Garcom', 0, 158),
(149, 'Restaurante', 0, 159),
(150, 'Garcom', 0, 159),
(151, 'Restaurante', 5, 160),
(152, 'Garcom', 5, 160),
(153, 'Restaurante', 0, 162),
(154, 'Garcom', 0, 162),
(155, 'Restaurante', 0, 163),
(156, 'Garcom', 0, 163),
(157, 'Restaurante', 0, 164),
(158, 'Garcom', 0, 164),
(159, 'Restaurante', 0, 165),
(160, 'Garcom', 0, 165),
(161, 'Restaurante', 0, 166),
(162, 'Garcom', 0, 166),
(163, 'Restaurante', 0, 167),
(164, 'Garcom', 0, 167),
(165, 'Restaurante', 0, 168),
(166, 'Garcom', 0, 168),
(167, 'Restaurante', 0, 170),
(168, 'Garcom', 0, 170),
(169, 'Restaurante', 0, 171),
(170, 'Garcom', 0, 171),
(171, 'Restaurante', 0, 172),
(172, 'Garcom', 0, 172),
(173, 'Restaurante', 0, 173),
(174, 'Garcom', 0, 173),
(175, 'Restaurante', 0, 174),
(176, 'Garcom', 0, 174),
(177, 'Restaurante', 0, 175),
(178, 'Garcom', 0, 175),
(179, 'Restaurante', 0, 176),
(180, 'Garcom', 0, 176),
(181, 'Restaurante', 0, 177),
(182, 'Garcom', 0, 177),
(183, 'Restaurante', 0, 178),
(184, 'Garcom', 0, 178),
(185, 'Restaurante', 0, 179),
(186, 'Garcom', 0, 179),
(187, 'Restaurante', 0, 180),
(188, 'Garcom', 0, 180),
(189, 'Restaurante', 0, 181),
(190, 'Garcom', 0, 181),
(191, 'Restaurante', 0, 182),
(192, 'Garcom', 0, 182),
(193, 'Restaurante', 0, 183),
(194, 'Garcom', 0, 183),
(195, 'Restaurante', 0, 184),
(196, 'Garcom', 0, 184),
(197, 'Restaurante', 0, 185),
(198, 'Garcom', 0, 185),
(199, 'Restaurante', 0, 186),
(200, 'Garcom', 0, 186),
(201, 'Restaurante', 0, 187),
(202, 'Garcom', 0, 187),
(203, 'Restaurante', 0, 187),
(204, 'Garcom', 0, 187),
(205, 'Restaurante', 0, 189),
(206, 'Garcom', 0, 189),
(207, 'Restaurante', 0, 191),
(208, 'Garcom', 0, 191),
(209, 'Restaurante', 0, 192),
(210, 'Garcom', 0, 192),
(211, 'Restaurante', 0, 205),
(212, 'Garcom', 0, 205),
(213, 'Restaurante', 0, 208),
(214, 'Garcom', 0, 208),
(215, 'Restaurante', 0, 209),
(216, 'Garcom', 0, 209),
(217, 'Restaurante', 0, 210),
(218, 'Garcom', 0, 210),
(219, 'Restaurante', 0, 211),
(220, 'Garcom', 0, 211),
(221, 'Restaurante', 0, 212),
(222, 'Garcom', 0, 212),
(223, 'Restaurante', 0, 213),
(224, 'Garcom', 0, 213),
(225, 'Restaurante', 0, 215),
(226, 'Garcom', 0, 215),
(227, 'Restaurante', 0, 216),
(228, 'Garcom', 0, 216),
(229, 'Restaurante', 0, 218),
(230, 'Garcom', 0, 218),
(231, 'Restaurante', 0, 219),
(232, 'Garcom', 0, 219),
(233, 'Restaurante', 0, 220),
(234, 'Garcom', 0, 220),
(235, 'Restaurante', 0, 221),
(236, 'Garcom', 0, 221),
(237, 'Restaurante', 0, 222),
(238, 'Garcom', 0, 222),
(239, 'Restaurante', 0, 223),
(240, 'Garcom', 0, 223),
(241, 'Restaurante', 0, 224),
(242, 'Garcom', 0, 224),
(243, 'Restaurante', 0, 224),
(244, 'Garcom', 0, 224),
(245, 'Restaurante', 0, 225),
(246, 'Garcom', 0, 225),
(247, 'Restaurante', 0, 226),
(248, 'Garcom', 0, 226),
(249, 'Restaurante', 0, 227),
(250, 'Garcom', 0, 227),
(251, 'Restaurante', 0, 228),
(252, 'Garcom', 0, 228),
(253, 'Restaurante', 0, 229),
(254, 'Garcom', 0, 229),
(255, 'Restaurante', 0, 230),
(256, 'Garcom', 0, 230),
(257, 'Restaurante', 0, 232),
(258, 'Garcom', 0, 232),
(259, 'Restaurante', 0, 231),
(260, 'Garcom', 0, 231),
(261, 'Restaurante', 0, 234),
(262, 'Garcom', 0, 234),
(263, 'Restaurante', 0, 233),
(264, 'Garcom', 0, 233),
(265, 'Restaurante', 0, 235),
(266, 'Garcom', 0, 235),
(267, 'Restaurante', 0, 236),
(268, 'Garcom', 0, 236),
(269, 'Restaurante', 0, 239),
(270, 'Garcom', 0, 239),
(271, 'Restaurante', 0, 240),
(272, 'Garcom', 0, 240),
(273, 'Restaurante', 0, 241),
(274, 'Garcom', 0, 241),
(275, 'Restaurante', 0, 242),
(276, 'Garcom', 0, 242),
(277, 'Restaurante', 0, 243),
(278, 'Garcom', 0, 243),
(279, 'Restaurante', 0, 247),
(280, 'Garcom', 0, 247),
(281, 'Restaurante', 0, 249),
(282, 'Garcom', 0, 249),
(283, 'Restaurante', 0, 251),
(284, 'Garcom', 0, 251),
(285, 'Restaurante', 0, 253),
(286, 'Garcom', 0, 253),
(287, 'Restaurante', 0, 254),
(288, 'Garcom', 0, 254),
(289, 'Restaurante', 0, 255),
(290, 'Garcom', 0, 255),
(291, 'Restaurante', 0, 256),
(292, 'Garcom', 0, 256),
(293, 'Restaurante', 0, 257),
(294, 'Garcom', 0, 257),
(295, 'Restaurante', 0, 258),
(296, 'Garcom', 0, 258),
(297, 'Restaurante', 0, 259),
(298, 'Garcom', 0, 259),
(299, 'Restaurante', 0, 260),
(300, 'Garcom', 0, 260),
(301, 'Restaurante', 0, 261),
(302, 'Garcom', 0, 261),
(303, 'Restaurante', 0, 262),
(304, 'Garcom', 0, 262),
(305, 'Restaurante', 0, 264),
(306, 'Garcom', 0, 264),
(307, 'Restaurante', 0, 265),
(308, 'Garcom', 0, 265),
(309, 'Restaurante', 0, 266),
(310, 'Garcom', 0, 266),
(311, 'Restaurante', 0, 267),
(312, 'Garcom', 0, 267),
(313, 'Restaurante', 0, 270),
(314, 'Garcom', 0, 270),
(315, 'Restaurante', 0, 272),
(316, 'Garcom', 0, 272),
(317, 'Restaurante', 0, 274),
(318, 'Garcom', 0, 274),
(319, 'Restaurante', 0, 276),
(320, 'Garcom', 0, 276),
(321, 'Restaurante', 0, 276),
(322, 'Garcom', 0, 276),
(323, 'Restaurante', 0, 277),
(324, 'Garcom', 0, 277),
(325, 'Restaurante', 0, 278),
(326, 'Garcom', 0, 278),
(327, 'Restaurante', 0, 281),
(328, 'Garcom', 0, 281),
(329, 'Restaurante', 0, 282),
(330, 'Garcom', 0, 282),
(331, 'Restaurante', 0, 283),
(332, 'Garcom', 0, 283),
(333, 'Restaurante', 0, 286),
(334, 'Garcom', 0, 286),
(335, 'Restaurante', 0, 289),
(336, 'Garcom', 0, 289),
(337, 'Restaurante', 0, 289),
(338, 'Garcom', 0, 289),
(339, 'Restaurante', 0, 289),
(340, 'Garcom', 0, 289),
(341, 'Restaurante', 0, 291),
(342, 'Garcom', 0, 291),
(343, 'Restaurante', 0, 290),
(344, 'Garcom', 0, 290),
(345, 'Restaurante', 0, 293),
(346, 'Garcom', 0, 293),
(347, 'Restaurante', 0, 295),
(348, 'Garcom', 0, 295),
(349, 'Restaurante', 0, 299),
(350, 'Garcom', 0, 299),
(351, 'Restaurante', 0, 300),
(352, 'Garcom', 0, 300);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbconta`
--

CREATE TABLE `tbconta` (
  `idConta` int(5) NOT NULL,
  `idMesa` int(5) NOT NULL,
  `idGarcom` int(5) DEFAULT NULL,
  `horaAbertura` datetime NOT NULL,
  `horaFechamento` datetime DEFAULT NULL,
  `idUsuario_Abertura` int(5) DEFAULT NULL,
  `idUsuario_Fechamento` int(5) DEFAULT NULL,
  `status_conta` varchar(20) NOT NULL,
  `gorjeta` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbconta`
--

INSERT INTO `tbconta` (`idConta`, `idMesa`, `idGarcom`, `horaAbertura`, `horaFechamento`, `idUsuario_Abertura`, `idUsuario_Fechamento`, `status_conta`, `gorjeta`, `flag`) VALUES
(251, 4, 2, '2018-07-28 16:33:22', '2018-07-28 16:35:30', 1, 1, '3', 10, 0),
(252, 3, 2, '2018-07-28 16:36:20', '2018-07-28 16:56:20', 1, 1, '3', 0, 0),
(253, 3, 2, '2018-07-28 16:37:23', '2018-07-28 16:38:17', 1, 1, '3', 10, 0),
(254, 1, 2, '2018-07-28 16:39:38', '2018-07-28 16:40:34', 1, 1, '3', 10, 0),
(255, 1, 2, '2018-07-28 17:34:48', '2018-07-28 17:35:44', 1, 1, '3', 10, 0),
(256, 3, 2, '2018-08-04 10:59:20', '2018-08-04 11:00:29', 1, 1, '3', 10, -1),
(257, 3, 2, '2018-08-04 11:46:39', '2018-08-04 11:47:26', 1, 1, '3', 10, -1),
(258, 4, 2, '2018-08-08 16:49:48', '2018-08-08 16:53:08', 1, 1, '3', 10, -1),
(259, 3, 2, '2018-08-09 16:16:02', '2018-08-09 16:17:13', 1, 1, '3', 10, -1),
(260, 3, 19, '2018-08-09 16:18:08', '2018-08-09 16:19:10', 1, 1, '3', 10, -1),
(261, 3, 2, '2018-08-09 16:19:36', '2018-08-09 16:20:43', 1, 1, '3', 10, -1),
(262, 4, 2, '2018-08-21 19:12:46', '2018-08-21 19:14:24', 1, 1, '3', 10, -1),
(263, 4, 19, '2018-08-27 19:15:43', NULL, 1, 1, '3', 0, -1),
(264, 3, 2, '2018-08-27 19:26:32', '2018-08-27 19:27:37', 1, 1, '3', 0, -1),
(265, 3, 19, '2018-08-28 16:22:24', '2018-08-28 16:23:25', 1, 1, '3', 10, -1),
(266, 16, 2, '2018-09-03 23:09:45', '2018-09-03 23:10:20', 21, 21, '3', 0, -1),
(267, 18, 2, '2018-09-08 02:31:09', '2018-09-08 02:35:04', 21, 21, '3', 10, -1),
(268, 18, 2, '2018-09-16 19:44:31', NULL, 21, 21, '3', 0, -1),
(269, 18, NULL, '2018-12-28 11:21:48', NULL, NULL, 24, '3', 0, -1),
(270, 11, 2, '2018-12-28 11:23:48', '2018-12-28 11:28:08', 24, 24, '3', 10, -1),
(271, 12, 2, '2018-12-28 11:28:25', NULL, 24, 24, '3', 0, -1),
(272, 10, 2, '2018-12-28 11:30:39', '2018-12-28 11:33:18', 24, 24, '3', 10, -1),
(273, 12, 2, '2018-12-28 11:33:27', NULL, 24, 24, '3', 0, -1),
(274, 9, 2, '2018-12-28 11:35:44', '2018-12-28 11:37:34', 24, 24, '3', 10, -1),
(275, 11, 2, '2018-12-28 11:37:48', NULL, 24, 24, '3', 0, -1),
(276, 10, 2, '2019-01-03 16:20:20', '2019-01-03 16:20:54', 21, 21, '3', 10, -1),
(277, 12, NULL, '2019-01-19 22:21:34', '2019-01-19 22:23:45', NULL, NULL, '3', 10, 0),
(278, 9, NULL, '2019-01-19 22:59:23', '2019-01-19 23:12:15', NULL, NULL, '3', 10, 0),
(279, 10, NULL, '2019-01-19 23:10:52', NULL, NULL, NULL, '3', 0, 0),
(280, 12, NULL, '2019-01-19 23:11:34', NULL, NULL, NULL, '3', 0, 0),
(281, 12, NULL, '2019-01-20 00:03:33', '2019-01-20 03:18:10', NULL, NULL, '3', 10, 0),
(282, 18, NULL, '2019-01-20 03:29:02', '2019-01-20 03:30:50', NULL, NULL, '3', 10, 0),
(283, 17, NULL, '2019-01-20 03:34:33', '2019-01-20 03:35:35', NULL, NULL, '3', 10, 0),
(285, 10, NULL, '2019-01-20 05:05:57', NULL, NULL, NULL, '3', 0, 0),
(286, 13, NULL, '2019-01-20 14:39:29', '2019-01-20 15:12:29', NULL, NULL, '3', 0, 0),
(287, 19, NULL, '2019-01-20 15:13:53', NULL, NULL, NULL, '3', 0, 0),
(288, 18, NULL, '2019-01-20 16:32:09', NULL, NULL, NULL, '3', 0, 0),
(289, 18, NULL, '2019-01-23 14:49:10', '2019-01-23 15:29:57', NULL, NULL, '3', 10, 0),
(290, 11, NULL, '2019-01-23 18:27:12', '2019-01-23 19:31:20', NULL, NULL, '3', 10, 0),
(291, 15, NULL, '2019-01-23 18:40:19', '2019-01-23 19:31:20', NULL, NULL, '3', 10, 0),
(292, 10, NULL, '2019-01-24 01:15:54', NULL, NULL, NULL, '3', 0, 0),
(293, 9, NULL, '2019-01-25 16:01:00', '2019-01-25 16:08:24', NULL, NULL, '3', 10, 0),
(294, 15, NULL, '2019-01-25 16:10:48', '2019-01-25 20:22:32', NULL, NULL, '3', 0, 0),
(295, 9, NULL, '2019-01-25 19:11:07', '2019-01-25 20:22:32', NULL, NULL, '3', 10, 0),
(296, 16, NULL, '2019-01-25 20:30:35', NULL, NULL, NULL, '3', 0, 0),
(297, 15, NULL, '2019-01-25 23:44:46', NULL, NULL, 1, '3', 0, -1),
(298, 9, NULL, '2019-01-26 00:16:55', NULL, NULL, 1, '3', 0, -1),
(299, 11, 19, '2019-09-12 16:55:56', '2019-09-12 17:00:06', 1, 1, '3', 10, -1),
(300, 9, 2, '2019-09-12 19:17:45', '2019-09-12 19:18:35', 22, 22, '3', 10, -1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbgarcom`
--

CREATE TABLE `tbgarcom` (
  `idGarcom` int(5) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `contComisao` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbgarcom`
--

INSERT INTO `tbgarcom` (`idGarcom`, `nome`, `contComisao`) VALUES
(1, 'Josenildo', 0),
(2, 'Rafael', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbmesa`
--

CREATE TABLE `tbmesa` (
  `idMesa` int(5) NOT NULL,
  `numMesa` int(10) NOT NULL,
  `idTablet` int(11) NOT NULL,
  `statusMesa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbmesa`
--

INSERT INTO `tbmesa` (`idMesa`, `numMesa`, `idTablet`, `statusMesa`) VALUES
(9, 1, 0, 0),
(10, 2, 0, 0),
(11, 3, 0, 0),
(12, 4, 0, 0),
(13, 5, 0, 0),
(15, 7, 0, 0),
(16, 8, 0, 0),
(17, 9, 0, 0),
(18, 10, 0, 0),
(19, 6, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbprodutos`
--

CREATE TABLE `tbprodutos` (
  `idProduto` int(5) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `descricao` text DEFAULT NULL,
  `preco` float DEFAULT NULL,
  `disponibilidade` tinyint(1) DEFAULT NULL,
  `img` varchar(60) DEFAULT NULL,
  `nivelDestaque` int(11) NOT NULL,
  `tipo` varchar(15) NOT NULL,
  `porcao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbprodutos`
--

INSERT INTO `tbprodutos` (`idProduto`, `nome`, `descricao`, `preco`, `disponibilidade`, `img`, `nivelDestaque`, `tipo`, `porcao`) VALUES
(11, 'Batata Frita', 'Batatinha bem quentinha', 10, 0, '/rest/products_img/Batata_Frita.jpg', 0, '2', 0),
(12, 'Cerveja', 'Puro Malte do bom', 5, 0, '/rest/products_img/7d1d7ed2b369900fe029447f707f-1447581.jpg', 2, '4', 0),
(13, 'Casquinha', 'Um sorvete mais humilde', 2, 0, '/rest/products_img/Casquinha.jpg', 3, '3', 0),
(14, 'Camarão', 'EBI', 15, 0, '/rest/products_img/Camarão.jpg', 5, '2', 0),
(17, 'Pure de Batatas', 'pure de batatas', 12.5, 0, '/rest/products_img/batata.jpg', 0, '2', 0),
(32, 'Risoto de Queijo', 'Risoto do Jovem', 110.5, 0, '/rest/products_img/32.jpg', 0, '1', 0),
(37, 'Macarronada', 'Bom pra engordar', 50.75, 0, '/rest/products_img/37283256280_b563048dca_h.jpg', 0, '1', 0),
(41, 'Fettuccine Alfredo', 'Fettuccine Alfredo é um prato feito de fettuccine com queijo Parmesão e manteiga.', 34.5, 0, '/rest/products_img/Fettuccine_Alfredo.jpeg', 0, '1', 0),
(50, 'Pudim Tradicional', 'Pudim de Leite Tradicional', 5, 0, '/rest/products_img/Pudim_Tradicional.jpeg', 0, '1', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbranking`
--

CREATE TABLE `tbranking` (
  `idRank` int(5) NOT NULL,
  `idGarcom` int(5) DEFAULT NULL,
  `idMesa` int(5) NOT NULL,
  `notaGarcom` int(2) NOT NULL,
  `hora` datetime NOT NULL,
  `notaRestaurante` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbtablet`
--

CREATE TABLE `tbtablet` (
  `idtablet` int(11) NOT NULL,
  `macTablet` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbtablet`
--

INSERT INTO `tbtablet` (`idtablet`, `macTablet`) VALUES
(1, 'ms-re-j5-r8'),
(2, 'ms-re-j5-r8'),
(3, '998455673'),
(4, 'trwex4567'),
(5, 'testeDeTab'),
(6, '4');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tbusuario`
--

CREATE TABLE `tbusuario` (
  `idUsuario` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `login` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `tipoUsuario` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `tbusuario`
--

INSERT INTO `tbusuario` (`idUsuario`, `nome`, `login`, `senha`, `tipoUsuario`) VALUES
(2, 'joseildo', 'jose', 'jose', 'Garçom'),
(5, 'admin', 'admin', 'admin', 'Administrador'),
(19, 'rafael', 'rafael', 'rafael', 'Garçom'),
(22, 'Balcão Padrão', 'balcao', 'balcao', 'Balcão');

-- --------------------------------------------------------

--
-- Estrutura para view `cozinha`
--
DROP TABLE IF EXISTS `cozinha`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozinha`  AS  select `tbmesa`.`numMesa` AS `numMesa`,`tbprodutos`.`nome` AS `nome`,`produtospedidos`.`quantidade` AS `quantidade`,`produtospedidos`.`obs` AS `obs`,`produtospedidos`.`hora_pedido` AS `hora_pedido` from (((`produtospedidos` join `tbconta` on(`tbconta`.`idConta` = `produtospedidos`.`idConta`)) join `tbmesa` on(`tbconta`.`idMesa` = `tbmesa`.`idMesa`)) join `tbprodutos` on(`produtospedidos`.`id_produtos` = `tbprodutos`.`idProduto`)) where `tbconta`.`status_conta` = 1 and `produtospedidos`.`status` = !1 ;

-- --------------------------------------------------------

--
-- Estrutura para view `ganhos_mesa`
--
DROP TABLE IF EXISTS `ganhos_mesa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ganhos_mesa`  AS  select `tbmesa`.`numMesa` AS `mesa_key`,`tbconta`.`idConta` AS `id_conta`,`produtospedidos`.`quantidade` AS `quant_prod`,`tbprodutos`.`preco` * `produtospedidos`.`quantidade` AS `soma_total`,`tbconta`.`horaAbertura` AS `data` from (`tbconta` join ((`produtospedidos` join `tbprodutos`) join `tbmesa`) on(`tbconta`.`idConta` = `produtospedidos`.`idConta` and `produtospedidos`.`id_produtos` = `tbprodutos`.`idProduto` and `tbconta`.`idMesa` = `tbmesa`.`idMesa`)) where `tbconta`.`status_conta` = '3' and `produtospedidos`.`status` = '1' ;

-- --------------------------------------------------------

--
-- Estrutura para view `gjt_rel`
--
DROP TABLE IF EXISTS `gjt_rel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `gjt_rel`  AS  select `tbusuario`.`idUsuario` AS `id`,`tbconta`.`idConta` AS `id_conta`,`tbusuario`.`nome` AS `nome`,truncate(`tbconta`.`gorjeta` * (`tbconta`.`gorjeta` / 100),0) AS `quant_gjt`,truncate(sum(`tbprodutos`.`preco` * `produtospedidos`.`quantidade` * (`tbconta`.`gorjeta` / 100)),2) AS `soma_total`,`tbconta`.`horaAbertura` AS `data` from (`tbusuario` join ((`tbconta` join `produtospedidos`) join `tbprodutos`) on(`tbusuario`.`idUsuario` = `tbconta`.`idGarcom` and `tbconta`.`idConta` = `produtospedidos`.`idConta` and `produtospedidos`.`id_produtos` = `tbprodutos`.`idProduto`)) where `tbusuario`.`tipoUsuario` = 'Garçom' and `tbconta`.`status_conta` = '3' and `produtospedidos`.`status` = '1' group by `tbconta`.`idConta` ;

-- --------------------------------------------------------

--
-- Estrutura para view `lastalert`
--
DROP TABLE IF EXISTS `lastalert`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lastalert`  AS  select `tbmesa`.`idMesa` AS `idMesa`,`tbconta`.`status_conta` AS `status_conta`,`tbalerts`.`idConta` AS `idConta`,`tbalerts`.`status` AS `status`,`tbalerts`.`tipoAlert` AS `tipoAlert`,`tbalerts`.`idAlert` AS `idAlert` from ((`tbalerts` join `tbconta` on(`tbconta`.`idConta` = `tbalerts`.`idConta`)) join `tbmesa` on(`tbconta`.`idMesa` = `tbmesa`.`idMesa`)) where `tbconta`.`status_conta` = 1 and `tbalerts`.`idAlert` in (select max(`tbalerts`.`idAlert`) from `tbalerts` group by `tbalerts`.`idConta` desc) ;

-- --------------------------------------------------------

--
-- Estrutura para view `ranking_pratos`
--
DROP TABLE IF EXISTS `ranking_pratos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ranking_pratos`  AS  select `tbprodutos`.`idProduto` AS `id`,`tbprodutos`.`nome` AS `nome`,`tbprodutos`.`preco` AS `preco`,`produtospedidos`.`id_produtos` AS `contador`,`produtospedidos`.`quantidade` AS `quantidade_vendida`,`produtospedidos`.`hora_pedido` AS `hora_pedido` from (`produtospedidos` join `tbprodutos` on(`produtospedidos`.`id_produtos` = `tbprodutos`.`idProduto`)) where `produtospedidos`.`status` = '1' ;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `produtospedidos`
--
ALTER TABLE `produtospedidos`
  ADD PRIMARY KEY (`id_produtos_pedidos`);

--
-- Índices de tabela `tbalerts`
--
ALTER TABLE `tbalerts`
  ADD PRIMARY KEY (`idAlert`);

--
-- Índices de tabela `tbavaliacao`
--
ALTER TABLE `tbavaliacao`
  ADD PRIMARY KEY (`id_avaliacao`);

--
-- Índices de tabela `tbconta`
--
ALTER TABLE `tbconta`
  ADD PRIMARY KEY (`idConta`);

--
-- Índices de tabela `tbgarcom`
--
ALTER TABLE `tbgarcom`
  ADD PRIMARY KEY (`idGarcom`);

--
-- Índices de tabela `tbmesa`
--
ALTER TABLE `tbmesa`
  ADD PRIMARY KEY (`idMesa`);

--
-- Índices de tabela `tbprodutos`
--
ALTER TABLE `tbprodutos`
  ADD PRIMARY KEY (`idProduto`);

--
-- Índices de tabela `tbranking`
--
ALTER TABLE `tbranking`
  ADD PRIMARY KEY (`idRank`);

--
-- Índices de tabela `tbtablet`
--
ALTER TABLE `tbtablet`
  ADD PRIMARY KEY (`idtablet`);

--
-- Índices de tabela `tbusuario`
--
ALTER TABLE `tbusuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `produtospedidos`
--
ALTER TABLE `produtospedidos`
  MODIFY `id_produtos_pedidos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=325;

--
-- AUTO_INCREMENT de tabela `tbalerts`
--
ALTER TABLE `tbalerts`
  MODIFY `idAlert` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=845;

--
-- AUTO_INCREMENT de tabela `tbavaliacao`
--
ALTER TABLE `tbavaliacao`
  MODIFY `id_avaliacao` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;

--
-- AUTO_INCREMENT de tabela `tbconta`
--
ALTER TABLE `tbconta`
  MODIFY `idConta` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;

--
-- AUTO_INCREMENT de tabela `tbgarcom`
--
ALTER TABLE `tbgarcom`
  MODIFY `idGarcom` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `tbmesa`
--
ALTER TABLE `tbmesa`
  MODIFY `idMesa` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de tabela `tbprodutos`
--
ALTER TABLE `tbprodutos`
  MODIFY `idProduto` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de tabela `tbranking`
--
ALTER TABLE `tbranking`
  MODIFY `idRank` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbtablet`
--
ALTER TABLE `tbtablet`
  MODIFY `idtablet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `tbusuario`
--
ALTER TABLE `tbusuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
