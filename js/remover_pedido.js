(function($) {	
	//responsavel pela remocao de um pedido feito no carrinho do cliente  
	remove = function(item) {
		var tr = $(item).closest('tr');	
    	tr.fadeOut(400, function() {
    		tr.remove();  	    
    	});	
    return false;	  
    }	
})(jQuery);