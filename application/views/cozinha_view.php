<!DOCTYPE html>
<html>
<head>
	<?php
		$redirect = site_url('cozinha/index');
	?>
	<title>Cozinha</title>
	<meta http-equiv="refresh" content="5;url=<?php echo $redirect;?>">
	<!--Fontes e Bootstrap-->
	<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/bootstrap-3.3.7/docs/dist/css/bootstrap.min.css">
	<script src="<?php echo base_url();?>js/jquery.js"></script>
	<script src="<?php echo base_url();?>js/atualizar_status.js"></script>
</head>
<body>
	<table id="table_cozinha" class="table table-bordered table-striped">
		<thead>
			<th>Mesa</th>
			<th>Pedido</th>
			<th>Quantidade</th>
			<th>Horário do Pedido</th>
			<th>Observação</th>
			<th>Ações</th>
		</thead>
		<tbody>
			<?php foreach($tabela as $row):?>
			<tr>
			<?php
				$data_pedido = date('H:i', strtotime($row['hora_pedido']));
			?>
				<th><?=$row['numMesa']?></th>
				<th><?=$row['nome']?></th>
				<th><?=$row['quantidade']?></th>
				<th><?=$data_pedido?></th>
				<th><?=$row['obs']?></th>
				<th>
					<?php 
					$idPedido=$row['id'];
					echo '<button style="border:none;background:none;">
                                    <span status_pedido="1" id_update="'.$idPedido.'"  tabela="produtospedidos" class="fa fa-check-square 
                                    atualizar_status" style="font-size:1.5em;"></span></button>'; ?>
				</th>
		<?php endforeach;?>
		</tbody>
	</table>

</body>
</html>