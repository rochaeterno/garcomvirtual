<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cozinha extends CI_Controller{
	public function index(){
		$this->load->model('cozinha_model');
		$dados['tabela']=$this->cozinha_model->Tabela();
		$this->load->view('cozinha_view',$dados);
	}
		public function Status_update(){
		//aqui com os dados trazidos de atualizar status o ajax se comunica com o model de balcão para capturar os dados
		$table=$_POST['tabela'];
		$id=$_POST['id_update'];
		$this->load->model('balcao_model');
		$confirm=$this->balcao_model->Status_update($table,$id);	
		if($confirm){
			echo $id;
		}		
		else{
			echo false; 
		}
	}
}

