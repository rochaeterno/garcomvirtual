<?php
class balcao_model extends CI_Model  {

	function Garcon(){

		$query=$this->db->where('tipoUsuario="Garçom"')->get('tbusuario');
		$garcon=$query->result_array();
		return $garcon;
	}

	function Mesas(){
		$this->db->order_by("numMesa", "asc");
		$query = $this->db->get('tbmesa');
		$results=$query->result_array();
		return $results;
	}
	
	function Cont_alert_mesa(){
		//Conta o numero de alertas que a mesa com id igual ao do paramentro possui
		$where = "tbconta.status_conta=1 and (tbalerts.tipoAlert=1 or tbalerts.tipoAlert=2)" ;
		return $results = $this->db->select("tbalerts.*,tbconta.idConta,tbconta.idMesa")
		->join("tbconta","tbalerts.idConta=tbconta.idConta")
		->where($where)
		->get('tbalerts')->result_array();
	}

	function Open_close(){
		//$where = "tbconta.status_conta='0' or tbconta.status_conta='2'";
		return $results4=$this->db->select("tbconta.*,tbmesa.*")
		->join("tbmesa","tbconta.idMesa=tbmesa.idMesa")
		//->where($where)
		->get('tbconta')->result_array();
	}
	
	function Historico($idMesa){
		$where = "(tbconta.status_conta='1' or tbconta.status_conta='2') and tbconta.idMesa=".$idMesa;

		$results=$this->db->select("tbprodutos.*, produtospedidos.*,tbconta.*")
		->join("tbprodutos", "produtospedidos.id_produtos=tbprodutos.idProduto")
		->join("tbconta", "produtospedidos.idConta=tbconta.idConta")
		->where($where)
		->order_by("produtospedidos.status")
		->order_by("produtospedidos.hora_pedido")
		->get("produtospedidos")->result_array();

		$where2 = "(tbconta.status_conta<'3') and tbconta.idMesa=".$idMesa;
		$results2 = $this->db->select("tbconta.*")->where($where2)->get("tbconta")->result_array();

		$cont  = array("indiceAberto" => $results, "indiceFechado" => $results2);
		// Retornando a quantidade em formato JSON
		echo json_encode($cont);
	}
	function Tela_fecha($id){
		//futuramente seria interessante unir esse metodo ao de mesmo nome presente no conta_model pois são praticamente o mesmo
		$where = "(tbconta.status_conta='2' or tbconta.status_conta='1') and produtospedidos.status='1' and tbconta.idConta=".$id;
		return $results3=$this->db  ->select("tbprodutos.*, produtospedidos.*,tbconta.*")
		->join("tbprodutos", "produtospedidos.id_produtos=tbprodutos.idProduto")
		->join("tbconta", "produtospedidos.idConta=tbconta.idConta")
		->where($where)
		->order_by("produtospedidos.hora_pedido")
		->get("produtospedidos")->result_array();
	}
	
	function Status_update($table,$id){
		$colId="";
		$status_val="";
		if($table=="tbalerts"){
			$colId = "idAlert";
			$status_val="1";
		}
		elseif($table=="produtospedidos"){
			$colId = "id_produtos_pedidos";
			$where ="$table.$colId=$id";
			$dados_pedidos = $this->db->get_where($table, $where)->row_array();
			$status_val=(string)$dados_pedidos['status'];
			$status_val=($status_val=="0")?"1":"0";
		}
		if($status_val=="1"){
			$where = "$table.status='0' and $table.$colId=$id";
		}
		if($status_val=="0"){
			$where = "$table.status='1' and $table.$colId=$id";
		}
		
		$dados=array("status"=>$status_val);
		
		$this->db->where($where);
		$this->db->update($table, $dados);
		
		$where = "$table.status='1' and $table.$colId=$id";
		
		$query = $this->db->get_where($table, $where);
		$num_rows = $query->num_rows();//conta o numero de linhas do select
		
		if($num_rows==1){
			return TRUE;
		}
		else{
			return FALSE;
		}					
	}
}
?>