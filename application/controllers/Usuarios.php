<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller{
	public function index(){
		$dados['titulo']="Usuários";
					//carrega a tela de inicial de Administrador
		$this->load->model('crud');
		$result=$this->crud->Select('tbusuario');
		$usuarios['usuarios']=$result;
		$this->load->view('header_adm_view',$dados);
		$this->load->view('usuarios_view',$usuarios);	
	}
	
	public function Insere_usuarios(){
		$form_usu['nome'] =  $_POST['nomeUsu'];
		$form_usu['login'] =  $_POST['loginUsu'];
		$form_usu['senha'] =  $_POST['senhaUsu'];
		$form_usu['tipoUser'] =  $_POST['tipoUser'];
		
		$table = 'tbusuario';
		$dados = array('nome' => $form_usu['nome'], 'senha' => $form_usu['senha'], 
			'login' => $form_usu['login'], 'tipoUsuario'=>$form_usu['tipoUser']);
		
		$this->load->model('usuarios_model');
		$confirm = $this->usuarios_model->Insert($table, $dados);
		echo $confirm;
		
		if($confirm){
			redirect('Usuarios/index');	
		}
	}
}
?>