var tot_contas = 0;
function create_table_line(){
	var persistence = '';
	var str;	
	$.ajax({
		//count_mesas = '<?php echo site_url("mesa/contar_mesas"); ?>'
		url:count_mesas,
		type:"POST",
		dataType: 'json',
		async: false,

		success:function(data_count){
			$("#table_bar").html("");
			tot_contas = data_count.indice.length;
			count=1;
			for(i = 0; i<tot_contas; i++){
				id_atual = data_count.indice[i].idMesa;
				num_atual = data_count.indice[i].numMesa;
				if(count==1){
				str='<div class="row">';
				} else if(count!=1){
					str = "";
				}
				str1='<!--Template das Mesas--><div class="col-md-2" style="display:none"><div id="'+id_atual+'" class="Mesa Mesa-preta ref" >'+
					'<div class="Mesa-icon"><i class="fa fa-user-times"aria-hidden="true"></i><p><strong>'+num_atual+'</strong></p>'+
					'<!-- CONTADOR --><span novos_pedidos=0 class="badge pedidos_feitos badge-primary badge-pill">0</span></div><input type="hidden" id="cod_tb"'+ 
					'value="'+num_atual+'"></input><input type="hidden" id="id_tb" value="'+id_atual+'"></input></div></div>';
				str2='';
				if (count%5==0){
					str2='</div><div class="row">';
				}
				else if (count%5!=0){
					str2='';
				}
				persistence = persistence + (str+str1+str2);
				count++;
			}
			$("#table_bar").append(persistence);
		}
	});
}

function refresh_alerts(id_conta,id_mesa){
	//count_alerts='<?php echo site_url("balcao/refresh_alerts"); ?>
	$.post(count_alerts,{id_conta:id_conta},function(data_count){
		$('#table_line').find('#'+id_mesa+'').find('.pedidos_feitos').html("");
		$('#table_line').find('#'+id_mesa+'').find('.pedidos_feitos').prepend(data_count.quant);
		if(data_count.quant>0){
			$('#table_line').find('#'+id_mesa+'').addClass('blink');
		}else if(data_count.quant==0){
			$('#table_line').find('#'+id_mesa+'').removeClass( "blink" );	
		}
		if(data_count.quant_garcom>0){
			$('#table_line').find('#'+id_mesa+'').removeClass( "Mesa-laranja" );

			$('#table_line').find('#'+id_mesa+'').addClass( 'Mesa-azul');
		}
	},"json");
}

function refresh_table_line(){
	/*A classe a seguir é responsavel por popular a metade da tela onde ficam as mesas, com as mesas abertas 
	atualmente no banco de dados, para tal ela segue o seguinte processo*/
	var i;
	var id_atual;
	var id_mesa_atual;
	var status_class;
	// Com este post, criamos um JSON com o ID de todas as contas presentes no sistema com o status menor que 3
	//count_contas = '<?php echo site_url("conta/contar_contas"); ?>'
	$.post(count_contas,function(data_count){
		if(tot_contas<data_count.indice.length){
			create_table_line();
		}
		//percorremos este JSON e capturamos o Id e status da conta
		for(i = 0; i<tot_contas; i++){
			id_atual = data_count.indice[i].idConta;
			status_atual = data_count.indice[i].status_conta;
			//com estes dados em mãos geramos as variaveis que futuramente definiram a cor de nossas mesas
			if(status_atual==0){status_class="verde";}//usando a classe new1 piscamos o verde, enquanto o new2 piscaria o azul
			if(status_atual==1){status_class="laranja";}
			if(status_atual==2){status_class="vermelha";}
			//usamos a seguir o idConta capturado para identificar a mesa e verificar o status da mesma
			$.ajax({
				//url_account='<?php echo site_url("conta/identificar_mesa"); 
				url:url_account,
				ajaxI:status_class,
				ajaxJ:id_atual,//as variaveis ajaxI e ajaxJ sevem de backup para os dados para que eles não sejam sobrepostos pela requisição async
				type:"POST",
				dataType: 'json',
				data: {id_conta : id_atual},

				success:function(data_account){
					status_class = this.ajaxI;

					//com o status da conta o id da mesa e da conta em mãos, criamos o icone da mesa na tela
					if(data_account.status==1){
						$('#table_line').find('#'+data_account.id_mesa+'').parent().show();
						if($('#table_line').find('#'+data_account.id_mesa+'').hasClass( "Mesa" )){
							$('#table_line').find('#'+data_account.id_mesa+'').removeClass( "Mesa-preta" );
							$('#table_line').find('#'+data_account.id_mesa+'').removeClass( "Mesa-verde" );
							$('#table_line').find('#'+data_account.id_mesa+'').removeClass( "Mesa-laranja" );
							$('#table_line').find('#'+data_account.id_mesa+'').removeClass( "Mesa-vermelha" );
						}
						$('#table_line').find('#'+data_account.id_mesa+'').addClass( 'Mesa-'+status_class+'');
						refresh_alerts(this.ajaxJ,data_account.id_mesa);
					}
				}
			});
		}
	},"json");
}

setInterval("refresh_table_line()", 6000);

$(function() {
	refresh_table_line();
});