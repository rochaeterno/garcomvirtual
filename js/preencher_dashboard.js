$(document).ready(function(){
	$(".ganhos_mesa_periodo").click(function(){
		//preenche as view de relatorio da tela do adm
		str1="";
		data = $(".form_ganhos_mesa_periodo").serialize();
		
		$.ajax({
			url: url_ganhos_mesa,
			type: "post",
			data: data,
			success: function(dados) {
				arr = $.parseJSON(dados);
				if((arr.length)==0){
					srt1='<tr><td colspan="4">Não há registros a serem mostrados, faça uma nova pesquisa.</td></tr>';			
				}
				else{			
				$(".table_ganhos_mesa tbody").html("");
				for(i = 0; i < arr.length; i++){

					//alert(arr[i]['mesa_key']);
					str1 = str1+"<tr class='active'>"
					+"<td>"+ arr[i]['mesa_key'] +"</td>"
					+"<td>"+ arr[i]['quant_contas'] +"</td>"
					+"<td>"+ arr[i]['quant_prod'] +"</td>"
					+"<td>R$ "+ parseFloat(arr[i]['soma_total']).toFixed(2) +"</td>"
					+"</tr>";
				}
			}
				//alert(str1);
				$(".table_ganhos_mesa tbody").append(str1);
			}

		});
	});
	
	$(".gjt_garcom").click(function(){
		str1="";
		data = $(".form_gjt_garcom").serialize();
		
		$.ajax({
			url: url_gjt_garcom,
			type: "post",
			data: data,
			success: function(dados) {
				arr = $.parseJSON(dados);
				if((arr.length)==0){
					srt1='<tr><td colspan="3">Não há registros a serem mostrados, faça uma nova pesquisa.</td></tr>';			
				}
				else{
					$(".table_gjt_garcom tbody").html("");
					for(i = 0; i < arr.length; i++){
						str1 = str1+"<tr class='active'>"
						+"<td>"+ arr[i]['nome'] +"</td>"
						+"<td>"+ arr[i]['quant_contas'] +"</td>"
						+"<td>"+ arr[i]['quant_gjt'] +"</td>"
						+"<td>R$ "+ parseFloat(arr[i]['soma_total']).toFixed(2) +"</td>"
						+"</tr>";
					}
				}
				//alert(str1);
				$(".table_gjt_garcom tbody").append(str1);
			}
		});
	});
	
	$(".ranking_pratos").click(function(){
		str1="";
		data = $(".form_ranking_pratos").serialize();
		
		$.ajax({
			url: url_ranking_pratos,
			type: "post",
			data: data,
			success: function(dados) {
				arr = $.parseJSON(dados);
				if((arr.length)==0){
				 srt1='<tr><td colspan="4">Não há registros a serem mostrados, faça uma nova pesquisa.</td></tr>';			
				}
				else{
					$(".table_ranking_pratos tbody").html("");
					for(i = 0; i < arr.length; i++){
						str1 = str1+"<tr class='active'>"
						+"<td>"+ arr[i]['nome'] +"</td>"
						+"<td>"+ arr[i]['preco'] +"</td>"
						+"<td>"+ arr[i]['quantidade_vendida'] +"</td>"
						+"</tr>";
					}
				}
				//alert(str1);
				$(".table_ranking_pratos tbody").append(str1);
			}
		});
	});
});