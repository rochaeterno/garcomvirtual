    $(document).ready(function(){
    // Ativa o tooltip
    $('[data-toggle="tooltip"]').tooltip();
    // Selecionar todas as checkbox
    var checkbox = $('table tbody input[type="checkbox"]');
    $("#selectAll").click(function(){
        if(this.checked){
            checkbox.each(function(){
                this.checked = true;                        
            });
        } else{
            checkbox.each(function(){
                this.checked = false;                        
            });
        } 
    });
    checkbox.click(function(){
        if(!this.checked){
            $("#selectAll").prop("checked", false);
        }
    });
    //Abre e fecha a side-bar
    $('#sidebarCollapse').on('click', function () {
       $('#sidebar').toggleClass('active');
   });
});

    $('.atualizar').click(function() {
        location.reload();
    });