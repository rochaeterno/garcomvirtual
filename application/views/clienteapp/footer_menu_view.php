<!--Modal Histórico HTML -->
<div id="historico" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <form>
        <div class="modal-header">                      
          <h4 class="modal-title">Histórico de Pedidos</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div class="modal-body">
          <table id="historico" class="table">
            <thead>
              <tr>
                <th scope="col" style="display: none">Qtd.</th>
                <th scope="col">Produto</th>
                <th scope="col" style="display: none">Preço Unid.</th>
                <th scope="col">Preço<span style="font-size:8px;">(Total)</span></th>
                <th scope="col">Status</th>

              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>

        <div class="modal-footer">
          <input type="button" class="btn btn-default" data-dismiss="modal" style="min-height: 60px;" value="Fechar">
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Fechamento -->
<div id="fechar" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
      <?php echo form_open('conta/Solicitar_Fechamento'); ?>

      <div class="modal-header">
        <h4 class="modal-title">Fechamento de conta </h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <input type="button" class="btn btn-default" onclick="chamar_garcom()" 
        value="Chamar Garçom" style="min-width: 0;float:right;margin-right: 20px;">
      </div>

      <div class="modal-body">
        <table id="table_close" class="table table-striped table-dark">
         <thead>
          <tr>
           <th scope="col" style="display: none">Qtd.</th>
           <th scope="col">Produto</th>
           <th scope="col">Preço Unid.</th>
           <th scope="col">Preço</th>
         </tr>
       </thead>
       <tbody>
        <div id="dados_fechamento"></div>
        <!--Tbody de fechamento alimentado pelo refresh_modals.js-->
        <div id="the_end"></div>
      </tbody>
    </table>
  </div>
  <div class="modal-footer">
    <input type="hidden" id="nota_rest" name="nota_rest"><!--Captura os dados que vem de avaliacao.js-->
    <input type="hidden" id="nota_garcom" name="nota_garcom"> <!--Captura os dados que vem de avaliacao.js-->
    <input type="hidden" id="idConta_fechada" name="idConta_fechada" value="<?php echo $_SESSION['idConta'];?>">
    <input type="hidden" id="last_gorjeta" name="gorjetapost" value="">
    <!--O botão a seguir direciona os dados do input dentro do form para a o controller conta e sua funcao Solicitar_Fechamento()-->
    <input type="submit" class="btn btn-laranja" style="min-height: 60px;" value="Fechar Conta">
  </div>
  <?php echo form_close();?>
</div>
</div>
</div>


<div id="share" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Compartilhe sua conta </h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>

      <div class="modal-body">

        <p>A senha da sua conta é:  <?php echo $_SESSION["idConta"];?></p>
        <img src="">
       <tbody>
        <div id="dados_fechamento"></div>
        <!--Tbody de fechamento alimentado pelo refresh_modals.js-->
        <div id="the_end"></div>
      </tbody>
    </table>
  </div>
  <div class="modal-footer">
    <input type="hidden" id="nota_rest" name="nota_rest"><!--Captura os dados que vem de avaliacao.js-->
    <input type="hidden" id="nota_garcom" name="nota_garcom"> <!--Captura os dados que vem de avaliacao.js-->
    <input type="hidden" id="idConta_fechada" name="idConta_fechada" value="<?php echo $_SESSION['idConta'];?>">
    <input type="hidden" id="last_gorjeta" name="gorjetapost" value="">
    <!--O botão a seguir direciona os dados do input dentro do form para a o controller conta e sua funcao Solicitar_Fechamento()-->
    <input type="submit" class="btn btn-laranja" style="min-height: 60px;" value="Fechar Conta">
  </div>
  <?php echo form_close();?>
</div>
</div>
</div>

</div>
<nav class="navbar-fixed-bottom container-fluid" style="height: 13%;">
  <ul class="nav nav-pills row fat" style="font-size: 1em;cursor: pointer;">
    <li id="contamenu" class="col-md-4 terco" style="position: relative;display: block;margin-left: 2px;">
      <div class="dropup">
        <a class=" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: block;padding: 10px 15px;">
          <i class="fa fa-credit-card-alt" style="display: block; padding-bottom: 4px;"></i><p style="display: block;left:0">Conta</p>
        </a>
        <ul class="dropdown-menu" aria-labelledby="about-us">
          <li style="height: 50px;"><a href="#share" data-toggle="modal" style="top:50%;left: 50%"><i class="fa fa-share-alt"></i> Compartilhar</a></li>
          <li style="height: 50px;"><a class="refresh_history" style="top:50%;left: 50%" href="#historico" data-toggle="modal"><i class="fa fa-history"></i> Histórico</a></li>
          <li style="height: 50px;"><a class="refresh_close" style="top:50%;left: 50%" href="#fechar" data-toggle="modal"><i class="fa fa-power-off"></i> Fechar</a></li>
        </ul>
      </div>
    </li>
    <li class="col-md-4 terco">
      <a id="solicit" onclick="chamar_garcom()" style="display: block;padding: 10px 15px;">
        <i id="garcon" class="fa fa-commenting-o" style="size:2em;display: block; padding-bottom: 4px;"></i><p style="display: block">Garçom</p>
      </a>
    </li>
  </li>
  <li class="col-md-4 terco">
    <a data-toggle="modal" href="#confirmar" class="cart" style="display: block;padding: 10px 15px;">
      <i class="fa fa-shopping-cart" style="display: block; padding-bottom: 4px;"></i><p style="display: block">Confirmar</p>
      <span></span>
    </a>
  </li>
</ul>
</nav>
  <!--Últimos JS-->
  <script src="<?php echo base_url();?>js/ultimosjs.js"></script>
  <script type="text/javascript">

    $('#solicit').click(function(){
      $('#alertg').show();
      $('#alertg').fadeOut(5000,function(){
        $('#alertg').hide(); 
      });

    });
    $('.close').click(function(){
      $('#alertg').hide();  
    });

  </script>
</body>
</html>