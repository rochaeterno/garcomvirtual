<?php
class crud extends CI_Model {
	function Insert($table, $dados){
		$this->db->insert($table, $dados);
		$query = $this->db->get_where($table, $dados);
		$num_rows = $query->num_rows();//conta o numero de linhas do select
		
		if($num_rows==1){
			return TRUE;
		}
		else{
			return FALSE;
		}				
	}
	
	function Insert_return_id($table, $dados){
		$result = $this->db->insert($table, $dados);
		$last_id = $this->db->insert_id();  
		return $last_id;				
	}

	function Select($table){
		$query = $this->db->get($table);
		$results=$query->result_array();
		return $results;
	}
	function Select_obj($table, $dados){
		$query = $this->db->get_where($table, $dados);
		
		$num_rows = $query->num_rows();//conta o numero de linhas do select
		
		if($num_rows==1){
			return $query->row_object();
		}
		else{
			return null;
		}		
	}
}
?>