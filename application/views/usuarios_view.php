 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?><!DOCTYPE html>
 <!-- Conteúdo da página -->
 <div id="page-wrapper" style="height:100vh;overflow: auto">
    <div class="container-fluid">
    <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Usuários
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Gerenciar <b>Usuários</b></h2>
                    </div>
                    <div class="col-sm-6">
                        <!--Nav da tabela-->
                        <a href="#adicionar" class="btn btn-laranja" data-toggle="modal">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> 
                            <span>Novo usuário</span></a>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <!--Colunas-->
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Login</th>
                            <th>Função</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($usuarios == FALSE): ?>
                            <p>Nenhum produto cadastrado!</p>
                            <?php else: ?>
                              <?php foreach ($usuarios as $row):?>
                                <tr>
                                    <!--Linhas-->
                                    <td><?=$row['idUsuario']?></td>
                                    <td><?=$row['nome']?></td>
                                    <td><?=$row['login']?></td>
                                    <td><?=$row['tipoUsuario']?></td>
                                    <td>
                                        <a href="#editar<?=$row['idUsuario']?>" class="edit" data-toggle="modal">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                        <!-- Editar Modal HTML -->
                                        <div id="editar<?=$row['idUsuario']?>" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <!--Form para editar Usuário-->
                                                    <?php echo form_open('Crud/EditarUser');?>
                                                    <div class="modal-header">                      
                                                        <h4 class="modal-title">Editar Usuário</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    </div>
                                                    <input type="hidden" name="id" value="<?=$row['idUsuario']?>">
                                                    <div class="modal-body">                    
                                                        <div class="form-group">
                                                            <label>Nome</label>
                                                            <input type="text"  name="nomeUsu" class="form-control" required value="<?=$row['nome']?>">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Login</label>
                                                            <input type="text" name="loginUsu" class="form-control" required value="<?=$row['login']?>">
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Senha</label>
                                                            <input type="password" name="senhaUsu" class="form-control" required value="<?=$row['senha']?>"></input>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Função</label>
                                                            <select class="form-control" id="TipoUser" name="TipoUser" required>
                                                                <?php 
                                                                $sl1='';$sl2='';$sl3='';$sl4='';
                                                                switch ($row['tipoUsuario']) {
                                                                    case 'Administrador':
                                                                    $sl2='selected';
                                                                    break;
                                                                    case 'Balcão':
                                                                    $sl1='selected';
                                                                    break;
                                                                    case 'Garçom':
                                                                    $sl3='selected';
                                                                    break;
                                                                    default:break;
                                                                }?>
                                                                <option value="Administrador" <?php echo $sl1;?>>Administrador</option>
                                                                <option value="Balcão" <?php echo $sl2;?>>Balcão</option>
                                                                <option value="Garçom" <?php echo $sl3;?>>Garçom</option>
                                                            </select>
                                                        </div>                  
                                                    </div>

                                                    <div class="modal-footer">
                                                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                                                        <input type="submit" class="btn btn-success" value="Salvar">
                                                    </div>
                                                    <?php echo form_close();?>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="delete" href="#excluiruser<?=$row['idUsuario']?>" data-toggle="modal">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    <!-- Excluir Modal HTML -->
                                    <div id="excluiruser<?=$row['idUsuario']?>" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                    <div class="modal-header">                      
                                                        <h4 class="modal-title">Excluir <strong><?=$row['nome']?></strong></h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    </div>
                                                    <div class="modal-body">                    
                                                        <p>Tem certeza que deseja apagar esse registro?</p>
                                                        <p class="text-warning"><small>Essa ação não pode ser desfeita.</small></p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <?php echo form_open('Crud/excluirUsuario');?>
                                                        <input type="hidden" name="id" value="<?=$row['idUsuario']?>">
                                                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                                                        <input type="submit" class="btn btn-danger" value="Excluir">
                                                        <?php echo form_close();?>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Adicionar Modal HTML -->
            <div id="adicionar" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <?php echo form_open('usuarios/Insere_usuarios', 'id="cadastraUsu"'); //chama o form de de inserir usuarios  ?> 
                        <div class="modal-header">                      
                            <h4 class="modal-title">Novo Usuário</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>

                        <div class="modal-body">                    
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text"  name="nomeUsu" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Login</label>
                                <input type="text" name="loginUsu" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Senha</label>
                                <input type="password" name="senhaUsu" class="form-control" required></input>
                            </div>
                            <div class="form-group">
                                <label>Função</label>
                                <select class="form-control" id="tipoUser" name="tipoUser" required>
                                    <option value="Administrador">Administrador</option>
                                    <option value="Balcão">Balcão</option>
                                    <option value="Garçom">Garçom</option>
                                </select>
                            </div>                  
                        </div>

                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                            <input type="submit" class="btn btn-success" value="Adicionar">
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
</body>
</html>